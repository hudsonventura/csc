<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


class ModelUsuario extends ModularCore\Model{

	function __construct() {
		//parent::__construct();
		//$this->loadDb('oraclePRD', 'oracle');
	}





	public function getUserSE($ip){
		$this->db->oracle->sqlBuilder->select('U.IDLOGIN, U.IDUSER');
		$this->db->oracle->sqlBuilder->FROM('SEUSERSESSION S');
		$this->db->oracle->sqlBuilder->JOIN('ADUSER U ON U.IDLOGIN = S.IDLOGIN');
		$this->db->oracle->sqlBuilder->WHERE("FGSESSIONTYPE = 2 AND
												to_char(DTDATE, 'DD/MM/YY') = to_char(sysdate, 'DD/MM/YY') AND
												nmloginaddress = '$ip'");
		$this->db->oracle->sqlBuilder->orderby('DTDATE desc');
		$return = $this->db->oracle->sqlBuilder->executeQuery();
		//var_dump($this->db->oracle->sqlBuilder->query()); die();

		//echo $this->db->oracle->sqlBuilder->getQuery(); die();
		if(isset($return[0]))
			return $return[0];
		else
			return false;
	}

	public function getUserHCM($user){
		$this->loadDb('oraclePRD', 'oracle');

		$cpf = $user['description'][0];
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('sap_xi.VRM_SOFTEXPERT@SEPRD');
		$this->db->oracle->sqlBuilder->where("cpf = '$cpf'");

		$user['HCM'] = $this->db->oracle->sqlBuilder->executeQuery();

		return $user;
	}

}