<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
header("Content-type: text/html; charset=utf-8");

class CronogramaModel extends ModularCore\Model{


	function __construct() {
		parent::__construct();
		$this->loadDb('oraclePRD', 'oracle');
		$this->urlSoap = Modularcore\core::$coreConfig['webservice_soap_PRD'];
	}
	
	function filtroSQLJoinVenture($usuario = null){
		//filtro de JOIN VENTURES ALDZ ALZ ALDC UNIDAS TAPAJOS UNITAPAJOS e TERFRON TERMINAL FRONTEIRA NORTE
		$joinVenture = '';
		if (isset($usuario[0]['mail'][0])) {
			
			
			switch (substr($usuario[0]['mail'][0], strpos($usuario[0]['mail'][0], '@'))) {
				case '@amaggildc.com.br':
					$joinVenture = "and FORMULARIO.empresa like '0035%'";
					break;
					
				case '@unitapajos.com.br':
					$joinVenture = "and (FORMULARIO.empresa like '0039%' OR FORMULARIO.empresa like '0041%')";
					break;
				
				case '@terfron.com.br':
					$joinVenture = "and (FORMULARIO.empresa like '0039%' OR FORMULARIO.empresa like '0041%')";
					break;
				
				default:
					$joinVenture = "";
					break;
			}
		}
		return $joinVenture;
	}

	function getEmpresas($registros = null){ //$usuario = NULL){
		
		if($registros){
			$empresas = array();
			foreach ($registros as $registro) {
				if (array_search(array('EMPRESA' => $registro['EMPRESA']), $empresas) === false) {
					array_push($empresas, array('EMPRESA' => $registro['EMPRESA']));
				}
			}
		}else{
			//$joinVenture = $this->filtroSQLJoinVenture($usuario);
		
			$this->db->oracle->sqlBuilder->select('EMPRESA');
			$this->db->oracle->sqlBuilder->from('DYNFECONTABIL FORMULARIO');
			$this->db->oracle->sqlBuilder->where("instancia = 'MODELO' ");//$joinVenture");
			$this->db->oracle->sqlBuilder->orderby('EMPRESA');
			//echo $this->db->oracle->sqlBuilder->getQuery();
			$empresas = $this->db->oracle->sqlBuilder->executeQuery();
		}
		
		
		return $empresas;
	}

	function getFechamentosAbertos($buscaEmpresa = null, $buscaAtividade = null, $buscaDiaUtil = null, $buscaEmAberto = null, $buscaPeriodo = null, $buscaDepartamento = null, $buscaUsuarioExecutor = '', $buscaUsuarioPapel = '', $usuario = null){
		
		
		
		
		//trata status da atividade
		if($buscaEmAberto == 'on') $buscaEmAberto = 'A.FGSTATUS = 2'; else $buscaEmAberto = ' (A.FGSTATUS = 2 OR A.FGSTATUS = 3) ';
		$diaUtil = '';
		if($buscaDiaUtil){
			foreach($buscaDiaUtil as $buscaDia){
				$diaUtil .= "OR (DECODE(ASSOC.UTIL, 'm1', '0', ASSOC.UTIL) like '$buscaDia%' or L.NMLANE IS NULL)";
			}
			$diaUtil = 'and ('.substr($diaUtil, 3).')';
		}


		//trata $buscaPeriodo
		$periodo = '';
		if ($buscaPeriodo && $buscaPeriodo <> "off"){
			foreach($buscaPeriodo as $buscaPeriod){
				$periodo .= "OR P.nmprocess like '$buscaPeriod%'";
			}
			$periodo = ' AND '.substr($periodo, 3);
		}else{
			$periodo = "AND P.nmprocess like '".$this->getPeriodos()[0]['PERIODO']."%'";
		}
		//var_dump($buscaPeriodo); die();

		//departamento
		$departamentoSQL = '';
		if($buscaDepartamento){
			foreach ($buscaDepartamento as $departamento)
			{
				$departamentoSQL .= "or (upper(REMOVE_ACENTOS(B.NMROLE)) like upper(REMOVE_ACENTOS('%$departamento%'))) ";
			}
			$departamentoSQL = ' ('.substr($departamentoSQL, 3).') ';
		}


		//empresa
		$empresaSQL = '';
		if($buscaEmpresa){
			foreach ($buscaEmpresa as $empresa)
			{
				$empresaSQL .= "or (upper(REMOVE_ACENTOS(FORMULARIO.EMPRESA)) like upper(REMOVE_ACENTOS('%$empresa%'))) ";
			}
			$empresaSQL = 'and ('.substr($empresaSQL, 3).') ';
		}
		
		##assegura que nenhuma empresa será exibida para uma JV join venture
		$joinVenture = $this->filtroSQLJoinVenture($usuario);
		//var_dump($usuario);
		if ($joinVenture <> '') {
			$empresaSQL = $joinVenture;
		}else{
			$joinVenture = "and (FORMULARIO.empresa NOT like '0039%' AND  FORMULARIO.empresa NOT like '0041%')";
		}
		
		##assegura que nenhuma empresa será exibida para uma JV join venture


		$sqlAtividade = "and (
								upper(REMOVE_ACENTOS(a.nmstruct)) like upper(REMOVE_ACENTOS('%$buscaAtividade%')) or upper(REMOVE_ACENTOS(b.nmrole)) like upper(REMOVE_ACENTOS('%$buscaAtividade%'))
								or upper(REMOVE_ACENTOS(b.nmuser)) like upper(REMOVE_ACENTOS('%$buscaAtividade%'))
							)";



		$this->db->oracle->sqlBuilder->select('P.IDPROCESS, FORMULARIO.empresa, REMOVE_ACENTOS(A.NMSTRUCT) AS NMSTRUCT,  to_char(A.DTEXECUTION, \'DD/MM/YYYY\') AS DTEXECUTION,

															to_char(A.DTESTIMATEDFINISH, \'DD/MM/YYYY\') AS "DTESTIMATEDFINISH",
															CASE WHEN A.NRTIMEESTFINISH IS NOT NULL THEN to_char(lpad(trunc(A.NRTIMEESTFINISH/60), 2, \'0\')||\':\'|| lpad(MOD(A.NRTIMEESTFINISH,60), 2, \'0\')||\':00\') ELSE \'23:59:00\' END AS NRTIMEESTFINISH,

															REMOVE_ACENTOS(CASE WHEN b.nmuser IS NULL THEN PAPEL.NMROLE ELSE b.nmuser END) AS "NMROLE",
															CASE WHEN REMOVE_ACENTOS(PAPEL.NMROLE) IS NOT NULL THEN REMOVE_ACENTOS(PAPEL.NMROLE) ELSE \'Usuario Especifico\' END AS "DEPTO",
															B.CDROLE,
															A.IDSTRUCT,
															CASE WHEN A.NRTIMEEXECUTION IS NOT NULL THEN to_char(lpad(trunc(A.NRTIMEEXECUTION/60), 2, \'0\')||\':\'|| lpad(MOD(A.NRTIMEEXECUTION,60), 2, \'0\')||\':00\') ELSE NULL END AS NRTIMEEXECUTION,
															DECODE(ASSOC.UTIL, \'m1\', \'0\', ASSOC.UTIL) as dia_util,
															DECODE(ASSOC.UTIL, \'m1\', \'0\', ASSOC.UTIL) as NMLANE,
															DECODE(P.FGSTATUS, 1, \'Em aberto\',  2 ,\'Suspenso\', 3 ,\'Cancelado\', 4 ,\'Encerrado\', 5 ,\'Bloqueado\') as INSTANCIA_STATUS,
															substr(p.nmprocess, 0, 7) as periodo, FORMULARIO.diautil1,FORMULARIO.diautil2,FORMULARIO.diautil3,FORMULARIO.diautil4,FORMULARIO.diautil5,FORMULARIO.diautil6,
															case when H.DSCOMMENT not like \'Anexo: %\' then \'<small>\'||H.DTHISTORY||\' as \'||H.TMHISTORY ||\' por \'||H.NMUSER||\':</small> <br />\'||REMOVE_ACENTOS(H.DSCOMMENT) else null end AS DSCOMMENT,
															ANEXO.cdattachment, ANEXO.NMATTACHMENT,

															case when A.DTEXECUTION is not null
																then case when A.DTEXECUTION > A.DTESTIMATEDFINISH then \'Encerrada em Atraso\' else \'Encerrada em Dia\' end
																else case when to_date(sysdate) <= A.DTESTIMATEDFINISH then \'Aberta em Dia\' else
																	CASE WHEN A.DTESTIMATEDFINISH IS NULL THEN \'Aberta em Dia\' ELSE \'Aberta em Atraso\' END end
																end as ATIVIDADE_STATUS,
																
															ASSOC.ATIVIDADE as ASSOCATIVIDADE, ASSOC.ASSOC, ASSOC.EMPRESA AS ASSOCEMPRESA, ASSOC.UTIL AS ASSOCDIAUTIL, ASSOC.HORARIO AS ASSOCHORARIO
															
															');

		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF				ON P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO		ON FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->join('WFSTRUCT A						ON P.IDOBJECT = A.IDPROCESS');
		$this->db->oracle->sqlBuilder->leftjoin('WFACTIVITY B				ON A.IDOBJECT = B.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin('adrole PAPEL		on PAPEL.cdrole = b.cdrole');
		//$this->db->oracle->sqlBuilder->join("(		SELECT ar.idrole
		//												FROM adrole ar 
		//												JOIN aduserrole aur ON ar.cdrole = aur.cdrole
		//												JOIN aduser  u ON u.cduser = aur.cduser
		//												where lower(u.nmuser) like lower('%$buscaUsuarioPapel%')) PAPELSUB ON PAPELSUB.IDROLE = PAPEL.IDROLE");
		//												
		$this->db->oracle->sqlBuilder->leftjoin('WFLANE L					ON A.IDLANE = L.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin("WFHISTORY H				ON H.IDSTRUCT = a.IDOBJECT AND H.DSCOMMENT IS NOT NULL AND H.DSCOMMENT not like 'Anexo: %'");
		$this->db->oracle->sqlBuilder->leftjoin('WFPROCATTACHMENT ATAASSOC ON A.IDOBJECT = ATAASSOC.IDSTRUCT');
		$this->db->oracle->sqlBuilder->leftjoin('ADATTACHMENT ANEXO ON ATAASSOC.CDATTACHMENT = ANEXO.CDATTACHMENT');
		$this->db->oracle->sqlBuilder->leftjoin("DYNFECONTABILASSOC ASSOC ON ASSOC.ATIVIDADE = SUBSTR(A.IDSTRUCT, 12) and (ASSOC.EMPRESA like '%'||substr(FORMULARIO.empresa, 0, 4)||'%' OR ASSOC.EMPRESA IS NULL) AND
																															((CASE WHEN (select COUNT(*) from DYNFECONTABILASSOC assoc2 WHERE  ASSOC2.ATIVIDADE = SUBSTR(A.IDSTRUCT, 12) AND (ASSOC2.EMPRESA LIKE '%'||SUBSTR(FORMULARIO.EMPRESA,0,4)||'%' OR ASSOC2.EMPRESA IS NULL)) > 1 THEN 2 ELSE 1 END = 1 AND 
																															ASSOC.EMPRESA IS NULL)
																															
																															OR
																															
																															(CASE WHEN (select COUNT(*) from DYNFECONTABILASSOC assoc2 WHERE  ASSOC2.ATIVIDADE = SUBSTR(A.IDSTRUCT, 12) AND (ASSOC2.EMPRESA LIKE '%'||SUBSTR(FORMULARIO.EMPRESA,0,4)||'%' OR ASSOC2.EMPRESA IS NULL)) > 1 THEN 2 ELSE 1 END = 2 AND
																															ASSOC.EMPRESA IS NOT NULL))");

		$this->db->oracle->sqlBuilder->where("A.NMSTRUCT IS NOT NULL AND FORMULARIO.EMPRESA <> 'MODELO' AND P.IDPROCESS IS NOT NULL  and A.IDSTRUCT <> 'FEContabil-DefinicaoEmpresa' and A.IDSTRUCT <> 'FEContabil-Fechamento' and A.IDSTRUCT <> 'FEContabil' AND A.NMSTRUCT <> 'Mensagem (enviar)'
												AND (H.DTHISTORY||' '||H.TMHISTORY = (SELECT MAX(HDT.DTHISTORY||' '||HDT.TMHISTORY) FROM WFHISTORY HDT WHERE HDT.IDSTRUCT = a.IDOBJECT AND HDT.DSCOMMENT IS NOT NULL) OR H.DTHISTORY IS NULL)
												and a.idstruct <> 'FEContabil' and a.idstruct <> 'FEContabil-Fechamento' and nmstruct <> 'timer' and nmstruct <> 'Timer41' 
												
												and (PAPEL.IDROLE IS NULL or PAPEL.IDROLE in (SELECT ar.idrole
																		FROM adrole ar
																		JOIN aduserrole aur ON ar.cdrole = aur.cdrole
																		JOIN aduser u ON u.cduser = aur.cduser
																		where B.NMROLE is null and lower(u.nmuser) like lower('%%')) or $departamentoSQL)
															and
															$buscaEmAberto
															$empresaSQL
															
															$diaUtil
															$periodo
															$sqlAtividade
															$buscaUsuarioExecutor
															$joinVenture");
		$this->db->oracle->sqlBuilder->orderby("substr(P.nmprocess, 0, 7) desc, P.IDPROCESS, DECODE(ASSOC.UTIL, 'm1', '0', ASSOC.UTIL), A.NMSTRUCT");

		//echo $this->db->oracle->sqlBuilder->getQuery(); die();///

		return $this->db->oracle->sqlBuilder->executeQuery();
	}

	function getFechamentosAbertosIndicadores($buscaEmpresa = null, $buscaAtividade = null, $buscaDiaUtil = null, $buscaEmAberto = null, $buscaPeriodo = null, $buscaDepartamento = null, $buscaUsuarioExecutor = '', $buscaUsuarioPapel = '', $usuario = null){
		
		
		
		
		//trata status da atividade
		if($buscaEmAberto == 'on') $buscaEmAberto = 'A.FGSTATUS = 2'; else $buscaEmAberto = ' (A.FGSTATUS = 2 OR A.FGSTATUS = 3) ';
		$diaUtil = '';
		if($buscaDiaUtil){
			foreach($buscaDiaUtil as $buscaDia){
				$diaUtil .= "OR (DECODE(ASSOC.UTIL, 'm1', '0', ASSOC.UTIL) like '$buscaDia%' or L.NMLANE IS NULL)";
			}
			$diaUtil = 'and ('.substr($diaUtil, 3).')';
		}


		//trata $buscaPeriodo
		$periodo = '';
		if ($buscaPeriodo && $buscaPeriodo <> "off"){
			foreach($buscaPeriodo as $buscaPeriod){
				$periodo .= "OR P.nmprocess like '$buscaPeriod%'";
			}
			$periodo = ' AND '.substr($periodo, 3);
		}else{
			$periodo = "AND P.nmprocess like '".$this->getPeriodos()[0]['PERIODO']."%'";
		}
		//var_dump($buscaPeriodo); die();

		//departamento
		$departamentoSQL = '';
		if($buscaDepartamento){
			foreach ($buscaDepartamento as $departamento)
			{
				$departamentoSQL .= "or (upper(REMOVE_ACENTOS(B.NMROLE)) like upper(REMOVE_ACENTOS('%$departamento%'))) ";
			}
			$departamentoSQL = 'or ('.substr($departamentoSQL, 3).') ';
		}


		//empresa
		$empresaSQL = '';
		if($buscaEmpresa){
			foreach ($buscaEmpresa as $empresa)
			{
				$empresaSQL .= "or (upper(REMOVE_ACENTOS(FORMULARIO.EMPRESA)) like upper(REMOVE_ACENTOS('%$empresa%'))) ";
			}
			$empresaSQL = 'and ('.substr($empresaSQL, 3).') ';
		}
		
		##assegura que nenhuma empresa será exibida para uma JV join venture
		$joinVenture = $this->filtroSQLJoinVenture($usuario);
		//var_dump($usuario);
		if ($joinVenture <> '') {
			$empresaSQL = $joinVenture;
		}else{
			$joinVenture = "and (FORMULARIO.empresa NOT like '0039%' AND  FORMULARIO.empresa NOT like '0041%')";
		}
		
		##assegura que nenhuma empresa será exibida para uma JV join venture


		$sqlAtividade = "and (
								upper(REMOVE_ACENTOS(a.nmstruct)) like upper(REMOVE_ACENTOS('%$buscaAtividade%')) or upper(REMOVE_ACENTOS(b.nmrole)) like upper(REMOVE_ACENTOS('%$buscaAtividade%'))
								or upper(REMOVE_ACENTOS(b.nmuser)) like upper(REMOVE_ACENTOS('%$buscaAtividade%'))
							)";

		$dm1 = '01'.date('/m/Y');
		$dm1 = $this->somar_dias_uteis($dm1, -1);

		$this->db->oracle->sqlBuilder->select('P.IDPROCESS, FORMULARIO.empresa, REMOVE_ACENTOS(A.NMSTRUCT) AS NMSTRUCT,  to_char(A.DTEXECUTION, \'DD/MM/YYYY\') AS DTEXECUTION, \''.$dm1.'\' as diautil0,

															to_char(A.DTESTIMATEDFINISH, \'DD/MM/YYYY\') AS "DTESTIMATEDFINISH",
															CASE WHEN A.NRTIMEESTFINISH IS NOT NULL THEN to_char(lpad(trunc(A.NRTIMEESTFINISH/60), 2, \'0\')||\':\'|| lpad(MOD(A.NRTIMEESTFINISH,60), 2, \'0\')||\':00\') ELSE \'23:59:00\' END AS NRTIMEESTFINISH,

															REMOVE_ACENTOS(CASE WHEN b.nmuser IS NULL THEN PAPEL.NMROLE ELSE b.nmuser END) AS "NMROLE",
															CASE WHEN REMOVE_ACENTOS(PAPEL.NMROLE) IS NOT NULL THEN REMOVE_ACENTOS(PAPEL.NMROLE) ELSE \'Usuario Especifico\' END AS "DEPTO",
															B.CDROLE,
															A.IDSTRUCT,
															CASE WHEN A.NRTIMEEXECUTION IS NOT NULL THEN to_char(lpad(trunc(A.NRTIMEEXECUTION/60), 2, \'0\')||\':\'|| lpad(MOD(A.NRTIMEEXECUTION,60), 2, \'0\')||\':00\') ELSE NULL END AS NRTIMEEXECUTION,
															DECODE(ASSOC.UTIL, \'m1\', \'0\', ASSOC.UTIL) as dia_util,
															DECODE(ASSOC.UTIL, \'m1\', \'0\', ASSOC.UTIL) as NMLANE,
															DECODE(P.FGSTATUS, 1, \'Em aberto\',  2 ,\'Suspenso\', 3 ,\'Cancelado\', 4 ,\'Encerrado\', 5 ,\'Bloqueado\') as INSTANCIA_STATUS,
															substr(p.nmprocess, 0, 7) as periodo, FORMULARIO.diautil1,FORMULARIO.diautil2,FORMULARIO.diautil3,FORMULARIO.diautil4,FORMULARIO.diautil5,FORMULARIO.diautil6,
															case when H.DSCOMMENT not like \'Anexo: %\' then \'<small>\'||H.DTHISTORY||\' as \'||H.TMHISTORY ||\' por \'||H.NMUSER||\':</small> <br />\'||REMOVE_ACENTOS(H.DSCOMMENT) else null end AS DSCOMMENT,
															ANEXO.cdattachment, ANEXO.NMATTACHMENT,

															case when A.DTEXECUTION is not null
																then case when A.DTEXECUTION > A.DTESTIMATEDFINISH then \'Encerrada em Atraso\' else \'Encerrada em Dia\' end
																else case when to_date(sysdate) <= A.DTESTIMATEDFINISH then \'Aberta em Dia\' else
																	CASE WHEN A.DTESTIMATEDFINISH IS NULL THEN \'Aberta em Dia\' ELSE \'Aberta em Atraso\' END end
																end as ATIVIDADE_STATUS,
																
															ASSOC.ATIVIDADE as ASSOCATIVIDADE, ASSOC.ASSOC, ASSOC.EMPRESA AS ASSOCEMPRESA, ASSOC.UTIL AS ASSOCDIAUTIL, ASSOC.HORARIO AS ASSOCHORARIO
															
															');

		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF				ON P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO		ON FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->join('WFSTRUCT A						ON P.IDOBJECT = A.IDPROCESS');
		$this->db->oracle->sqlBuilder->leftjoin('WFACTIVITY B				ON A.IDOBJECT = B.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin('adrole PAPEL		on PAPEL.cdrole = b.cdrole');
		//$this->db->oracle->sqlBuilder->join("(		SELECT ar.idrole
		//												FROM adrole ar 
		//												JOIN aduserrole aur ON ar.cdrole = aur.cdrole
		//												JOIN aduser  u ON u.cduser = aur.cduser
		//												where lower(u.nmuser) like lower('%$buscaUsuarioPapel%')) PAPELSUB ON PAPELSUB.IDROLE = PAPEL.IDROLE");
		//												
		$this->db->oracle->sqlBuilder->leftjoin('WFLANE L					ON A.IDLANE = L.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin("WFHISTORY H				ON H.IDSTRUCT = a.IDOBJECT AND H.DSCOMMENT IS NOT NULL AND H.DSCOMMENT not like 'Anexo: %'");
		$this->db->oracle->sqlBuilder->leftjoin('WFPROCATTACHMENT ATAASSOC ON A.IDOBJECT = ATAASSOC.IDSTRUCT');
		$this->db->oracle->sqlBuilder->leftjoin('ADATTACHMENT ANEXO ON ATAASSOC.CDATTACHMENT = ANEXO.CDATTACHMENT');
		$this->db->oracle->sqlBuilder->leftjoin("DYNFECONTABILASSOC ASSOC ON ASSOC.ATIVIDADE = SUBSTR(A.IDSTRUCT, 12) and (ASSOC.EMPRESA like '%'||substr(FORMULARIO.empresa, 0, 4)||'%' OR ASSOC.EMPRESA IS NULL) AND
																															((CASE WHEN (select COUNT(*) from DYNFECONTABILASSOC assoc2 WHERE  ASSOC2.ATIVIDADE = SUBSTR(A.IDSTRUCT, 12) AND (ASSOC2.EMPRESA LIKE '%'||SUBSTR(FORMULARIO.EMPRESA,0,4)||'%' OR ASSOC2.EMPRESA IS NULL)) > 1 THEN 2 ELSE 1 END = 1 AND 
																															ASSOC.EMPRESA IS NULL)
																															
																															OR
																															
																															(CASE WHEN (select COUNT(*) from DYNFECONTABILASSOC assoc2 WHERE  ASSOC2.ATIVIDADE = SUBSTR(A.IDSTRUCT, 12) AND (ASSOC2.EMPRESA LIKE '%'||SUBSTR(FORMULARIO.EMPRESA,0,4)||'%' OR ASSOC2.EMPRESA IS NULL)) > 1 THEN 2 ELSE 1 END = 2 AND
																															ASSOC.EMPRESA IS NOT NULL))");

		$this->db->oracle->sqlBuilder->where("A.NMSTRUCT IS NOT NULL AND FORMULARIO.EMPRESA <> 'MODELO' AND P.IDPROCESS IS NOT NULL  and A.IDSTRUCT <> 'FEContabil-DefinicaoEmpresa' and A.IDSTRUCT <> 'FEContabil-Fechamento' and A.IDSTRUCT <> 'FEContabil' AND A.NMSTRUCT <> 'Mensagem (enviar)'
												AND (H.DTHISTORY||' '||H.TMHISTORY = (SELECT MAX(HDT.DTHISTORY||' '||HDT.TMHISTORY) FROM WFHISTORY HDT WHERE HDT.IDSTRUCT = a.IDOBJECT AND HDT.DSCOMMENT IS NOT NULL) OR H.DTHISTORY IS NULL)
												and a.idstruct <> 'FEContabil' and a.idstruct <> 'FEContabil-Fechamento' and nmstruct <> 'timer' and nmstruct <> 'Timer41' 
												
												and (PAPEL.IDROLE in (SELECT ar.idrole
																		FROM adrole ar
																		JOIN aduserrole aur ON ar.cdrole = aur.cdrole
																		JOIN aduser u ON u.cduser = aur.cduser
																		where REMOVE_ACENTOS(CASE WHEN b.nmuser IS NULL THEN PAPEL.NMROLE ELSE b.nmuser END) is not null and lower(u.nmuser) like lower('%%')) $departamentoSQL)
															and
															$buscaEmAberto
															$empresaSQL
															
															$diaUtil
															$periodo
															$sqlAtividade
															$buscaUsuarioExecutor
															$joinVenture");
		$this->db->oracle->sqlBuilder->orderby("substr(P.nmprocess, 0, 7) desc, P.IDPROCESS, DECODE(ASSOC.UTIL, 'm1', '0', ASSOC.UTIL), A.NMSTRUCT");

		//echo $this->db->oracle->sqlBuilder->getQuery(); die();///

		return $this->db->oracle->sqlBuilder->executeQuery();
	}

	function getDiasUteis($periodo){
		$dm1 = '01'.date('/m/Y');
		$dm1 = $this->somar_dias_uteis($dm1, -1);
		$this->db->oracle->sqlBuilder->select("'$dm1' AS diautil0, diautil1, diautil2, diautil3, diautil4, diautil5, diautil6");
		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF ON P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO ON FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->where("P.nmprocess like '$periodo%' and empresa = '0001 - Amaggi Exp. e Imp.'");
		return $this->db->oracle->sqlBuilder->executeQuery();
	}

	function getPessoasDosPapeisFuncionais($cdrole){
		$this->db->oracle->sqlBuilder->select("ar.idrole as ID_Papel, ar.nmrole as Papel, u.idlogin as Login, u.nmuser as Nome, dp.nmdepartment as Area, p.nmposition as Funcao, u.iduser,
												case u.fguserenabled when 1 then 'Ativo' else 'Inativo' end as Status");
		$this->db->oracle->sqlBuilder->from("adrole ar");
		$this->db->oracle->sqlBuilder->join("aduserrole aur ON ar.cdrole = aur.cdrole");
		$this->db->oracle->sqlBuilder->join("aduser  u ON u.cduser = aur.cduser");
		$this->db->oracle->sqlBuilder->join("ADUSERDEPTPOS d ON d.cduser = u.cduser");
		$this->db->oracle->sqlBuilder->join("adposition p ON p.cdposition = d.cdposition");
		$this->db->oracle->sqlBuilder->join("addepartment dp ON dp.cddepartment = d.cddepartment");
		$this->db->oracle->sqlBuilder->where("d.fgdefaultdeptpos = 1 AND AR.CDROLE = '$cdrole'");// AND u.cduser <> 9");
		return $this->db->oracle->sqlBuilder->executeQuery();
	}

	function getPeriodos($usuario = null){
		
		$joinVenture = $this->filtroSQLJoinVenture($usuario);
		
		
		$this->db->oracle->sqlBuilder->select(" distinct substr(p.nmprocess, 0, 7) as periodo");
		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF			ON P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO	ON FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->where("(P.FGSTATUS = 1 or P.FGSTATUS = 4) $joinVenture");
		$this->db->oracle->sqlBuilder->orderby("substr(p.nmprocess, 0, 7) desc");
		//echo $this->db->oracle->sqlBuilder->getQuery();
		return $this->db->oracle->sqlBuilder->executeQuery();
	}

	function getDepartamentos(){
		$this->db->oracle->sqlBuilder->select(' distinct PAPEL.NMROLE as "NAOUSAR", REMOVE_ACENTOS(PAPEL.NMROLE) AS "DEPTO"');
		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF				ON P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO		ON FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->join('WFSTRUCT A						ON P.IDOBJECT = A.IDPROCESS');
		$this->db->oracle->sqlBuilder->leftjoin('WFACTIVITY B				ON A.IDOBJECT = B.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin('adrole PAPEL		on PAPEL.cdrole = b.cdrole');
		$this->db->oracle->sqlBuilder->where("PAPEL.NMROLE NOT LIKE 'Atividade %'  and A.IDSTRUCT <> 'FEContabil-DefinicaoEmpresa' and A.IDSTRUCT <> 'FEContabil-Fechamento' and A.IDSTRUCT <> 'FEContabil'");
		$this->db->oracle->sqlBuilder->orderby('PAPEL.NMROLE');
		return $this->db->oracle->sqlBuilder->executeQuery();
	}

	function getDepartamentosUnicos($registros){
		
		/*$joinVenture = $this->filtroSQLJoinVenture($usuario);
		
		
		$sql = "SELECT DISTINCT DEPTO FROM (SELECT
											CASE WHEN PAPEL.NMROLE LIKE 'Fiscal Indiretos%' THEN 'Fiscal Indiretos' ELSE REMOVE_ACENTOS(PAPEL.NMROLE) END AS \"DEPTO\"

											FROM wfprocess P
											JOIN GNASSOCFORMREG GNF	ON P.cdassocreg = GNF.cdassoc
											JOIN DYNFECONTABIL FORMULARIO	ON FORMULARIO.oid = GNF.OIDENTITYREG
											JOIN WFSTRUCT A	ON P.IDOBJECT = A.IDPROCESS
											LEFT JOIN WFACTIVITY B	ON A.IDOBJECT = B.IDOBJECT
											LEFT JOIN adrole PAPEL on PAPEL.cdrole = b.cdrole
											WHERE PAPEL.NMROLE NOT LIKE 'Atividade %' $joinVenture and A.IDSTRUCT <> 'FEContabil-DefinicaoEmpresa' and A.IDSTRUCT <> 'FEContabil-Fechamento' and A.IDSTRUCT <> 'FEContabil'and PAPEL.NMROLE <> 'Fical Diretos' and PAPEL.NMROLE <> 'Fiscal Indiretos ('
											ORDER BY PAPEL.NMROLE) order by DEPTO";
		$this->db->oracle->sqlBuilder->setQuery($sql);
		//die($this->db->oracle->sqlBuilder->getQuery());
		return $this->db->oracle->sqlBuilder->executeQuery();*/
		
		$retorno = array();
		foreach ($registros as $registro) {
			if (array_search(array('DEPTO' => $registro['DEPTO']), $retorno) === false) {
				array_push($retorno, array('DEPTO' => $registro['DEPTO']));
			}
		}
		
		return $retorno;
	}

	function getInstanciasAbertas($buscaEmpresa, $buscaAtividade, $buscaDiaUtil, $buscaEmAberto, $buscaPeriodo, $usuario = null){
		
		$joinVenture = $this->filtroSQLJoinVenture($usuario);
		
		//trata status da atividade
		if($buscaEmAberto == 'on') $buscaEmAberto = ' AND A.FGSTATUS = 2 '; else $buscaEmAberto = ' ';

		//tratadia
		$diaUtil = '';
		foreach($buscaDiaUtil as $buscaDia){
			$diaUtil .= "OR DECODE(ASSOC.UTIL, 'm1', '0', ASSOC.UTIL) like '$buscaDia%'";
		}
		$diaUtil = substr($diaUtil, 3);

		//trata $buscaPeriodo
		$periodo = '';
		if ($buscaPeriodo <> "off"){
			foreach($buscaPeriodo as $buscaPeriod){
				$periodo .= "OR P.nmprocess like '$buscaPeriod%'";
			}
			$periodo = ' AND '.substr($periodo, 3);
		}


		$empresaSQL = '';
		foreach ($buscaEmpresa as $empresa)
		{
			$empresaSQL .= "OR (upper(REMOVE_ACENTOS(FORMULARIO.EMPRESA)) like upper(REMOVE_ACENTOS('%$empresa%')) and (upper(REMOVE_ACENTOS(a.nmstruct)) like upper(REMOVE_ACENTOS('%$buscaAtividade%'))OR upper(REMOVE_ACENTOS(b.nmrole)) like upper(REMOVE_ACENTOS('%$buscaAtividade%')) OR upper(REMOVE_ACENTOS(b.NMUSER)) like upper(REMOVE_ACENTOS('%$buscaAtividade%'))))";
		}
		$empresaSQL = substr($empresaSQL, 3);
		
		##assegura que nenhuma empresa será exibida para uma JV join venture
		$joinVenture = $this->filtroSQLJoinVenture($usuario);
		//var_dump($usuario);
		if ($joinVenture <> '') {
			$empresaSQL = "$joinVenture";
			$empresaSQL = substr($empresaSQL, 3);
		}
		##assegura que nenhuma empresa será exibida para uma JV join venture

		
		$this->db->oracle->sqlBuilder->select('DISTINCT P.IDPROCESS, P.NMPROCESS, P.CDPROCESS, DECODE(P.FGSTATUS, 1, \'Em aberto\',  2 ,\'Suspenso\', 3 ,\'Cancelado\', 4 ,\'Encerrado\', 5 ,\'Bloqueado\') as INSTANCIA_STATUS, substr(p.nmprocess, 0, 7) as periodo');
		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF			ON P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO	ON FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->join('WFSTRUCT A ON A.IDPROCESS = P.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin('WFLANE L					ON A.IDLANE = L.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin('WFACTIVITY B				ON A.IDOBJECT = B.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin('DYNFECONTABILASSOC ASSOC ON ASSOC.ATIVIDADE = SUBSTR(A.IDSTRUCT, 12) AND (FORMULARIO.empresa = ASSOC.EMPRESA OR ASSOC.EMPRESA IS NULL)');
		$this->db->oracle->sqlBuilder->where("($empresaSQL) $buscaEmAberto AND ($diaUtil) $periodo and FORMULARIO.EMPRESA <> 'MODELO' AND P.IDPROCESS IS NOT NULL AND (A.FGSTATUS = 3 OR A.FGSTATUS = 2) $joinVenture
															");
		$this->db->oracle->sqlBuilder->orderby('substr(p.nmprocess, 0, 7) desc, P.IDPROCESS');
		//ECHO $this->db->oracle->sqlBuilder->getQuery(); die();
		return $this->db->oracle->sqlBuilder->executeQuery();
	}

	function seInstanciar($empresa, $dia, $matricula){
		error_reporting(0);
		$this->loadLib('nusoap');
		error_reporting('ALL');
	

		$client = @new nusoap_client($this->urlSoap);//*****ALTERAR DOMINIO****

		$paramWebService = array();
		$paramWebService["WorkflowID"] = 'FECONTABIL'; //Identificador da instancia (obtido via sql acima)
		$paramWebService["WorkflowTitle"] = date('Y/m').' - '.$empresa;
		$paramWebService["UserID"] = $matricula; // Matrícula do usuário.
		$client->setCredentials("SE Suite\\sistema.automatico","softexpert123","basic");

		//return array('RecordID'=>'FEC2017056', 'Detail'=>'Instancia aberta com sucesso');

		$result = $client->call("newWorkflow", $paramWebService);
		$error = $client->getError();
		if($result){
			return $result;
		}else{
			return $error;
		}

	}

	function seUploadFile($instancia, $idstruct, $matricula, $filename, $file){
		error_reporting(0);
		$this->loadLib('nusoap');
		error_reporting('ALL');

		$client =new nusoap_client($this->urlSoap);//*****ALTERAR DOMINIO****

		$paramWebService = array();
		$paramWebService["WorkflowID"] = $instancia;
		$paramWebService["ActivityID"] = $idstruct;


		$paramWebService["FileName"] = $filename;
		$fileSize = filesize($file);
		$paramWebService["FileContent"] = base64_encode(fread(fopen($file, "r"), $fileSize));

		$paramWebService["UserID"] = $matricula; // Matrícula do usuário.

		$paramWebService["AttachmentID"] = substr(date('Y-m-d_H-i-s-').$filename, 0, 48);
		$paramWebService["AttachmentName"] = $filename;


		$client->setCredentials("SE Suite\\sistema.automatico","softexpert123","basic");


		$result = $client->call("newAttachment", $paramWebService);
		$error = $client->getError();
		if($result){
			return $result;
		}else{
			return $error;
		}

	}

	function seSetComentario($idprocess, $idstruct, $comment){
		$this->db->oracle->sqlBuilder->select("h.idobject");
		$this->db->oracle->sqlBuilder->from("wfprocess P");
		$this->db->oracle->sqlBuilder->join("WFSTRUCT A  ON P.IDOBJECT = A.IDPROCESS");
		$this->db->oracle->sqlBuilder->join("WFHISTORY H ON H.IDSTRUCT = a.IDOBJECT");
		$this->db->oracle->sqlBuilder->where("h.dscomment = 'Executado via Web Service' and P.IDPROCESS = '$idprocess' and a.idstruct = '$idstruct'");
		$idobject = $this->db->oracle->sqlBuilder->executeQuery()[0]['IDOBJECT'];

		$this->db->oracle->sqlBuilder->update(array('DSCOMMENT' => ''.ModularCore\core::removeSpecialChar($comment)), "WFHISTORY");
		$this->db->oracle->sqlBuilder->where("idobject = '$idobject'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();

		return $return;
	}

	function seExecutarAtividade($instancia, $idstruct, $matricula){

		//$this->seExecutarAtividade41($instancia);
		
		//busca o registro na entidade para marcar a fecontabilx como executada
		$this->db->oracle->sqlBuilder->select("P.*, FORMULARIO.*, A.*, substr(a.idstruct, 12) as numativiade");
		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->join('WFSTRUCT A ON A.IDPROCESS = P.IDOBJECT');
		$this->db->oracle->sqlBuilder->where("P.IDPROCESS = '$instancia' AND A.Dtenabled IS NOT NULL AND NMSTRUCT LIKE '%-%' AND A.DTEXECUTION IS NULL");
		$this->db->oracle->sqlBuilder->orderby('P.IDPROCESS, a.idstruct');
		$r1 = $this->db->oracle->sqlBuilder->executeQuery();
		


		$oid = $r1[0]['OID']; 
		
		//pega o codigo da atidade que está sendo executada
		$campoTabela = strtoupper(str_replace('-', '', $idstruct));
		
		
		
		//marca a atividade como executada
		$this->db->oracle->sqlBuilder->update(array($campoTabela => 0), 'DYNFECONTABIL');
		$this->db->oracle->sqlBuilder->where("OID = '$oid'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();
		
		//se nao for possivel executar a atividade, apresenta o erro
		if (!$return) die('Houve um erro ao executar esta atividade.');
		

		
		error_reporting(0);
		$this->loadLib('nusoap');
		error_reporting('ALL');

		$client =new nusoap_client($this->urlSoap);//*****ALTERAR DOMINIO****

		$paramWebService = array();
		$paramWebService["WorkflowID"] = $instancia;
		$paramWebService["ActivityID"] = $idstruct;
		$paramWebService["ActionSequence"] = 1;
		$paramWebService["UserID"] = $matricula;

		$client->setCredentials("SE Suite\\sistema.automatico","softexpert123","basic");
		//return array('Code'=>'1');
		
		$result = $client->call("executeActivity", $paramWebService);

		$error = $client->getError();
		//var_dump($client);
		if($result){
			return $result;
		}else{
			return $error;
		}

	}

	function seExecutarAtividade41($instancia){

		$this->db->oracle->sqlBuilder->select('fecontabil41, fecontabil9 , fecontabil33 , fecontabil34 , fecontabil35 , fecontabil36 , fecontabil37 , fecontabil39 , fecontabil40 , fecontabil42 , fecontabil43 , fecontabil44 , fecontabil45 , fecontabil46 , fecontabil47 , fecontabil48 , fecontabil49 , fecontabil50 , fecontabil67, OID');
		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->where("P.IDPROCESS = '$instancia'");

		//echo $this->db->oracle->sqlBuilder->getQuery(); die();

		$atividade41 = $this->db->oracle->sqlBuilder->executeQuery()[0];

		if ($atividade41['FECONTABIL41'] == 1)
		{
			array_shift($atividade41);
			foreach ($atividade41 as $atividade => $value)
			{
				if($value == 1){
					echo "$atividade => $value<br>";
					return false;
				}
			}

		}
		$oid = $atividade41['OID'];
		$array = array('FECONTABIL41' => 0);
		$this->db->oracle->sqlBuilder->update($array, 'DYNFECONTABIL');
		$this->db->oracle->sqlBuilder->where("oid = '$oid'");

		$return = $this->db->oracle->sqlBuilder->executeQuery();
		return true;
	}

	function seEditaForm($empresa, $dia, $instancia){

		$fecontabil = array();

		//PEGA OS VALORES DO REGISTRO MODELO DA EMPRESA
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->from('DYNFECONTABIL');
		$this->db->oracle->sqlBuilder->where("INSTANCIA = 'MODELO' AND EMPRESA = '$empresa'");
		$resultado =$this->db->oracle->sqlBuilder->executeQuery()[0];


		if(!isset($resultado)){
			echo $this->db->oracle->sqlBuilder->getQuery();
		}
		foreach($resultado as $campo => $valor){
			if(strpos($campo, 'FECONTABIL')===0){
				$fecontabil[$campo] = $valor;
			}else{
				//var_dump($campo .'=>'. $valor.'Não tem fecontabil');
			}
		}

		// regra de datas de ultimo dia util das empresas

		//para empresas que finalizam no 4º dia util
		$somaDia1 = 0;
		$somaDia2 = 1;
		$somaDia3 = 1;
		$somaDia4 = 1;
		$somaDia5 = 0;
		$somaDia6 = 0;
		$verificarEmpresa = substr($empresa, 0, 4);
		
		//para empresas que finalizam no 5º dia util
		if($verificarEmpresa == '0010' || $verificarEmpresa == '0032' || $verificarEmpresa == '0004' || $verificarEmpresa == '0039' || $verificarEmpresa == '0041'){
			$somaDia5 = 1;
			$somaDia6 = 0;
		}
		
		//para empresas que finalizam no 6º dia util
		if($verificarEmpresa == '0001' || $verificarEmpresa == '0015' || $verificarEmpresa == '0005'){
			$somaDia5 = 1;
			$somaDia6 = 1;
		}
		// regra de datas de ultimo dia util das empresas


		$fecontabil['DIAUTIL1'] = $this->somar_dias_uteis($dia, $somaDia1);
		$fecontabil['DIAUTIL2'] = $this->somar_dias_uteis($fecontabil['DIAUTIL1'], $somaDia2);
		$fecontabil['DIAUTIL3'] = $this->somar_dias_uteis($fecontabil['DIAUTIL2'], $somaDia3);
		$fecontabil['DIAUTIL4'] = $this->somar_dias_uteis($fecontabil['DIAUTIL3'], $somaDia4);
		$fecontabil['DIAUTIL5'] = $this->somar_dias_uteis($fecontabil['DIAUTIL4'], $somaDia5);
		$fecontabil['DIAUTIL6'] = $this->somar_dias_uteis($fecontabil['DIAUTIL5'], $somaDia6);
		$fecontabil['EMPRESA'] = $empresa;
		$fecontabil['HOJE'] = date('d/M/Y');
		$fecontabil['INSTANCIA'] = $instancia;


		//PEGA O OID A SER ALTERADO
		$this->db->oracle->sqlBuilder->select('FORMULARIO.OID');
		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF			ON P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join("DYNFECONTABIL FORMULARIO	ON FORMULARIO.oid = GNF.OIDENTITYREG ");
		$this->db->oracle->sqlBuilder->where("P.IDPROCESS = '$instancia'");

		$oid = $this->db->oracle->sqlBuilder->executeQuery()[0]['OID'];

		$this->db->oracle->sqlBuilder->beginTransaction();
		$this->db->oracle->sqlBuilder->setQuery("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'");
		$this->db->oracle->sqlBuilder->executeQuery();

		//ALTERA OS VALORES DOS CAMPOS FECONTABIL PERANTE O MODELO E O OID DA INSTANCIA GERADA
		$this->db->oracle->sqlBuilder->update($fecontabil, 'DYNFECONTABIL');
		$this->db->oracle->sqlBuilder->where("OID = '$oid'");


		$result = $this->db->oracle->sqlBuilder->executeQuery();
		$this->db->oracle->sqlBuilder->commit();

		return $result;
	}
	
	function seCalculaPrazos($instancia = ''){
		
		if (isset($_GET['primeiraExecucao'])) {
			sleep(15);
		}
		
		$this->db->oracle->sqlBuilder->select("P.*, FORMULARIO.*, A.*, substr(a.idstruct, 12) as numativiade, DECODE(ASSOC.UTIL, 'm1', '0', ASSOC.UTIL) as NMLANE, ASSOC.assoc, ASSOC.atividade, ASSOC.util, ASSOC.horario");
		$this->db->oracle->sqlBuilder->from('wfprocess P');
		$this->db->oracle->sqlBuilder->join('GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc');
		$this->db->oracle->sqlBuilder->join('DYNFECONTABIL FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG');
		$this->db->oracle->sqlBuilder->join('WFSTRUCT A ON A.IDPROCESS = P.IDOBJECT');
		$this->db->oracle->sqlBuilder->join('WFLANE L ON A.IDLANE = L.IDOBJECT');
		$this->db->oracle->sqlBuilder->leftjoin('DYNFECONTABILASSOC ASSOC ON ASSOC.ATIVIDADE = SUBSTR(A.IDSTRUCT, 12) AND (FORMULARIO.empresa = ASSOC.EMPRESA OR ASSOC.EMPRESA IS NULL)');
		$this->db->oracle->sqlBuilder->where("P.IDPROCESS = '$instancia' AND A.Dtenabled IS NOT NULL AND NMSTRUCT LIKE '%-%' AND A.DTEXECUTION IS NULL");
		$this->db->oracle->sqlBuilder->orderby('P.IDPROCESS, a.idstruct');
		$r1 = $this->db->oracle->sqlBuilder->executeQuery();
		//ECHO $this->db->oracle->sqlBuilder->getQuery(); die();
		
		if (!$r1) {
			//die('Nenhuma instancia de fechamento contabil encontrado com este numero de FEC.');
		}
		
		$oid = $r1[0]['OID'];
		
		
		//pega o codigo da empresa em questão
		$empresa = substr($r1[0]['EMPRESA'], 0, 4);
		
		

		
		
		
		
		
		//associação e prazos de atividades
		$this->db->oracle->sqlBuilder->select("EMPRESA, ATIVIDADE, UTIL, HORARIO, CASE WHEN ASSOC IS NOT NULL THEN ASSOC ELSE '-' END AS ASSOC");
		$this->db->oracle->sqlBuilder->from('DYNFECONTABILASSOC ASSOC');
		$this->db->oracle->sqlBuilder->where("ASSOC.EMPRESA like '%$empresa%' or ASSOC.EMPRESA IS NULL");
		$this->db->oracle->sqlBuilder->orderby("ATIVIDADE, EMPRESA desc");
		$associacoesAtividades = $this->db->oracle->sqlBuilder->executeQuery();
		//ECHO $this->db->oracle->sqlBuilder->getQuery(); DIE();
		
		
		$associacoes = array();
		foreach ($associacoesAtividades as $associacaoAtividade) {
			$associacoes[$associacaoAtividade['ATIVIDADE']] = array('ASSOC' => $associacaoAtividade['ASSOC'], 'UTIL' => $associacaoAtividade['UTIL'], 'HORARIO' => $associacaoAtividade['HORARIO'], 'EMPRESA' => $associacaoAtividade['EMPRESA']);
		}
		ksort($associacoes);
		
		
		

		
		foreach ($r1 as $atividadeMaster) {
			
		
			$instancia = $atividadeMaster['INSTANCIA'];
			$idobject = $atividadeMaster['IDOBJECT'];
			$indiceAtividade = substr($atividadeMaster['IDSTRUCT'], 11);
			
			
			$indiceAtividadeFecontabil = strtoupper('FECONTABIL'.$indiceAtividade);
			$prazoAtualData = $atividadeMaster['DTESTIMATEDFINISH'];
			$prazoAtualHora = $atividadeMaster['NRTIMEESTFINISH'];
			
			
			
			
			//----------------------------------------------------------------pode informar erro
			$associacoesString = @$associacoes[$indiceAtividade]['ASSOC'];
			$associacoesDiaUtil = @$associacoes[$indiceAtividade]['UTIL'];
			$associacoesHorario = @$associacoes[$indiceAtividade]['HORARIO'];
			$associacoesEmpresa = @$associacoes[$indiceAtividade]['EMPRESA'];
			//----------------------------------------------------------------pode informar erro
			
			
			//echo $prazoAtualData.'<br>';
			
			
			//print_r($atividadeMaster) ; 
			
			$indiceAtividadeFecontabil = str_replace('-', '', $indiceAtividadeFecontabil);
			
			
			if($atividadeMaster[$indiceAtividadeFecontabil] == 1)
			//caso a atividade AINDA NAO tenha sido executada
			{
				$diaUtilPadrao = @$atividadeMaster["DIAUTIL$associacoesDiaUtil"];
				$horarioPadrao = @$atividadeMaster["HORARIO"];

				//ATRIBUI A USUARIO FANTASMA PARA A EXECUÇÃO DE ATIVIDADE NAO APARECER PARA O USUARIO
				$this->db->oracle->sqlBuilder->update(array('CDUSER' => '1',), 'WFTASK');
				$this->db->oracle->sqlBuilder->where("IDACTIVITY = '$idobject'");
				$return = $this->db->oracle->sqlBuilder->executeQuery();
				
				
				$horario = $this->formatar_horas_SE($associacoesHorario);
				
				//caso de d-1
				if (!$diaUtilPadrao) {
					$diaUtilPadrao = $this->somar_dias_uteis($atividadeMaster["DIAUTIL1"], -1);
					$horarioPadrao = '23:59';
				}
				
				
				//caso nao existam associações para esta atividade
				if ($associacoesString == '-') {
					
					ECHO "$indiceAtividadeFecontabil - Prazo FIXO: $diaUtilPadrao as $horario <br><br>";
					
					
					
					//executar update na data do dia padrao
					$this->db->oracle->sqlBuilder->update(array('DTESTIMATEDFINISH' => $diaUtilPadrao, 'NRTIMEESTFINISH' => $horario), 'WFSTRUCT');
					$this->db->oracle->sqlBuilder->where("IDOBJECT = '$idobject'");
					$return = $this->db->oracle->sqlBuilder->executeQuery();
					//----------------------------------------------------------------pode informar erro
				}
				
				//caso EXISTAM ASSOCIAÇÔES PARA ESTA ATIVIDADE
				else{
					
					
					
					$time_diaUtilPadrao = DateTime::createFromFormat('d/m/Y H:i', "$diaUtilPadrao $horarioPadrao");
					$timestamp_diaUtilPadrao = $time_diaUtilPadrao->getTimestamp();
					
					$time_agora = DateTime::createFromFormat('d/m/Y H:i', date('d/m/Y  H:i'));
					$timestamp_agora = $time_agora->getTimestamp();
					
					//echo "$timestamp_diaUtilPadrao e $timestamp_agora";
					
					//adianta o prazo padrao em um periodo, para que antes da atividade vencer, o prazo seja renovado
					$timestamp_agora_MenosTempoParaLancamento = strtotime('+8 hours', $timestamp_agora);
					$time_agora = new DateTime();
					$time_agora->setTimestamp($timestamp_agora_MenosTempoParaLancamento);
					$timestamp_agora = $time_agora->getTimestamp();
					
					
					echo 'Comprando '.$time_agora->format('Y-m-d H:i') . " com ".$time_diaUtilPadrao->format('Y-m-d H:i').' Result: '.($timestamp_agora > $timestamp_diaUtilPadrao).'<br>';
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					$pendencias = explode(',', $associacoesString);
					ECHO "$indiceAtividadeFecontabil - Prazo padrão: $diaUtilPadrao as $horario <br>";
					
					
					//varre as pendencias da atidade em busca de alguma que ainda esteja ativa
					foreach ($pendencias as $pendencia) {
						$pendenciaFecontabil = strtoupper('FECONTABIL'.$pendencia);
						
						//compara se agora é antes da data de prazo padrao da atividade
						if ($timestamp_agora > $timestamp_diaUtilPadrao) {
							//se o prazo é menor que a data atual, e a pendencia ainda estiver ativa, o usuario ganha d+1 de prazo
							$prazo = $this->somar_dias_uteis(date('d/m/Y'), 1);
							//echo ' - Prazo alterado!<br>';
						}else{
							//se o prazo padrao for maior que agora, o prazo fica o padrao mesmo
							$prazo = $diaUtilPadrao;
							//echo ' - Prazo NAO alterado!<br>';
						}
						
							
						
						
						
						//caso a PENDENCIA AINDA NAO tenha sido executada
						if((isset($atividadeMaster[$pendenciaFecontabil]) && $atividadeMaster[$pendenciaFecontabil] == 1) || (!$atividadeMaster['DTESTIMATEDFINISH']))
						{
							ECHO "---> Atividade pendente em aberto $pendenciaFecontabil  -> Novo prazo: $diaUtilPadrao<br> ";
							
							//executar update na data de amanha
							$this->db->oracle->sqlBuilder->update(array('DTESTIMATEDFINISH' => $prazo, 'NRTIMEESTFINISH' => $horario), 'WFSTRUCT');
							$this->db->oracle->sqlBuilder->where("IDOBJECT = '$idobject'");
							$return = $this->db->oracle->sqlBuilder->executeQuery();
							
							goto considerarApenasUmaDependencia;
						}
						
						//pendencias de dias uteis (todas de um determinado dia)
						if (strpos($pendencia, 'f') > -1) {
							$diaUtilVerificado = substr($pendencia, 1, 1);
							ECHO "---> Verificando atividades pendentes em aberto do $diaUtilVerificado º DIA UTIL<br>";
							foreach ($r1 as $atividadeDataFinal) {
								
								if (SUBSTR($atividadeDataFinal["NMLANE"], 0, 1) == $diaUtilVerificado) {
									echo 'Atividade pendente '.$atividadeDataFinal["NMSTRUCT"].'<BR> ';
									//VAR_DUMP($atividadeDataFinal); DIE();
									//echo $atividadeDataFinal["NMSTRUCT"].' - '.$atividadeDataFinal['DTFINISH'].'<BR>';
									ECHO "---> Atividade pendente em aberto ".$atividadeDataFinal["NMSTRUCT"]."  -> Novo prazo: $prazo<br> ";
							
									//executar update na data de amanha
									$this->db->oracle->sqlBuilder->update(array('DTESTIMATEDFINISH' => $prazo, 'NRTIMEESTFINISH' => $horario, 'NRTIMEESTFINISH' => $horario), 'WFSTRUCT');
									$this->db->oracle->sqlBuilder->where("IDOBJECT = '$idobject'");
									$return = $this->db->oracle->sqlBuilder->executeQuery();

									
									goto considerarApenasUmaDependencia;
								}
							}
						}
					}
					
					//se nenhuma pendencia ainda estiver ativa, nao altera o prazo
					ECHO "---> Prazo da atividade não alterado pois nao existentem atividades pendentes associadas ainda em aberto<br> ";
					
					//se alguma atividade for executada, ele sai direto para este ponto
					considerarApenasUmaDependencia:
					echo '<br>';
				}
			}
			//var_dump($_SERVER);
			//if ($_SERVER['REMOTE_HOST'] <> '172.15.10.47' && $_SERVER['REMOTE_HOST'] <> '172.12.12.59' && $_SERVER['REMOTE_HOST'] <> '172.12.12.190') {
				ob_clean();
			//}
		}
		
		return true;
	}



	private function somar_dias_uteis($data,$dias) {
		$dataArray = explode('/', $data);
		if ($dias >= 0) {
			$dataGerada = date('d/m/Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " +$dias days"));
			$fds = date('w', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " +$dias days"));
		}else{
			$dataGerada = date('d/m/Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " $dias days"));
			$fds = date('w', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " $dias days"));
		}
		
		
		//verifica feriado cadastrado no SE
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->from('AMAGGI_DIASUTEIS DIASUTEIS');
		$this->db->oracle->sqlBuilder->where("DATA = to_date('$dataGerada')");
		$util = $this->db->oracle->sqlBuilder->executeQuery()[0]['UTIL'];
		//verifica feriado cadastrado no SE

		if($util == 0)//($fds == 0 || $fds == 6){//} || $util == 0)
		{
			if ($dias >= 0) {
				return $this->somar_dias_uteis($data, $dias+1);
			}
			else{
				return $this->somar_dias_uteis($data, $dias-1);
			}
			
		}

		return $dataGerada;
	}
	
	private function formatar_horas_SE($horaFinal, $horaInicial = '00:00') {
			
		$data1 = "2006-07-22 $horaInicial:00";
		$data2 = "2006-07-22 $horaFinal:00";

		$unix_data1 = strtotime($data1);
		$unix_data2 = strtotime($data2);

		return ($unix_data2 - $unix_data1)/60 ;
	}
}