<?php header("Content-type: text/html; charset=utf-8"); ?>
<!DOCTYPE html >
<html lang="en">

<head>

     <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />
	
	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
	
	
    <title><?php echo $title?></title>
    
	<!--Imports-->
		
		
		<!-- Bootstrap Core CSS --><link href="<?php echo ASSETS;?>bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<!-- jQuery --><script src="<?php echo ASSETS;?>jquery/js/jquery.min.js"></script>
		<!-- Bootstrap Core JS --> <script src="<?php echo ASSETS;?>bootstrap/js/bootstrap.min.js"></script>
		<!-- Bootsbox --> <script src="<?php echo ASSETS;?>bootstrap/js/bootbox.min.js"></script>
		<!-- Google Charts  --><script src="<?php echo ASSETS;?>GoogleCharts/loader.js"></script>
		<!-- Google Charts  --><script src="<?php echo ASSETS;?>GoogleCharts/loader.js"></script>
		<!-- pickadate.js  --><script src="<?php echo ASSETS;?>pickadate/picker.js"></script>
		<!-- pickadate.js  --><script src="<?php echo ASSETS;?>pickadate/picker.date.js"></script>
		<!-- pickadate.js  --><link href="<?php echo ASSETS;?>pickadate\themes/default.css" rel="stylesheet" />
		<!-- pickadate.js  --><link href="<?php echo ASSETS;?>pickadate\themes/default.date.css" rel="stylesheet" />
		<!-- notify  --><script src="<?php echo ASSETS;?>notify/bootstrap-notify.min.js"></script>
		<!--SELECT BOX--><link rel="stylesheet" href="<?php echo ASSETS;?>selectbox/select2.min.css">
		<!--SELECT BOX--><script src="<?php echo ASSETS;?>selectbox/select2.min.js"></script>
		<!-- Custom Fonts --><link href="<?php echo ASSETS?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />


    <!-- Bootstrap styles -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Generic page styles -->
    <link rel="stylesheet" href="<?php echo ASSETS;?>jQuery-File-Upload/css/style.css">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<?php echo ASSETS;?>jQuery-File-Upload/css/jquery.fileupload.css">
    
</head>

<body>

	<div id="myModal" class="modal fade">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <!-- dialog body -->
		  <div class="modal-body">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
        
		  </div>
		  <!-- dialog buttons -->
		  <div class="modal-footer"><button type="button" class="btn btn-primary">OK</button></div>
		</div>
	  </div>
	</div>

	<?php if(isset($msg)){?>
    <!--<div class="alert alert-<?php echo $msgType?>"><?php echo $msg?></div>-->
    
    			<script>
						$.notify({
							title: "<?php echo $msg?><br>",
							message: ""
						},{
							type: "<?php echo $msgType?>",
							allow_dismiss: true,
							newest_on_top: false,
							timer: null,
						});
			</script>
	<?php } ?>


	<div class="" style="margin-top: -50px;">
		<div class="col-md-12">
			<div class="panel text-center">
				
				<div class="panel-body" style="margin-top: -20px;">
					<div class="col-md-3"><img src="<?php echo ASSETS;?>logo_csc.png" height="54" /></div><div class="col-md-3"></div><h2><?php echo $title?></h2><br />
					<div class="col-md-12" style="margin-top: -20px;">
						<div class="panel panel-default">
							<div class="panel-heading">
								<form method="GET" action="" id="form" name="form" class="col-md-12 form-horizontal" style="margin-top: 30px">
									<div class="row">

										<!--busca-->
										<div class="col-md-6">
											<div class="form-group com-md-6">
												<label class="col-sm-3 control-label" for="atividade">Atividade</label>
												<div class="col-sm-9">
													<input class="form-control" type="text" name="atividade" value="<?php if(isset($buscaAtividade)) echo $buscaAtividade?>" placeholder="Buscar por Atividade" />
												</div>
											</div>
										</div>

										<!-- empresa-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-sm-3 control-label" for="empresa">Empresa</label>
												<div class="col-md-9">
													<select class="form-control selectbox" name="empresa[]" multiple="multiple"  style="width: 100%">
														<?php 
															foreach($empresas as $empresa){
																	echo "<option  value='".$empresa['EMPRESA']."'";
																	if(isset($buscaEmpresa) && in_array($empresa['EMPRESA'], $buscaEmpresa) == 1) echo 'selected';
																	echo ">".$empresa['EMPRESA']."</option>";
															}
                                                        ?>
													</select>
												</div>
											</div>
										</div>
									</div>

									<div class="row">									

									<!-- dias uteis-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="col-sm-4  col-md-offset-2 control-label" for="diautil">Dias Úteis</label>
												<div class="col-sm-6">
													<select class="form-control selectbox" multiple="multiple" name="diautil[]">
														<option value="" <?php if(isset($buscaDiaUtil[0]) && $buscaDiaUtil[0] == '') echo 'selected'?>>Todos</option>
														<option value="0" <?php if(isset($buscaDiaUtil) && in_array(1, $buscaDiaUtil) == 0) echo 'selected'?>>D-1</option>
														<option value="1" <?php if(isset($buscaDiaUtil) && in_array(1, $buscaDiaUtil) == 1) echo 'selected'?>>1º</option>
														<option value="2" <?php if(isset($buscaDiaUtil) && in_array(2, $buscaDiaUtil) == 2) echo 'selected'?>>2º</option>
														<option value="3" <?php if(isset($buscaDiaUtil) && in_array(3, $buscaDiaUtil) == 3) echo 'selected'?>>3º</option>
														<option value="4" <?php if(isset($buscaDiaUtil) && in_array(4, $buscaDiaUtil) == 4) echo 'selected'?>>4º</option>
														<option value="5" <?php if(isset($buscaDiaUtil) && in_array(5, $buscaDiaUtil) == 5) echo 'selected'?>>5º</option>
													</select>
												</div>
											</div>
										</div>
										<!--periodo-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="col-sm-3 control-label" for="periodo">Período</label>
												<div class="col-sm-9">
													<select class="form-control selectbox" name="periodo[]" multiple="multiple" style="width: 100%"><?php
													foreach($periodos as $periodo){
														echo "<option  value='".$periodo['PERIODO']."' ";
														foreach ($instanciasAbertas as $instancia)
														{
															if(isset($buscaPeriodo) && in_array($periodo['PERIODO'], $instancia)) {
																echo 'selected';
																goto saiForeachPeriodo;
															}
														}
														saiForeachPeriodo:
														echo ">".$periodo['PERIODO']."</option>";
													}
                                                                                                                                                    ?>
													</select>
												</div>
											</div>
										</div>

										<!-- departamento-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-sm-3 control-label" for="departamento">Departamento</label>
												<div class="col-md-9">
													<select class="form-control selectbox" name="departamento[]" multiple="multiple" style="width: 100%"><?php
													foreach($departamentos as $departamento){
														echo "<option  value='".$departamento['DEPTO']."'";
														if(isset($buscaDepartamento) && in_array($departamento['DEPTO'], $buscaDepartamento) == 1) echo 'selected';
														echo ">".$departamento['DEPTO']."</option>";
													}
                                                                                                                                                         ?>
													</select>
												</div>
											</div>
										</div>
										</div>
										
										<?php
											## filtro de JV join ventures. Se caso jv seja passado via get, esse parametro assegura que somente dados saquela jv será exibido ao usuario mesmo com uma nova pesquisa
											if (isset($_GET['jv'])) {
												echo '<input class="form-control" type="hidden" name="jv" value="'.$_GET['jv'].'" />';
											}
										
										?>
										
										<div class="row">
											<!-- departamento-->
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-sm-3 control-label" for="departamento">Executor</label>
													<div class="col-md-9">
														<input class="form-control" type="text" name="usuarioExecutor" value="<?php if(isset($buscaUsuarioExecutor)) echo $buscaUsuarioExecutor?>" placeholder="Buscar por usuário que executou uma atividade" />
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-sm-3 control-label" for="departamento">Responsável</label>
													<div class="col-md-9">
														<input class="form-control" type="text" name="usuarioPapel" value="<?php if(isset($buscaUsuarioPapel)) echo $buscaUsuarioPapel?>" placeholder="Buscar por usuário que pode executar uma atividade " />
													</div>
												</div>
											</div>
										</div>


										
										<div class="row">
											<div class="col-md-2 col-md-offset-7">
												<input type="checkbox" name="ASSOCIACOES" /> <i style="color: #aaa;"> Mostrar associações</i>
											</div>
											<div class="col-md-6 col-md-offset-6">
												<input type="checkbox" name="emaberto" <?php if(isset($buscaEmAberto)) echo 'checked'?> /> Somente atividades em aberto
												<button class="btn btn-default col-md-3 pull-right" style="left: -15px;" value="submit">Buscar</button>
											</div>
										</div>


</form>
							</div>
							<div class="panel-body">
								<table class="table">
									<tr><th>Num. Fechamento</th></tr>
                                    <?php
									
									 foreach( $instanciasAbertas AS $instanciaAberta){

										
										echo '<tr style="font-size: 12px;" class="text-left">';
										echo '<td><b>'.$instanciaAberta['IDPROCESS'].'</b> - <a href="'.BASEDIR.'se/instancia/abrirLeitura?instancia='.$instanciaAberta['IDPROCESS'].'">'.$instanciaAberta['NMPROCESS'].'</a></td>';
											echo '<td>';
												echo '<table class="table table-striped">';
												echo '<tr><th>Dia</th>';
												echo '<th>';
												echo '<form method="POST" action="https://se.amaggi.com.br:6244/se/v90663/generic/gn_attachment/attachment_icon_list.php" name="'.$instanciaAberta['CDPROCESS'].'" id="'.$instanciaAberta['CDPROCESS'].'" target="blank">
																<input type="hidden" name="path" value="workflow/gn_attachment/" />
																<input type="hidden" name="cdprod" value="104" />
																<input type="hidden" name="view" value="1" />
																<input type="hidden" name="action" value="2" />
																<input type="hidden" name="classname" value="procattachment" />
																<input type="hidden" name="idprocess" value="'.$instanciaAberta['CDPROCESS'].'" />
																<input type="hidden" name="fgtype" value="1" />
															';
													echo '<button type="Submit" class="btn btn-link"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></button><a>';
													echo '</form>';
												echo '</th>';
												
												echo '<th>Atividade</th>';
												echo '<th>Prazo</th>';
												if (isset($_GET['ASSOCIACOES'])) {
													echo '<th>Atividades Vinculadas</th>';
												}
												echo '<th>Status</th>';
												echo '<th>Responsável</th>';
												echo '<th>Executado</th>';
												echo '<th>Comentário</th>';
												echo '<th>Execução <a href="'.CONTROLLERDIR.'/calculaPrazos?instancia='.$instanciaAberta['IDPROCESS'].'" target="_black" >.</a></th></tr>';

												$anexo = null;
												foreach( $fechamentosAbertos AS $fechamentoAberto)
												{
													if($anexo[1] <> $fechamentoAberto['NMSTRUCT'] || ((!isset($anexo) && $fechamentoAberto['CDATTACHMENT'])) || ($anexo[2] <> $fechamentoAberto['IDPROCESS'])){
														if($fechamentoAberto['IDPROCESS'] == $instanciaAberta['IDPROCESS'] && substr($fechamentoAberto['NMSTRUCT'], 0, 2) <> '41')
														{
															$dateTimeExecution = DateTime::createFromFormat('d/m/YH:i:s', $fechamentoAberto['DTEXECUTION'].$fechamentoAberto['NRTIMEEXECUTION']);
															$dateTimeEstimated = DateTime::createFromFormat('d/m/YH:i:s', $fechamentoAberto['DTESTIMATEDFINISH'].$fechamentoAberto['NRTIMEESTFINISH']);
															
															
															
															$atrasoPrazo = '';
															$imgAtraso = '';
															$atrasoExec = '';
															$dateNow = DateTime::createFromFormat('d/m/YH:i:s', date('d/m/YH:i:s'));
															
															
															//------------------------------status
															//em atraso em aberto
															if(!$fechamentoAberto['DTEXECUTION'] && $fechamentoAberto['DTESTIMATEDFINISH'] && $dateNow > $dateTimeEstimated){
																$atrasoPrazo = ' class="danger"';
																$imgAtraso ="<img title='Atividade ABERTA em ATRASO' src='".ASSETS."atrasoAberta.png' />";
															}
															
															//em atraso já finalizada
															if($dateTimeExecution > $dateTimeEstimated && $dateTimeEstimated){
																$atrasoExec = '';//' class="danger"';
																$atrasoPrazo = ' class="danger"';
																$imgAtraso ="<img title='Atividade ENCERRADA em ATRASO' src='".ASSETS."atrasoFinalizada.png' />";
															}
															
															
															
															//em dia em aberto
															if(!$fechamentoAberto['DTEXECUTION'] && $fechamentoAberto['DTESTIMATEDFINISH'] && $dateNow < $dateTimeEstimated){
																$atrasoPrazo = ' ';
																$imgAtraso ="<img title='Atividade ABERTA em DIA' src='".ASSETS."diaAberta.png' />";
															}
															
															
															
															//em dia já finalizada
															if($dateTimeExecution <= $dateTimeEstimated && $dateTimeEstimated && $imgAtraso == ''){
																//$atrasoExec = '';//' class="danger"';
																//$atrasoPrazo = ' ';
																$imgAtraso ="<img title='Atividade ENCERRADA em DIA' src='".ASSETS."diaFinalizada.png' />";
															}
															//------------------------------status
															
															
															
															
															
															
															
															echo "<tr $atrasoPrazo>";
															if(substr($fechamentoAberto['DIA_UTIL'], 0, 1) <> '0') echo '<td>'.substr($fechamentoAberto['DIA_UTIL'], 0, 1).'º</td>';
															else echo '<td>D-1</td>';
															echo '<td>';
															
																//varre para saber se a atividade possui algum anexo
																foreach ($fechamentosAbertos as $anexoInstancia)
																{
																	if($fechamentoAberto['NMSTRUCT'] == $anexoInstancia['NMSTRUCT'] && $fechamentoAberto['CDATTACHMENT']){
																		// se tiver, adiciona um botao link para abrir o MODAL e usa o atalho GOTO
																		echo '<a data-toggle="modal" data-target="#myModal'.$fechamentoAberto['IDSTRUCT'].'">
																			<span class="glyphicon glyphicon-paperclip" aria-hidden="true"> </span>
																		</a>';
																		goto linkJaAdicionado;
																	}
																}
																linkJaAdicionado:
																
																		
																//modal anexo
																echo '<div class="modal fade" id="myModal'.$fechamentoAberto['IDSTRUCT'].'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																			<div class="modal-dialog" role="document">
																				<div class="modal-content">
																				<div class="modal-header">
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																					<h4 class="modal-title" id="myModalLabel">Anexos da atividade '.$fechamentoAberto['NMSTRUCT'].'</h4>
																				</div>
																				<div class="modal-body">
																					';
																					$anexoAdicionado = array();
																					foreach ($fechamentosAbertos as $anexoInstancia)
																					{
																						if($fechamentoAberto['IDSTRUCT'] == $anexoInstancia['IDSTRUCT'] && $anexoInstancia['CDATTACHMENT'] && array_search($anexoInstancia['CDATTACHMENT'], $anexoAdicionado) === false){
																							echo '<h6>'.$anexoInstancia['EMPRESA'].' - '.'<a href="https://se.amaggi.com.br:6244/se/v90663/generic/gn_attachment/attachment_view_file.php?cdattach='.$anexoInstancia['CDATTACHMENT'].'">'.$anexoInstancia['NMATTACHMENT'].'</a></h6>';
																							//goto escaparAnexo;
																							array_push($anexoAdicionado, $anexoInstancia['CDATTACHMENT']);
																						}
																						
																					}
																					//escaparAnexo:
																				echo '
																				</div>
																				<div class="modal-footer">
																					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
																				</div>
																				</div>
																			</div>
																		</div>';
																		
																		
															
															
															echo '</td>';
															
															echo '<td>'.$fechamentoAberto['NMSTRUCT'].'</td>';
															echo '<td'.$atrasoPrazo.'>'; if($fechamentoAberto['NRTIMEESTFINISH']) echo $fechamentoAberto['DTESTIMATEDFINISH'].' às '.$fechamentoAberto['NRTIMEESTFINISH']; echo "</td>";
															if (isset($_GET['ASSOCIACOES'])) {
																echo "<td>".$fechamentoAberto['ASSOC']."</td>";
															}
															echo "<td>$imgAtraso</td>";
															echo "<td $atrasoPrazo title=\"";
															foreach ($fechamentoAberto['PAPELFUNCIONAL'] as $papelFuncional)
															{
																echo $papelFuncional['NOME'].' ('.$papelFuncional['FUNCAO'].')&#013;';
															}
															
															echo '">'.$fechamentoAberto['NMROLE'].'</td>';
															//echo '<td>'; if($fechamentoAberto['DTEXECUTION']) echo '<b class="text-success">OK</b>'; echo'</td>';
															echo '<td '.$atrasoExec; if($fechamentoAberto['DSCOMMENT']) echo "title='".$fechamentoAberto['DSCOMMENT']."'"; echo'>'; if($fechamentoAberto['NRTIMEEXECUTION']) echo $fechamentoAberto['DTEXECUTION'].' às '.$fechamentoAberto['NRTIMEEXECUTION']; echo '</td>';
															echo '<td'.$atrasoExec.'>'; if($fechamentoAberto['DSCOMMENT']) echo $fechamentoAberto['DSCOMMENT'].'</td>';


															echo '<td>';
															
															foreach ($fechamentoAberto['PAPELFUNCIONAL'] as $papelFuncional)
															{
																if($papelFuncional['LOGIN'] == $usuario['IDLOGIN']){

																	if(!$fechamentoAberto['DTEXECUTION']){
																		$idprocess = $fechamentoAberto['IDPROCESS'];
																		$idstruct = $fechamentoAberto['IDSTRUCT'];
																		$nmstruct = $fechamentoAberto['NMSTRUCT'];
																		$iduser = $usuario['IDUSER'];

																		if($dateNow > $dateTimeEstimated){
																			echo "<a class='btn btn-danger' onclick=\"getForm('$idprocess', '$idstruct', '$iduser', 1, '".addslashes ($nmstruct)."')\">Executar Atividade</a>";
																		}else{
																			echo "<a class='btn btn-default' onclick=\"getForm('$idprocess', '$idstruct', '$iduser', 0, '".addslashes ($nmstruct)."')\">Executar Atividade</a>";
																		}

                                    ?>

																		<script>
																			function getForm(idprocess, idstruct, iduser, atraso, nmstruct) {

																				$.ajax({
																					type: "POST",
																					url: "<?php echo CONTROLLERDIR; ?>/executarAtividade",
																					data: {"IDPROCESS": idprocess,
																							"IDSTRUCT": idstruct,
																							"IDUSER": iduser,
																							"DELAY": atraso,
																							"NMSTRUCT": nmstruct,
																							},
																					success: function (data) {
																					var obj = JSON.parse(data);
																						console.log(obj);
																						if(obj.return >= 0){
																								document.getElementById("modal-body").innerHTML = obj.msg;
																								$('#modalExecucao').modal('show');
																						}

																					}
																				});
																			}

																			function habilitaBotaoExecutar() {
																			console.log('teclou');
																				if ($('#motivo').val().length >= 15) {
																					document.getElementById('botaoExecutarAtividade').removeAttribute("disabled");
																				} else {
																					document.getElementById('botaoExecutarAtividade').setAttribute("disabled", true);
																				}
																			}

																			function botaoClicado(){
																					//document.getElementById('botaoExecutarAtividade').setAttribute("disabled", true);
																					document.getElementById('botaoExecutarAtividade').innerHTML = '<i class="fa fa-cog fa-spin fa-lg fa-fw"></i> Aguarde...';
																			}

																		</script>
									<?php
																	}

																}
																
															}
															echo '</td>';
															echo '</tr>';
														}
														$anexo[0] = $fechamentoAberto['CDATTACHMENT'];
														$anexo[1] = $fechamentoAberto['NMSTRUCT'];
														$anexo[2] = $fechamentoAberto['IDPROCESS'];
													}
														
												}
														
												echo '</table>';
											echo '</td>';
										echo '</tr>';
										  
										  }?>
											
								</table>
										

							</div>
									
									
							<br />
							<!--Nay - Responsavel por aÃ§Ã£o -->
							<div class="container">
								<div class="col-md-3">
									<table class="table" style="font-size: 13px;">
												
												
									</table>
											
								</div>
										
							</div>
							<!--Nay - Responsavel por aÃ§Ã£o -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!-- Modal -->
    <div class="modal fade" id="modalExecucao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content"  id="modal-body">
                
            </div>
        </div>
    </div>

    <script type="text/javascript">
		$(document).ready(function() {
			$(".selectbox").select2();
		});
    </script>
	
</body>



</html>