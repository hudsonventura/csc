<!DOCTYPE html >
<html lang="en">

<head>

    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />
	
	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
	
	
    <title><?php echo $title?></title>
    
	<!--Imports-->
		
		
		<!-- Bootstrap Core CSS --><link href="						<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<!-- jQuery --><script src="								<?php echo DEFAULTVIEWDIR;?>assets/jquery/js/jquery.min.js"></script>
		<!-- Bootstrap Core JS --> <script src="					<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/js/bootstrap.min.js"></script>
		<!-- Custom Fonts --><link href="							<?php echo DEFAULTVIEWDIR;?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

		<!-- FusionCharts --><script type="text/javascript" src="	<?php echo DEFAULTVIEWDIR;?>assets/fusioncharts/js/fusioncharts.js"></script>
		<!-- FusionCharts --><script type="text/javascript" src="	<?php echo DEFAULTVIEWDIR;?>assets/fusioncharts/js/themes/fusioncharts.theme.fint.js"></script>


</head>

<body>
	<div class="panel panel-default">
		<div class="panel-body">
			<table class="table table-condensed table-striped">
				<tr>
					<td></td>

					<?php

					///titulos
					foreach ($dias as $diaKey => $dia){
						$diaUtil = substr($diaKey, 7);

						echo "<th>";
						if ($diaKey === 'DIAUTIL0') {
							echo "D-1 <small>$dia</small>";
						}else{
							echo $diaUtil."º dia <small> $dia</small>";
						}
						echo "</th>";
					}

					//conteudo
					foreach ($listaDepartamentos as $departamento){
						$nomeDepartamento = $departamento['DEPTO'];
						echo "<tr height='10px;'>";
						echo "<th style='text-align: left;'>$nomeDepartamento</th>";
						foreach ($dias as $diaKey => $dia){
							$diaUtil = substr($diaKey, 7);
							foreach ($indicadores as $indicador)
							{
								foreach ($indicador as $indicadorDia)
								{
									if($indicadorDia['DEPTO']==$departamento['DEPTO'] && $indicadorDia['ATIVIDADEDIA']==$dia){

										$class = '';
										$classIndicador = 'success';
										$classIndicadorRestante = 'info';
										$percentRestante = 100;

										$dataArray = explode('/', $indicadorDia['ATIVIDADEDIA']);
										$dataAtual = date('d/m/Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
										$percent = number_format($indicadorDia['ATIVIDADEPERCENT'], 0);
										$count = $indicadorDia['ATIVIDADECOUNT'];
										$restante = $indicadorDia['ATIVIDADETOTAL'] - $indicadorDia['ATIVIDADECOUNT'];

										if ($dataAtual == date('d/m/Y'))
										{
											$class = 'info';
											$classIndicador = 'success';
											$classIndicadorRestante = 'primary progress-bar-striped active';
											$percentRestante = 100-$percent;
										}
										if ($dataAtual < date('d/m/Y'))
										{
											$class = 'success';
											$classIndicador = 'success';
											$classIndicadorRestante = 'danger progress-bar-striped active';
											$percentRestante = 100-$percent;
										}

										if ($dataAtual > date('d/m/Y'))
										{
											$class = '';
											$classIndicador = 'success';
											$classIndicadorRestante = 'primary progress-bar-striped active';
											$percentRestante = 100-$percent;
										}

										if($percentRestante == 100){
											//$percentRestante = 0;
										}

										$hint = '---- Atividades restantes para este dia ----&#013;&#013;';
										foreach ($retornoBanco as $itemHint)
										{
											if($itemHint['DEPTO'] <> '' && $itemHint['DEPTO']==$departamento['DEPTO'] && $itemHint['DTESTIMATEDFINISH']==$dia && ($itemHint['ATIVIDADE_STATUS'] <> 'Encerrada em Dia' && $itemHint['ATIVIDADE_STATUS'] <> 'Encerrada em Atraso')){
												$hint .= 'Empresa: '.$itemHint['EMPRESA'].'&#013;';
												$hint .= 'Atividade: '.$itemHint['NMSTRUCT'].'&#013;';
												$hint .= '&#013;';
											}

										}


										echo "<td class='$class'>";
											if($percent <> '100'){
												if($indicadorDia['ATIVIDADEPERCENT'] == -1){
													echo "<font size='5' class='text-muted'>-</font>";
												}else{
													echo "<div class='progress' style='margin: 0;padding: 0;' title='$hint'>
															<div class='progress-bar progress-bar-$classIndicador' role='progressbar' style='width:$percent%'>$percent%</div>
															<div class='progress-bar progress-bar-$classIndicadorRestante' role='progressbar' style='width:$percentRestante%'>$restante - $percentRestante%</div>
													</div> ";
												}

											}else{
												echo "<div class='progress'  style='margin: 0; padding: 0;' title='$hint'>
															<div class='progress-bar progress-bar-$classIndicador' role='progressbar' style='width:$percent%'>$percent%</div>
															<div class='progress-bar progress-bar-' role='progressbar' style='width:$percentRestante%'>$restante - $percentRestante%</div>
													</div> ";
											}

										echo "</td>";
									}
								}


							}
						}
						echo '</tr>';
					}
					?>
				</tr>
			</table>
			
		</div>
	</div>

	
</body>



</html>