<!DOCTYPE html >
<html lang="en">

<head>

    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />
	
	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
	
	
    <title><?php echo $title?></title>
    
	<!--Imports-->
		
		
		<!-- Bootstrap Core CSS --><link href="<?php echo ASSETS;?>bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<!-- jQuery --><script src="<?php echo ASSETS;?>jquery/js/jquery.min.js"></script>
		<!-- Bootstrap Core JS --> <script src="<?php echo ASSETS;?>bootstrap/js/bootstrap.min.js"></script>
		<!-- Bootsbox --> <script src="<?php echo ASSETS;?>bootstrap/js/bootbox.min.js"></script>
		<!-- Google Charts  --><script src="<?php echo ASSETS;?>GoogleCharts/loader.js"></script>
		<!-- pickadate.js  --><script src="<?php echo ASSETS;?>pickadate/picker.js"></script>
		<!-- pickadate.js  --><script src="<?php echo ASSETS;?>pickadate/picker.date.js"></script>
		<!-- pickadate.js  --><link href="<?php echo ASSETS;?>pickadate\themes/default.css" rel="stylesheet" />
		<!-- pickadate.js  --><link href="<?php echo ASSETS;?>pickadate\themes/default.date.css" rel="stylesheet" />
		<!-- notify  --><script src="<?php echo ASSETS;?>notify/bootstrap-notify.min.js"></script>
</head>

<body>

<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        Hello world!
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer"><button type="button" class="btn btn-primary">OK</button></div>
    </div>
  </div>
</div>


			<div class="container">
				<div class="col-md-12"><ins></ins>
					<div class="panel text-center">
						<div class="panel-heading" style="background-color: #fff;">
							<img src="<?php echo ASSETS;?>logo_csc.png" height="54px" />
						</div>
						<div class="panel-body">
							<h2><?php echo $title?></h2><br />
							
							
							
							<div class="container">
								<form class="form-inline" method="GET">
									  <!--ANO AUDITORIA-->
									  <div class="form-group col-md-3">
										<label>Empresa</label>
											<select name="empresa" id="empresa" class="form-control" >
												<option value='Todas'>Todas</option>
												<?php foreach($empresas as $empresa){
													echo "<option value='".$empresa['EMPRESA']."'>".$empresa['EMPRESA']."</option>";
												}?>
											</select>
									  </div>
									  
									  <!--DIA-->
									  <div class="form-group col-md-2">
									  	<label>1� dia �til</label>
									  		<input class="form-control dia" id="dia" name="dia" value="<?php if(isset($dia)) echo $dia?>" />
									  	</div>
										<script>$('.dia').pickadate({
											format: 'dd/mm/yyyy',
											//min: [<?php echo date('Y').','.date('m').','.date('d')?>],
											disable: [1, 7],
											today: 'Hoje',
											clear: 'Limpar',
											close: 'Fechar',
											weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
											monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
											
											onSet: function(context){
									  											console.log('Just set stuff:', context)
																			},
										})
											
										</script>
										<!--DIA-->
										
										<div class="form-group col-md-2">
											<label>Login</label>
											<input class="form-control" id="login" name="login" />
										</div>
										
										<div class="form-group col-md-2">
											<label>Senha</label>
											<input class="form-control" id="senha" name="senha" type="password" />
										</div>
										
										<div class="form-group col-md-2">
											<label>Iniciar</label>
											<a class="btn btn-default" id="iniciarFechamento">Iniciar Fechamento Cont�bil</a>
										</div>
										<br />
										<div class="col-md-2 col-md-offset-9"><br />
                                            <a href="<?php echo CONTROLLERDIR;?>/index">Ir para o cronograma</a> <br />
                                            <a target="_blank" href="https://se.amaggi.com.br:6244/softexpert/workspace?page=execution,104,1">Ir para o SoftExpert</a>
										</div>
										
										<script>
											$('#iniciarFechamento').click(function() {
												    bootbox.confirm({
																				title: "Confirmar",
																				message: "Deseja iniciar o fechamento contabil para a(s) empresa(s) selecionada(s)?<br> <h4>Aguarde at� todas os fechamentos serem iniciados. Isso pode demorar at� 10 minutos<br><br><p style='color: #F00'>Voc� n�o poder� fechar esta tela dentro deste tempo.</p></h4>",
																				buttons: {
																				   cancel: {
																				       label: '<i class="fa fa-times"></i> Cancelar'
																				   },
																				   confirm: {
																				       label: '<i class="fa fa-check"></i> Confirmar'
																				   }
																				},
																				callback: function (result) {

																				   if(result == true){
																				   	if($('#empresa').val() == 'Todas'){

																				   		$.ajax({
																									type: "GET",
																									url: "<?php echo CONTROLLERDIR?>/listaEmpresas",
																									async: false,
																									data: {	"login" : $('#login').val(),
																												"senha" : $('#senha').val(),
																												"dia" : $('#dia').val()},
																									success: function (data) {
																										console.log(data);
																										resultado = JSON.parse(data);
																									}
																								});
																								console.log(resultado);
																							if(resultado.resultado == '1'){
																								console.log('Varrendo as empresas');
																								for(var i=0; i<resultado.empresas.length; i++) {
																									console.log(resultado.empresas[i]);
																									iniciaProcesso(resultado.empresas[i].EMPRESA);//----------
																								}
																							}else{
																								exibirAlertas(resultado);
																							}

																				   	}else{
																				   		iniciaProcesso($('#empresa').val());//--------
																						}

																				   }
																				}
																			});

											});




											//fila de execução do ajax
											var ajaxManager = {
											    requests: [],
											    addReq: function(opt) {
											        this.requests.push(opt);
											        if (this.requests.length == 1) {
											            this.run();
											        }
											    },
											    removeReq: function(opt) {
											        if($.inArray(opt, requests) > -1)
											            this.requests.splice($.inArray(opt, requests), 1);
											    },
											    run: function() {
											        // original complete callback
											        oricomplete = this.requests[0].complete;
											        // override complete callback
											        var ajxmgr = this;
											        ajxmgr.requests[0].complete = function() {
											             if (typeof oricomplete === 'function')
											                oricomplete();

											             ajxmgr.requests.shift();
											             if (ajxmgr.requests.length > 0) {
											                ajxmgr.run();
											             }
											        };

											        $.ajax(this.requests[0]);
											    },
											    stop: function() {
											        this.requests = [];
											    },
											}
											//fila de execução do ajax





											function iniciaProcesso(empresa){
												ajaxManager.addReq({ //adiciona na fila de execução
													type: "GET",
													url: "<?php echo CONTROLLERDIR?>/instanciarFechamento",
													data: {	"empresa" : empresa,
																"dia" : $('#dia').val(),
																"login" : $('#login').val(),
																"senha" : $('#senha').val()},
													success: function (data) {
														console.log(data);
														resultado = JSON.parse(data);
														exibirAlertas(resultado);
														return true;
													}
												});
											}

											function exibirAlertas(data){
												if(data.resultado == '0'){
													$.notify({
																title: "<b>"+data.hora+' - '+"</b>",
																message: data.empresa+' - '+data.instancia+'<br>'+data.mensagem
															},{
																type: "danger",
																allow_dismiss: true,
																newest_on_top: false,
																timer: null,
															});
												}else{
													$.notify({
																title: "<b>"+data.hora+' - '+"</b>",
																message: data.empresa+' - '+data.instancia+'<br>'+data.mensagem
															},{
																type: "success",
																allow_dismiss: true,
																newest_on_top: false,
																timer: null,
															});
												}
											}



										</script>
										
								</form>
							</div>
							<br />
							
							
							
							
							
							
						</div>
					</div>
				</div>
			</div>				

					



</body>

</html>