<!DOCTYPE html >
<html lang="en">

<head>

    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />
	
	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
	
	
    <title><?php echo $title?></title>
    
	<!--Imports-->
		
		
		<!-- Bootstrap Core CSS --><link href="						<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<!-- jQuery --><script src="								<?php echo DEFAULTVIEWDIR;?>assets/jquery/js/jquery.min.js"></script>
		<!-- Bootstrap Core JS --> <script src="					<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/js/bootstrap.min.js"></script>
		<!-- Custom Fonts --><link href="							<?php echo DEFAULTVIEWDIR;?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

		<!-- FusionCharts --><script type="text/javascript" src="	<?php echo DEFAULTVIEWDIR;?>assets/fusioncharts/js/fusioncharts.js"></script>
		<!-- FusionCharts --><script type="text/javascript" src="	<?php echo DEFAULTVIEWDIR;?>assets/fusioncharts/js/themes/fusioncharts.theme.fint.js"></script>


</head>

<body>

	<!--INDICADOR 1-->
	<script>
	FusionCharts.ready(function(){
		var fusioncharts = new FusionCharts({
			type: 'stackedcolumn2d',
			renderAt: 'indicador1',
			width: '100%',
			height: '650',
			dataFormat: 'json',
			dataSource: {
				"chart": {
					"caption": "Atividades Encerradas",
					"subCaption": "por Empresas",
					//"xAxisname": "Quarter",
					//"yAxisName": "Num. Atividades",
					//"numberPrefix": "$",
					//To show revenue stack in Percentage distribution
					"stack100Percent": "1",
					"decimals": "0",
					//To show value as datavalue and percent value in tooltip
					"showPercentInTooltip": "1",
					"showValues": "1",
					"showPercentValues": "1",
					//Value font color
					"valueFontColor": "#ffffff",

					"theme": "fint"
				},

				"categories": [{
					"category": [
										<?php
								foreach ($indicador1 as $item)
								{
									$titulo = $item['EMPRESA'];
									echo "{'label': '$titulo'},";
								}
                                        ?>
					]

				}],

				"dataset": [{
					"seriesname": "Atividades Encerradas",
					"color": '#4C9ED4',
					"data": [
																		<?php
												foreach ($indicador1 as $item)
												{
													$titulo = $item['EMPRESA'];
													$valor = $item['ATIVIDADECOUNT'];
													echo "{'value': '$valor'},";
												}
                                                                        ?>
							]
				}, {
					"seriesname": ".",
					"color": '#EEFEEE',
					"data": [
																				<?php
												foreach ($indicador1 as $item)
												{
													$titulo = $item['EMPRESA'];
													$valor = $item['ATIVIDADETOTAL'];
													echo "{'value': '$valor'},";
												}
                                                                        ?>
							]
				}]
			}
		}
		);
			fusioncharts.render();
})

	</script>
	








	<div class="panel panel-default">
		<div class="panel-body">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
                        <div id="indicador1">Erro na exibi��o dos dados!</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
</body>



</html>