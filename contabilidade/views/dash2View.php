<!DOCTYPE html >
<html lang="en">

<head>

    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />
	
	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
	
	
    <title><?php echo $title?></title>
    
	<!--Imports-->
		
		
		<!-- Bootstrap Core CSS --><link href="						<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<!-- jQuery --><script src="								<?php echo DEFAULTVIEWDIR;?>assets/jquery/js/jquery.min.js"></script>
		<!-- Bootstrap Core JS --> <script src="					<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/js/bootstrap.min.js"></script>
		<!-- Custom Fonts --><link href="							<?php echo DEFAULTVIEWDIR;?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

		<!-- FusionCharts --><script type="text/javascript" src="	<?php echo DEFAULTVIEWDIR;?>assets/fusioncharts/js/fusioncharts.js"></script>
		<!-- FusionCharts --><script type="text/javascript" src="	<?php echo DEFAULTVIEWDIR;?>assets/fusioncharts/js/themes/fusioncharts.theme.fint.js"></script>


</head>

<body>
	








	<div class="panel panel-default">
		<div class="panel-body">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table table-striped table-condensed">
							<tr>
								<th></th>
								<?php foreach ($listaDepartamentos as $departamento)
									{
										$nomeDepto = $departamento['DEPTO'];
										echo "<th>$nomeDepto</th>";
									}
								?>
							</tr>

							<?php foreach ($listaEmpresas as $empresa)
									{
										$nomeEmpresa = $empresa['EMPRESA'];
										echo "<tr>";
										echo "<th>$nomeEmpresa</th>";
										foreach ($listaDepartamentos as $departamento)
											{
												$nomeDepto = $departamento['DEPTO'];
												foreach ($indicador1 as $atividade)
												{
													if($atividade['EMPRESA'] == $empresa['EMPRESA'] && $atividade['DEPTO'] == $departamento['DEPTO']){
														$indicar = $atividade['ATIVIDADEENCERRADA'];
														echo "<td>$indicar</td>";
													}
												}
											}
										echo "</tr>";
									}
							?>
						</table>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	
</body>



</html>