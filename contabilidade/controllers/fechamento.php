<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
header('Content-Type:text/html;charset=utf-8');

class Fechamento extends ModularCore\Controller{

	function __construct() {
		parent::__construct();

		//definição de usuario
		$this->loadModel("usuarioCSC");
		$user = $this->models->usuarioCSC->getUserAD($_SERVER['REMOTE_ADDR']);
		if(!$user){
			die('Primeiro faca login no no portal https://suporte.amaggi.com.br ou no SoftExpert <span style="color: #FFF">'.$_SERVER['REMOTE_ADDR'].'</span>');
		}
		
		//join ventures por get
		if (isset($_GET['jv'])) {
			
				
			if ($_GET['jv'] == 'ldz' || $_GET['jv'] == 'ldc' || $_GET['jv'] == 'aldc' || $_GET['jv'] == 'aldz') 
				$user[0]['mail'][0] = $user[0]['samaccountname'][0].'@amaggildc.com.br';
				
			if ($_GET['jv'] == 'unidas' || $_GET['jv'] == 'terfron' || $_GET['jv'] == 'unitapajos') 
				$user[0]['mail'][0] = $user[0]['samaccountname'][0].'@unitapajos.com.br';
				
			
		}
		
		
		
		
		$this->coreView['usuario'] = $this->usuario = $user;
		//definição de usuario


		//configuração de periodo para os indicadores
		$da = date('Y-m-d');
		$date = strtotime($da.' -1 months'); /////////////////////alterar
		$this->periodo = date('Y/m', $date);



		$this->loadModel('cronogramaModel');
	}

	function index(){

		$this->coreView['title'] = 'Cronograma do Fechamento Contábil';

		$this->loadLib('session');
		$this->coreView['msg'] = $this->libs->session->getTemp('msg');
		$this->coreView['msgType'] = $this->libs->session->getTemp('msgType');

		if(isset($_FILES['files']['name'][0]) && $_FILES['files']['name'][0] <> '')
		{
			$resultUpload = array();
			for ($i = 0; $i < count($_FILES['files']['name']); $i++)
			{
				date_default_timezone_set("Brazil/East"); //Definindo timezone padrão

				$ext = strtolower(substr($_FILES['files']['name'][$i],-4)); //Pegando extensão do arquivo
				$new_name = date("Y.m.d-H.i.s.") . $ext; //Definindo um novo nome para o arquivo
				$dir = '/uploads/'; //Diretório para uploads

				$filename = substr($_FILES['files']['name'][$i], 0);
				$file = $_FILES['files']['name'][$i];


				$file = MODULEFOLDER."/temp/$filename";
				move_uploaded_file($_FILES['files']['tmp_name'][$i], $file);


				$result = $this->models->cronogramaModel->seUploadFile($_POST['IDPROCESS'], $_POST['IDSTRUCT'], $_POST['IDUSER'], $filename, $file);
				array_push($resultUpload, $result);
				@unlink($file);
			}
			$this->loadLib('session');
			$this->libs->session->setTemp('msg', "teste");

			//fazer um foreach no array de resultado de upload




		}

		if(isset($_POST['IDPROCESS']) && isset($_POST['IDSTRUCT']) && isset($_POST['IDUSER'])){
			$resultExec = $this->models->cronogramaModel->seExecutarAtividade($_POST['IDPROCESS'], $_POST['IDSTRUCT'], $_POST['IDUSER']);
			$resultCalc = $this->models->cronogramaModel->seCalculaPrazos($_POST['IDPROCESS']);
			
			//var_dump($resultExec);
			//var_dump($resultCalc); die();
			
			if($resultExec['Code'] == '1'){
				$return = $this->models->cronogramaModel->seSetComentario($_POST['IDPROCESS'], $_POST['IDSTRUCT'], 'Executado via cronograma: "'.$_POST['motivo'].'"');
				$this->libs->session->setTemp('msgType', 'success');
		    	$this->libs->session->setTemp('msg', "Atividade <b>".$_POST['NMSTRUCT']."</b> executada com sucesso!");
			}else{
				$this->libs->session->setTemp('msgType', 'danger');
		    	$this->libs->session->setTemp('msg', 'Erro SoftExpert: '.$resultExec['Detail']);
			}
			//header('Location: '.FUNCTIONDIR);
			echo '<meta http-equiv="refresh" content="0">';

		}

		//BUSCA
		$buscaEmpresa = array();
		$buscaEmpresa[0] = '';
		if(isset($_GET['empresa']) && $_GET['empresa'] <> ''){
			$buscaEmpresa 					= $_GET['empresa'];
			$this->coreView['buscaEmpresa']	= $_GET['empresa'];
		}

		$buscaDepartamento = array();
		$buscaDepartamento[0] = '';
		if(isset($_GET['departamento']) && $_GET['departamento'] <> ''){
			$buscaDepartamento 					= $_GET['departamento'];
			$this->coreView['buscaDepartamento']	= $_GET['departamento'];
		}

		$buscaAtividade = '';
		if(isset($_GET['atividade']) && $_GET['atividade'] <> ''){
			$buscaAtividade 					= $_GET['atividade'];
			$this->coreView['buscaAtividade']	= $_GET['atividade'];
		}

		$buscaDiaUtil = array();
		$buscaDiaUtil[0] = '';
		if(isset($_GET['diautil'][0])){
			$buscaDiaUtil 					= $_GET['diautil'];
			$this->coreView['buscaDiaUtil']	= $_GET['diautil'];
		}

		$buscaEmAberto = 'off';
		if(isset($_GET['emaberto']) && $_GET['emaberto'] <> ''){
			$buscaEmAberto 				= $_GET['emaberto'];
			$this->coreView['buscaEmAberto']	= $_GET['emaberto'];
		}

		$buscaPeriodo = 'off';
		if(isset($_GET['periodo']) && $_GET['periodo'] <> ''){
			$buscaPeriodo 				= $_GET['periodo'];
			$this->coreView['buscaPeriodo']	= $_GET['periodo'];
		}
		
		//usuario executor
		$buscaUsuarioExecutor = '';
		if(isset($_GET['usuarioExecutor']) && $_GET['usuarioExecutor'] <> ''){
			$buscaUsuarioExecutor 			= "and lower(A.Nmuserupd) like lower('%".$_GET['usuarioExecutor']."%')";
			$this->coreView['buscaUsuarioExecutor']	= $_GET['usuarioExecutor'];
		}
		
		//usuario papel funcional
		$buscaUsuarioPapel = '';
		if(isset($_GET['usuarioPapel']) && $_GET['usuarioPapel'] <> ''){
			$buscaUsuarioPapel 			= $_GET['usuarioPapel'];
			$this->coreView['buscaUsuarioPapel']	= $_GET['usuarioPapel'];
		}
		//BUSCA

		
		
		
		//busca os fechamentos ainda em aberto
		$instanciasAbertas = $this->models->cronogramaModel->getInstanciasAbertas($buscaEmpresa, $buscaAtividade, $buscaDiaUtil, $buscaEmAberto, $buscaPeriodo, $this->usuario);
		$this->coreView['instanciasAbertas'] = $instanciasAbertas;

		//busca os periodos disponiveis
		$periodos = $this->models->cronogramaModel->getPeriodos($this->usuario);
		$this->coreView['periodos'] = $periodos;

		



		//busca os dados dos fechamentos ainda em aberto
		$fechamentosAbertosSemPapel = $this->models->cronogramaModel->getFechamentosAbertos($buscaEmpresa, $buscaAtividade, $buscaDiaUtil, $buscaEmAberto, $buscaPeriodo, $buscaDepartamento, $buscaUsuarioExecutor, $buscaUsuarioPapel, $this->usuario);
		
		//busca as empresas
		$this->coreView['empresas'] = $this->models->cronogramaModel->getEmpresas(null);

		
		
		//busca os departamentos
		$departamentos = $this->models->cronogramaModel->getDepartamentos($this->usuario);
		$this->coreView['departamentos'] = $departamentos;

		$fechamentosAbertos = array();
		$departamentos = array();
		foreach($fechamentosAbertosSemPapel as $fechamentoSemPapel){
			$comPapel = $fechamentoSemPapel;
			$comPapel['PAPELFUNCIONAL'] = $this->models->cronogramaModel->getPessoasDosPapeisFuncionais($fechamentoSemPapel['CDROLE']);
			array_push($fechamentosAbertos, $comPapel);
		}

		$this->coreView['fechamentosAbertos'] = $fechamentosAbertos;

		

		$this->loadView('cronogramaView');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='300;URL=''>";
	}

	function iniciar(){
		$this->coreView['title'] = 'Iniciar Fechamento Contabil';
		$this->coreView['empresas'] = $empresas = $this->models->cronogramaModel->getEmpresas();

		$this->loadView('iniciarView');
	}

	function listaEmpresas(){

		if(isset($_GET['login']) && isset($_GET['senha']) && (isset($_GET['dia']) && $_GET['dia'] <> '') ){
			//verifica login e senha
			$this->loadLib('ActiveDirectory');
			$autenticado = $this->libs->ActiveDirectory->authenticate($_GET['login'], $_GET['senha']);

			if($autenticado == false){
				echo json_encode(array('resultado' => '0', 'mensagem' => 'Usuário ou senha incorretos. Tente novamente', 'instancia' => date('H:i:s'), 'empresa' => '', 'hora'=> date('H:i:s')));
				die();
			}
			echo json_encode(array('resultado' => '1', 'empresas' => $this->models->cronogramaModel->getEmpresas()));
		}else{

			echo json_encode(array('resultado' => '0', 'mensagem' => 'Erro na requisição JSON ao listar as empresas. Preencha todos os dados.', 'instancia' => '', 'empresa' => '', 'hora'=> date('H:i:s')));
			die();
		}
	}

	function instanciarFechamento(){

		if(isset($_GET['empresa']) && (isset($_GET['dia']) && $_GET['dia'] <> '') && isset($_GET['login']) && isset($_GET['senha'])){



			//verifica login e senha
			$this->loadLib('ActiveDirectory');
			$autenticado = $this->libs->ActiveDirectory->authenticate($_GET['login'], $_GET['senha']);

			$empresa= $_GET['empresa'];
			$dia= $_GET['dia'];


			//sleep(45);
			if($autenticado == false){
				echo json_encode(array('resultado' => '0', 'mensagem' => 'Usuário ou senha incorretos. Tente novamente', 'instancia' => date('H:i:s'), 'empresa' => $_GET['empresa'], 'hora'=> date('H:i:s')));
				die();
			}



			$matricula = $this->libs->ActiveDirectory->getUser('samaccountname='.$_GET['login'])['description'][0];
			$result = $this->models->cronogramaModel->seInstanciar($empresa, $dia, $matricula);
			//$result['RecordID'] = 'FEC2017057'; ///força para testes

			if(isset($result['RecordID'])){
				$resultEditaForm = $this->models->cronogramaModel->seEditaForm($empresa, $dia, $result['RecordID']);

				if($resultEditaForm == false){
					//echo $resultEditaForm['Detail'];
					echo json_encode(array('resultado' => '0', 'mensagem' => 'Não foi possivel editar o form', 'instancia' => $result['RecordID'], 'empresa' => $empresa, 'hora'=> date('H:i:s')));
					die();
				}

				//$resultEnviarFechamento = $this->models->cronogramaModel->seEnviarFechamento($result['RecordID'],$matricula);

				//$this->libs->console->write($resultEnviarFechamento);
				//if($resultEnviarFechamento['Code'] < 0){
					//echo @$resultEnviarFechamento['Detail'];
					echo json_encode(array('resultado' => '0', 'mensagem' => "Processo foi instanciado, mas a execução deve ser manual.", 'instancia' => $result['RecordID'], 'empresa' => $empresa, 'hora'=> date('H:i:s')));
					die();
				//}

				echo json_encode(array('resultado' => '1', 'mensagem' => 'Processo instanciado com sucesso', 'instancia' => $result['RecordID'], 'empresa' => $empresa, 'hora'=> date('H:i:s')));
			}else{
				echo json_encode(array('resultado' => '0', 'mensagem' => 'Erro ao instanciar o processo', 'instancia' => '', 'empresa' => $empresa, 'hora'=> date('H:i:s')));
				die();
			}
		}else{
			//var_dump($_GET);
			echo json_encode(array('resultado' => '0', 'mensagem' => 'Erro na requisição JSON ao iniciar o fechamento. Preencha todos os dados.', 'instancia' => '', 'empresa' => '', 'hora'=> date('H:i:s')));
		}

	}

	function executarAtividade(){
		if(isset($_POST['IDPROCESS']) && isset($_POST['IDSTRUCT']) && isset($_POST['IDUSER'])){
			//echo($_POST['DELAY']);
			if (isset($_POST['DELAY']) && $_POST['DELAY'] >= 1)
			{
				$textArea = '<textarea class="form-control" rows="9" name="motivo" id="motivo" onkeyup="habilitaBotaoExecutar();" Placeholder="Informe o motivo do atraso da entrega da atividade (pelo menos 15 caracteres) para liberar o botão de EXECUCAO"></textarea>';
				$botao = '<button class="btn btn-danger pull-right" id="botaoExecutarAtividade" onclick="botaoClicado()" disabled="" type="SUBMIT">Executar Atividade</button>';
			}else{
				$textArea = '<textarea class="form-control" name="motivo" id="motivo"  rows="9" Placeholder="Inserir informacoes perminentes (nao necessario)"></textarea>';
				$botao = '<button class="btn btn-danger pull-right" id="botaoExecutarAtividade" onclick="botaoClicado()" type="SUBMIT">Executar Atividade</button>';
			}




			$retorno = '<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"><b>'.$_POST['NMSTRUCT'].'</b></h4>
						</div>
							<div class="modal-body" style="height:300px">
								<div class"container">
								<form class="form-horizontal col-md-10 col-md-offset-1" method="POST" action="" enctype="multipart/form-data">
									<div class="form-group">
										'.$textArea.'
									</div>
									<input type="hidden" name="IDPROCESS" value="'.$_POST['IDPROCESS'].'" />
									<input type="hidden" name="IDSTRUCT" value="'.$_POST['IDSTRUCT'].'" />
									<input type="hidden" name="IDUSER" value="'.$_POST['IDUSER'].'" />
									<input type="hidden" name="NMSTRUCT" value="'.$_POST['NMSTRUCT'].'" />
									'.$botao.'
									<span class="btn btn-success fileinput-button">
										<i class="glyphicon glyphicon-plus"></i>
										<span>Anexar Arquivos...</span>
										<input id="files" type="file" name="files[]" multiple>
										<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
									</span>
								</form>
							</div>
						</div>
						';

			echo json_encode(array('return'=>'1', 'msg'=> $retorno));
		}else{
			echo json_encode(array('return'=>'-1', 'msg'=>'Erro na requisicao'));
		}
	}
	
	function calculaPrazos($instancia = null){
		if (!$instancia) {
			$instancia = $_GET['instancia'];
		}
		$resultCalc = $this->models->cronogramaModel->seCalculaPrazos($instancia);
		
		if ($resultCalc && !isset($_GET['instancia'])) {
			return array('sucesso'=>'1', 'mensagem'=>'Calculo de prazos executado com sucesso!');
		}else{
			return array('sucesso'=>'1', 'mensagem'=>'Houve algum erro ao calcular os prazos');
		}
		
		
	}
	
	function webService(){
		
		$this->loadLib('nusoap');
		$this->server = new \nusoap_server();
		$this->server->configureWSDL('Fechamento','urn:hudsonventura.com.br');
		$this->server->wsdl->schemaTargetNamespace = 'urn:ModularCore';

		$this->server->register('calculaPrazos',
									array('instancia'=>'xsd:string'),
									array('sucesso'=>'xsd:string', 'mensagem'=>'xsd:string'));



		$data = file_get_contents('php://input');
		$data = isset($data) ? $data : '';
		$this->server->soap_defencoding = 'utf-8';
		$this->server->service($data);
		die();
	}




















	function indicadores(){
		$this->coreView['title'] = 'Indicadores';
		$this->loadView('indicadoresView');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='80;URL=''>";
	}
	
	function retornoBancoIndicadores(){
		return $this->models->cronogramaModel->getFechamentosAbertosIndicadores(null, null, null, null, array($this->periodo), null, null, null, $this->usuario);
	}

	/////////////////////// DASH1

	function dash1(){
		$this->coreView['title'] = 'Indicadores por Departamento';



		//get SUPRIMENTOS PARA OS INDICADORES
		$this->coreView['retornoBanco'] = $retornoBanco = $this->retornoBancoIndicadores();
		$this->coreView['listaDepartamentos'] = $listaDepartamentos = $this->models->cronogramaModel->getDepartamentosUnicos($retornoBanco);
		


		//get INDICADORES
		$this->coreView['indicador1'] = $this->Dash1Indicador1($retornoBanco, $listaDepartamentos);
		$this->coreView['indicador2'] = $this->Dash1Indicador2($retornoBanco, $listaDepartamentos);


		$this->loadView('dash1View');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='60;URL=''>";
	}


	private function Dash1Indicador1($retornoBanco, $listaDepartamentos){
		$indicador = array();
		//$indicador[0] = array('DEPTO' => 'Fiscal', 'ATIVIDADEDIA' => 0, 'ATIVIDADEATRASO' => 0, 'ATIVIDADETOTAL' => 0);
		foreach ($listaDepartamentos as $departamento)
		{
			//echo '<b>'.$departamento['DEPTO'].'</b><br>';
			$countDia = 0;
			$countAtraso = 0;
			$total = 0;

			
			foreach ($retornoBanco as $atividade)
			{

				if($atividade['DEPTO'] == $departamento['DEPTO']){// && substr($departamento['DEPTO'],0, 16) <> 'Fiscal Indiretos'){
					
					switch ($atividade['ATIVIDADE_STATUS'])
					{
						case 'Aberta em Dia': $countDia ++; break;
						case 'Aberta em Atraso': $countAtraso ++; break;
						default: $total++;
					}
					
				}
				
			}
			array_push($indicador, array('DEPTO' => $departamento['DEPTO'], 'ATIVIDADEDIA' => $countDia, 'ATIVIDADEATRASO' => $countAtraso, 'ATIVIDADETOTAL' => $total));
		}
		return $indicador;



	}

	private function Dash1Indicador2($retornoBanco, $listaDepartamentos){
		$indicador = array();
		foreach ($listaDepartamentos as $departamento)
		{
			$countDia = 0;
			$countAtraso = 0;
			$total = 0;

			foreach ($retornoBanco as $atividade)
			{
				if($atividade['DEPTO'] == $departamento['DEPTO']){
					switch ($atividade['ATIVIDADE_STATUS'])
					{
						case 'Encerrada em Dia': $countDia ++; break;
						case 'Encerrada em Atraso': $countAtraso ++; break;
						default: $total++;
					}
				}
			}
			array_push($indicador, array('DEPTO' => $departamento['DEPTO'], 'ATIVIDADEDIA' => $countDia, 'ATIVIDADEATRASO' => $countAtraso, 'ATIVIDADETOTAL' => $total));
		}
		return $indicador;



	}


	/////////////////////// DASH1







	/////////////////////// DASH2
	function dash2(){
		$this->coreView['title'] = 'Dash 2';


		//get SUPRIMENTOS PARA OS INDICADORES
		$this->coreView['retornoBanco'] = $retornoBanco = $this->retornoBancoIndicadores();
		$this->coreView['listaDepartamentos'] = $listaDepartamentos = $this->models->cronogramaModel->getDepartamentosUnicos($retornoBanco);
		$this->coreView['listaEmpresas'] = $listaEmpresas = $this->models->cronogramaModel->getEmpresas($retornoBanco);


		//get INDICADORES
		$this->coreView['indicador1'] = $indicador1 = $this->Dash2Indicador1($retornoBanco, $listaDepartamentos, $listaEmpresas);




		$this->loadView('dash2View');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='60;URL=''>";
	}

	private function Dash2Indicador1($retornoBanco, $listaDepartamentos, $listaEmpresas){
		$indicador = array();

		foreach ($listaEmpresas as $empresa)
		{


			foreach ($listaDepartamentos as $departamento)
			{
				$countEncerrada = 0;
				$total = 0;
				foreach ($retornoBanco as $atividade)
				{
					if($atividade['DEPTO'] == $departamento['DEPTO'] && $atividade['EMPRESA'] == $empresa['EMPRESA']){
						if(substr($atividade['ATIVIDADE_STATUS'], 0, 9) == 'Encerrada'){
							$countEncerrada ++;
						}
						$total++;
					}
				}
				$addValue = @(int)($countEncerrada/$total*100);
				switch ($addValue)
				{
					case 100: $addValue = "<b class='text-success'>$addValue%</b>"; break;
					case 0: $addValue = "<b class='text-danger'>$addValue%</b>"; break;
					default: $addValue = "<b class='text-muted'>$addValue%</b>";
				}


				if($total == 0){
					$addValue = '-';
				}





				array_push($indicador, array('EMPRESA' => $empresa['EMPRESA'], 'DEPTO' => $departamento['DEPTO'], 'ATIVIDADEENCERRADA' => $addValue));
			}



			//var_dump($indicador);
		}


		return $indicador;
	}
	/////////////////////// DASH2









	/////////////////////// DASH3
	function dash3(){
		$this->coreView['title'] = 'Indicadores por Empresa';


		//get SUPRIMENTOS PARA OS INDICADORES
		$this->coreView['retornoBanco'] = $retornoBanco = $this->retornoBancoIndicadores();
		$this->coreView['listaEmpresas'] = $listaEmpresas = $this->models->cronogramaModel->getEmpresas($retornoBanco);



		//get INDICADORES
		$this->coreView['indicador1'] = $indicador1 = $this->Dash3Indicador1($retornoBanco, $listaEmpresas);
		$this->coreView['indicador2'] = $indicador2 = $this->Dash3Indicador2($retornoBanco, $listaEmpresas);




		$this->loadView('dash3View');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='60;URL=''>";
	}

	private function Dash3Indicador1($retornoBanco, $listaEmpresas){
		$indicador = array();
		foreach ($listaEmpresas as $empresa)
		{
			$countDia = 0;
			$countAtraso = 0;
			$total = 0;

			foreach ($retornoBanco as $atividade)
			{
				if($atividade['EMPRESA'] == $empresa['EMPRESA']){
					switch ($atividade['ATIVIDADE_STATUS'])
					{
						case 'Aberta em Dia': $countDia ++; break;
						case 'Aberta em Atraso': $countAtraso ++; break;
						default: $total++;
					}
				}

			}
			array_push($indicador, array('EMPRESA' => $empresa['EMPRESA'], 'ATIVIDADEEMDIA' => $countDia, 'ATIVIDADEEMATRASO' => $countAtraso, 'ATIVIDADETOTAL' => $total));
		}
		return $indicador;
	}

	private function Dash3Indicador2($retornoBanco, $listaEmpresas){
		$indicador = array();
		foreach ($listaEmpresas as $empresa)
		{
			$countDia = 0;
			$countAtraso = 0;
			$total = 0;

			foreach ($retornoBanco as $atividade)
			{
				if($atividade['EMPRESA'] == $empresa['EMPRESA']){
					switch ($atividade['ATIVIDADE_STATUS'])
					{
						case 'Encerrada em Dia': $countDia ++; break;
						case 'Encerrada em Atraso': $countAtraso ++; break;
						default: $total++;
					}
				}
			}
			array_push($indicador, array('EMPRESA' => $empresa['EMPRESA'], 'ATIVIDADEDIA' => $countDia, 'ATIVIDADEATRASO' => $countAtraso, 'ATIVIDADETOTAL' => $total));
		}
		return $indicador;
	}
	/////////////////////// DASH3





	/////////////////////// DASH4
	function dash4(){
		$this->coreView['title'] = 'Indicadores por Empresa';


		//get SUPRIMENTOS PARA OS INDICADORES
		$this->coreView['retornoBanco'] = $retornoBanco = $this->retornoBancoIndicadores();
		$this->coreView['listaEmpresas'] = $listaEmpresas = $this->models->cronogramaModel->getEmpresas($retornoBanco);



		//get INDICADORES
		$this->coreView['indicador1'] = $indicador1 = $this->Dash4Indicador1($retornoBanco, $listaEmpresas);




		$this->loadView('dash4View');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='60;URL=''>";
	}

	private function Dash4Indicador1($retornoBanco, $listaEmpresas){
		$indicador = array();
		foreach ($listaEmpresas as $empresa)
		{
			$count = 0;
			$total = 0;

			foreach ($retornoBanco as $atividade)
			{
				if($atividade['EMPRESA'] == $empresa['EMPRESA']){
					switch ($atividade['ATIVIDADE_STATUS'])
					{
						case 'Encerrada em Dia': $count ++; break;
						case 'Encerrada em Atraso': $count ++; break;
						default: $total++;
					}
				}

			}
			array_push($indicador, array('EMPRESA' => $empresa['EMPRESA'], 'ATIVIDADECOUNT' => $count, 'ATIVIDADETOTAL' => $total));
		}
		return $indicador;
	}

	/////////////////////// DASH4



	/////////////////////// DASH5
	function dash5(){
		$this->coreView['title'] = 'Indicadores por Empresa';

		$periodo = $this->periodo;

		//get SUPRIMENTOS PARA OS INDICADORES
		$this->coreView['retornoBanco'] = $retornoBanco = $this->retornoBancoIndicadores();
		$this->coreView['listaEmpresas'] = $listaEmpresas = $this->models->cronogramaModel->getEmpresas($retornoBanco);
		$this->coreView['dias'] = $dias = $this->models->cronogramaModel->getDiasUteis($periodo)[0];
		
		

		//get INDICADORES
		$indicadores = array();
		foreach ($listaEmpresas as $empresa)
		{
			array_push($indicadores, $this->Dash5Indicadores($retornoBanco,$empresa, $dias));
		}
		$this->coreView['indicadores'] = $indicadores;


		//var_dump($indicadores); die();
		$this->loadView('dash5View');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='60;URL=''>";
	}

	private function Dash5Indicadores($retornoBanco, $empresa, $dias){
		$indicador = array();
		

			foreach ($dias as $diaKey => $dia)
			{
				$count = 0;
				$total = 0;
				foreach ($retornoBanco as $atividade)
				{
					//if($atividade['EMPRESA'] == $empresa['EMPRESA'] && $atividade['DTESTIMATEDFINISH'] == $dia){
						if($atividade['EMPRESA'] == $empresa['EMPRESA'] && 'DIAUTIL'.$atividade['DIA_UTIL'] == $diaKey){	
						$total++;
						switch ($atividade['ATIVIDADE_STATUS'])
						{
							case 'Encerrada em Dia': $count ++; break;
							case 'Encerrada em Atraso': $count ++; break;
						}
					}

				}
				if($total > 0){
					array_push($indicador, array(	'EMPRESA' => $empresa['EMPRESA'], 
													'ATIVIDADEPERCENT' => $count/$total*100, 
													'ATIVIDADECOUNT' => $count, 
													'ATIVIDADETOTAL' => $total, 
													'ATIVIDADEDIA' => $dia, 
													'ATIVIDADEDIAUTIL' => $diaKey, 
													'ATIVIDADEDIACORRENTE' => $atividade[$diaKey]));
				}else{
					array_push($indicador, array(	'EMPRESA' => $empresa['EMPRESA'], 
													'ATIVIDADEPERCENT' => -1, 
													'ATIVIDADECOUNT' => -1, 
													'ATIVIDADEDIA' => $dia, 
													'ATIVIDADEDIAUTIL' => $diaKey, 
													'ATIVIDADETOTAL' => -1, 
													'ATIVIDADEDIACORRENTE' => $atividade[$diaKey]));
				}

			}
			return $indicador;
	}

	/////////////////////// DASH5




	/////////////////////// DASH6
	function dash6(){
		$this->coreView['title'] = 'Indicadores por Departamento';

		$periodo = $this->periodo;

		//get SUPRIMENTOS PARA OS INDICADORES
		$this->coreView['retornoBanco'] = $retornoBanco = $this->retornoBancoIndicadores();
		$this->coreView['listaDepartamentos'] = $listaDepartamentos = $this->models->cronogramaModel->getDepartamentosUnicos($retornoBanco);
		$this->coreView['dias'] = $dias = $this->models->cronogramaModel->getDiasUteis($periodo)[0];



		//get INDICADORES
		$indicadores = array();
		foreach ($listaDepartamentos as $departamento)
		{
			array_push($indicadores, $this->Dash6Indicadores($retornoBanco,$departamento, $dias));
		}
		$this->coreView['indicadores'] = $indicadores;



		//var_dump($listaDepartamentos); die();
		$this->loadView('dash6View');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='60;URL=''>";
	}

	private function Dash6Indicadores($retornoBanco, $departamento, $dias){
		$indicador = array();

		$indicador[0] = array('DEPTO' => 'Fiscal', 'ATIVIDADEPERCENT' => 0, 'ATIVIDADEDIA' => 0, 'ATIVIDADEDIACORRENTE' => 0, 'ATIVIDADECOUNT' => 0, 'ATIVIDADETOTAL' => 0);
		foreach ($dias as $diaKey => $dia)
		{
			$diaUtil = substr($diaKey, 7);
			$count = 0;
			$total = 0;
			$indicador[0]['ATIVIDADETOTAL'] = 0;
			$indicador[0]['ATIVIDADECOUNT'] = 0;
			foreach ($retornoBanco as $atividade)
			{
				//var_dump($dia);
				if(substr($atividade['DEPTO'],0, 16) == substr($departamento['DEPTO'],0, 16) && $atividade['DTESTIMATEDFINISH'] == $dia){
					if($atividade['DEPTO'] == $departamento['DEPTO'] && substr($atividade['DEPTO'],0, 16) <> 'Fiscal'){
						$total++;
						switch ($atividade['ATIVIDADE_STATUS'])
						{
							case 'Encerrada em Dia': $count ++; break;
							case 'Encerrada em Atraso': $count ++; break;
						}
					}

					if(substr($atividade['DEPTO'],0, 16) == 'Fiscal'){
						$indicador[0]['ATIVIDADETOTAL']++;

						switch ($atividade['ATIVIDADE_STATUS'])
						{
							case 'Encerrada em Dia': $indicador[0]['ATIVIDADECOUNT']++; break;
							case 'Encerrada em Atraso': $indicador[0]['ATIVIDADECOUNT']++; break;
						}
					}
				}

			}


			if(substr($departamento['DEPTO'],0, 16) <> 'Fiscal'){
				if($total > 0){
					array_push($indicador, array('DEPTO' => $departamento['DEPTO'], 'ATIVIDADEPERCENT' => $count/$total*100, 'ATIVIDADECOUNT' => $count, 'ATIVIDADETOTAL' => $total, 'ATIVIDADEDIA' => $dia, 'ATIVIDADEDIACORRENTE' => $atividade[$diaKey]));
				}else{
					array_push($indicador, array('DEPTO' => $departamento['DEPTO'], 'ATIVIDADEPERCENT' => -1, 'ATIVIDADECOUNT' => -1, 'ATIVIDADETOTAL' => -1, 'ATIVIDADEDIA' => $dia, 'ATIVIDADEDIACORRENTE' => $atividade[$diaKey]));
				}

			}else{

				if($indicador[0]['ATIVIDADETOTAL'] > 0){
					array_push($indicador, array('DEPTO' => $departamento['DEPTO'], 'ATIVIDADEPERCENT' => $indicador[0]['ATIVIDADECOUNT']/$indicador[0]['ATIVIDADETOTAL']*100, 'ATIVIDADECOUNT' => $indicador[0]['ATIVIDADECOUNT'], 'ATIVIDADETOTAL' => $indicador[0]['ATIVIDADETOTAL'], 'ATIVIDADEDIA' => $dia, 'ATIVIDADEDIACORRENTE' => $atividade[$diaKey]));
				}else{
					array_push($indicador, array('DEPTO' => $departamento['DEPTO'], 'ATIVIDADEPERCENT' => -1, 'ATIVIDADECOUNT' => -1, 'ATIVIDADEDIA' => $dia, 'ATIVIDADETOTAL' => -1, 'ATIVIDADEDIACORRENTE' => $atividade[$diaKey]));
				}
			}
		}
		return $indicador;
	}

	/////////////////////// DASH6
}