<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class Kri extends ModularCore\Controller{

	function __construct() {
		parent::__construct();
		$this->loadModel('kriModel');
	}

	function index(){

		//definição de usuario
		$this->loadModel("usuarioCSC");
		$user = $this->models->usuarioCSC->getUserAD($_SERVER['REMOTE_ADDR']);
		if(!$user){
			die('Primeiro faca login no SoftExpert <span style="color: #FFF">'.$_SERVER['REMOTE_ADDR'].'</span>');
		}
		$this->usuario = $user[0];
		$this->coreView['usuario'] = $this->usuario;
		//definição de usuario


		if(isset($_FILES["arquivo"])){
			foreach ($_FILES["arquivo"]["error"] as $key => $error) {
				if ($error == UPLOAD_ERR_OK) {
					$tmp_name = $_FILES["arquivo"]["tmp_name"][$key];
					$name = $_FILES["arquivo"]["name"][$key];
					move_uploaded_file($tmp_name, MODULEFOLDER.'\\models\\'."$name");
				}
			}
		}


		$this->loadLib('cache');
		$this->loadLib('benchmark');

		try
		{

			$unidades = array();

			//UNIDADE
				foreach($_GET as $key => $value){
					if(strpos($key, 'un') === 0){
						array_push($unidades, str_replace('_', ' ', substr($key, 3)));
					}
				}



			//ANO AUDITORIA
			if(isset($_GET['ano']) && $_GET['ano'] > '') {
				$ano = $_GET['ano'];
			} else {
				$ano = "%";
			}


			//ANO KRI
			if(isset($_GET['anokri']) && $_GET['anokri'] > '')
			{
				$anoKri = $_GET['anokri'];
			} else
			{
				$anoKri = '';
			}


			if(isset($_GET['deleteCache'])){

				if($this->models->kriModel->deleteCache()){
					$this->libs->session->setTemp('msg', "Cache deletado com sucesso");
					$this->libs->session->setTemp('msgType', 'success');
					header('Location: '.BASEDIR);
				}else{
					$this->libs->session->setTemp('msg', "Houve um erro ao deletar o cache");
					$this->libs->session->setTemp('msgType', 'danger');
					header('Location: '.BASEDIR);
				}

			}

			$this->coreView['anokri']		= $anoKri;
			$this->coreView['ano']			= $ano;
			$this->coreView['unidades'] 	= $unidades = $this->array_change_key_case_recursive($this->models->kriModel->getUnidadesMD5($unidades));



			//obter a informação de qual empresa se trata e troca a logo
			$logo = 'LogoAmaggi.gif';
			foreach ($unidades as $unidade)
			{
				if(strpos(strtolower($unidade), 'unitapaj') === 0){
					$logo = 'LogoUnidas.jpg';

					$this->loadLib('Permission');
					$this->libs->Permission->required = array('@unitapajos.com.br', 'hudson.ventura', 'cinthia.leite', 'nayara.silva', 'vanessa.bresolin', 'sergio.pizzatto', 'dante.pozzi', 'vilmar.serafini', 'victor.veiga');
					$this->libs->Permission->verify(array(substr($this->usuario['mail'][0], strpos($this->usuario['mail'][0], '@')),  $this->usuario['samaccountname'][0]));

					$this->coreView['usuario'] = $this->usuario;
				}
			}
			$this->coreView['logo'] = $logo;


			$this->coreView['filiais']		= $filiais	= $this->array_change_key_case_recursive($this->models->kriModel->getUnidades());
			$this->coreView['safras']		= $safras	= $this->array_change_key_case_recursive($this->models->kriModel->getAnosSafras());
			$this->coreView['KRIs']			= $KRIs		= $this->array_change_key_case_recursive($this->models->kriModel->getKri());
			$this->coreView['dataArea']		= $dataArea	= $this->array_change_key_case_recursive($this->models->kriModel->getAreas());
			$this->coreView['anosSafras']	= $data		= $this->array_change_key_case_recursive($this->models->kriModel->getKRICompleto($unidades, $ano, $anoKri));



			//variaveis
			$total = array();
			$nc = array();
			$nc30 = array();
			$nc90 = array();
			$reincidente = array();
			$encerrados = array();
			$andamento = array();
			$controlesAndamentoEmdia = array();
			$controlesAndamentoEmatraso = array();
			$controlesEncerradosEmdia = array();
			$controlesEncerradosEmatraso = array();
			$semPlano = array();
			$responsavelPlano = array();
			$responsavelAcao = array();
			$responsavelPlanoIDNC = array();
			$selecionadasUnidades = array();
			$naoaplicaveis = array();
			$arrayteste = array();


			file_put_contents("cache_file", serialize($data));

			$data = unserialize(file_get_contents("cache_file"));

			if(count($data) == 0)
			{
				$this->coreView['total'] = array();
				$this->loadView('relatorioView');
				return true;
			}


			foreach($data as $row)
			{
				//bloco que desconsidera a contagem das NC com varias ações de diferentes setores.
				foreach($total as $verificaSeExisteID ){
					if($row['id da nc'] == $verificaSeExisteID['id da nc'] && $verificaSeExisteID['id da nc'] <> null){
						goto escapar;
					}
				}//bloco que desconsidera a contagem das NC com varias ações de diferentes setores.

					
					array_push($selecionadasUnidades, $row['filial']);
					
					//conta total de controles
					array_push($total, $row);
					
					//nao aplicaveis
					if($row['naoaplicavel'] == 1){
						array_push($naoaplicaveis, $row['id da nc']);
					}
					
					//conta nao conformidades
					if($row['id da nc'] && $row['naoaplicavel'] <> 1){
						array_push($nc, $row['id da nc']);
					}

					//conta +30
					if($row['status dias atraso'] == 'Maior 30 dias' && $row['naoaplicavel'] <> 1){
						array_push($nc30, $row['id da nc']);
					}

					//conta +90
					if($row['status dias atraso'] == 'Maior 90 dias' && $row['naoaplicavel'] <> 1){
						array_push($nc90, $row['id da nc']);
					}

					//reincidente
					if($row['reincidente'] == 'Reincidente' && $row['naoaplicavel'] <> 1){
						array_push($reincidente, $row['id da nc']);
					}

					//encerradas
					if($row["status da nc"] == 'Encerrada' && $row['naoaplicavel'] <> 1){
						array_push($encerrados, $row['id da nc']);
					}

					//em andamento
					if(($row["status da nc"] == 'Em andamento' || strpos($row["status do tnc"], 'Sem plano')=== 0) && $row['naoaplicavel'] <> 1){
						array_push($andamento, $row["status da nc"]);
					}


					//aberto em dia
					if(($row["status do tnc"] == 'Em andamento em dia' || $row["status do tnc"] == 'Aprovacao do planejamento em dia' || $row["status do tnc"] == 'Planejamento em dia') && $row['naoaplicavel'] <> 1){
						array_push($controlesAndamentoEmdia, $row["status da nc"]);
						array_push($arrayteste, $row);
						//coleta a area do responsavel pelas ações
						array_push($responsavelPlanoIDNC, $row["id da nc"]);

					}
					//aberto em atrazo
					if(($row["status do tnc"] == 'Em andamento em atraso' || $row["status do tnc"] == 'Aprovacao do planejamento em atraso' || $row["status do tnc"] == 'Planejamento em atraso') && $row['naoaplicavel'] <> 1){
						array_push($controlesAndamentoEmatraso, $row["status da nc"]);
						
						//coleta a area do responsavel pelas ações
						array_push($responsavelPlanoIDNC, $row["id da nc"]);
					}

					//encerrado em dia
					if($row["status do tnc"] == 'Encerrada em dia' && $row['naoaplicavel'] <> 1){
						array_push($controlesEncerradosEmdia, $row["status da nc"]);
						array_push($responsavelPlanoIDNC, $row["id da nc"]);
					}
					//encerrado em atrazo
					if($row["status do tnc"] == 'Encerrada em atraso' && $row['naoaplicavel'] <> 1){
						array_push($controlesEncerradosEmatraso, $row["status da nc"]);
						//coleta a area do responsavel pelas ações
						array_push($responsavelPlanoIDNC, $row["id da nc"]);
					}

					//sem prazo/ sem plano
					if(strpos($row["status do tnc"], 'Sem plano')=== 0 && $row['naoaplicavel'] <> 1){
						array_push($semPlano, $row["id plano"]);
					}

					//coleta a area do responsavel pelo plano
					if($row["status da nc"] == 'Em andamento' && $row['naoaplicavel'] <> 1){
						//array_push($responsavelplano, $row["area responsavel plano"]);
					}
					//var_dump($row["area responsavel plano"].' - '.$row["status da nc"]); echo '<br />';
					if(($row["area responsavel plano"] <> null && ($row["status da nc"] == 'Em andamento' || $row["status da nc"] == 'Sem plano')) && $row['naoaplicavel'] <> 1){

						array_push($responsavelPlano, $row["area responsavel plano"]);
					}

				escapar:
			}

			$this->coreView['selecionadasUnidades'] 	= $selecionadasUnidades;
			
			

			//GERAÇÃO DAS ESTATISTICAS BASICAS PARA OS INDICADORES
			$controlesTotal = count($total);
			$this->coreView['nc'] = $nc;
			if(count($nc) > 0){
				$controlesEfetivos = $controlesTotal-count($nc)-count($naoaplicaveis);
				$controlesEfetivosPercent = number_format($controlesEfetivos/$controlesTotal*100, 0);

				$controlesNaoEfetivos = count($nc);
				$controlesNaoEfetivosPercent = number_format($controlesNaoEfetivos/$controlesTotal*100, 0);

				$controlesEncerrados = count($encerrados);
				$controlesEncerradosPercent = number_format($controlesEncerrados/$controlesNaoEfetivos*100, 0);

				$controlesEncerradosEmdiaContagem = count($controlesEncerradosEmdia);
				$controlesEncerradosEmdiaContagemPercent = number_format($controlesEncerradosEmdiaContagem/$controlesNaoEfetivos*100, 0);
				$controlesEncerradosEmatrasoContagem = count($controlesEncerradosEmatraso);
				$controlesEncerradosEmatrasoContagemPercent = number_format($controlesEncerradosEmatrasoContagem/$controlesNaoEfetivos*100, 0);

				$controlesAndamento = count($andamento);
				$controlesAndamentoPercent = number_format($controlesAndamento/$controlesNaoEfetivos*100, 0);


				$controlesAtraso = count($controlesAndamentoEmatraso);
				$controlesAtrasoPercent = number_format($controlesAtraso/$controlesNaoEfetivos*100, 0);

				$controlesPrazo = count($controlesAndamentoEmdia);
				$controlesPrazoPercent = number_format($controlesPrazo/$controlesNaoEfetivos*100, 0);

				$controlesSemPlano = count($semPlano);
				$controlesSemPlanoPercent = number_format($controlesSemPlano/$controlesNaoEfetivos*100, 0);

				$naoReincidencia = round(count($total)-count($reincidente));
				$naoReincidenciaPercent = round($naoReincidencia*100/count($total));
				$reincidencia = count($reincidente);

				$controlesImplementadosPercent = round($controlesEfetivos/$controlesTotal*100);
				
				$controlesNaoAplicaveis = count($naoaplicaveis);
				$controlesNaoAplicaveisPercent = number_format($controlesNaoAplicaveis/$controlesNaoEfetivos*100, 0);
			}
			else
			{ //KRI ZERADO ---------------
				$controlesEfetivos = count($total);
				$controlesEfetivosPercent = 100;

				$controlesNaoEfetivos = 0;
				$controlesNaoEfetivosPercent = 0;

				$controlesEncerrados = 0;
				$controlesEncerradosPercent = 0;

				$controlesEncerradosEmdiaContagem = 0;
				$controlesEncerradosEmdiaContagemPercent = 0;
				$controlesEncerradosEmatrasoContagem = 0;
				$controlesEncerradosEmatrasoContagemPercent = 0;

				$controlesAndamento = 0;
				$controlesAndamentoPercent = 0;


				$controlesAtraso = 0;
				$controlesAtrasoPercent = 0;

				$controlesPrazo = 0;
				$controlesPrazoPercent =  0;

				$controlesSemPlano = 0;
				$controlesSemPlanoPercent = 0;

				$naoReincidencia = 0;
				$naoReincidenciaPercent = 100;
				$reincidencia = 0;

				$controlesImplementadosPercent = 0;
				
				$controlesNaoAplicaveis = 0;
				$controlesNaoAplicaveisPercent =0;

				$this->coreView['responsavelPlanoArray'] = array();
				$this->coreView['responsavelAcaoArray'] = array();
				$this->coreView['responsavelPlanoArray'] = array();

				$this->coreView['tratamentoPrazo'] = 100;
				$this->coreView['aderencia'] = 100;
				$this->coreView['vencido30'] = 0;
				$this->coreView['vencido90'] = 0;



				goto enviarVarParaView;
			} //KRI ZERADO ---------------






			//CALCULO DO GRAFICO DE KRI
			$this->coreView['vencido30'] = round(count($nc30)/count($nc)*100).'%';
			$this->coreView['vencido90'] = $vencido90 = round(count($nc90)/count($nc)*100).'%';
			$this->coreView['tratamentoPrazo'] = $tratamentoPrazo = round(((100 - (count($nc30)*100/count($nc))))*0.3 +
																								((100 - (count($nc90)*100/count($nc))))*0.7);
			$this->coreView['aderencia'] = round((((100 - (count($nc30)*100/count($nc))))*0.3 +
																		((100 - (count($nc90)*100/count($nc))))*0.7	)//$tratamentoPrazo
																		*0.5+(100-(count($reincidente)/count($total)*100))*0.5);

			foreach($data as $row){ //$dataArea
				if($row['area responsavel acao'] <> null)
				array_push($responsavelAcao, $row['area responsavel acao']);
			}

			//var_dump($responsavelAcao); die();


			//CALCULO DO GRAFICO DE ACAO POR AREA
			$responsavelAcaoArray = array();
			$contagem = array_count_values($responsavelAcao);
			foreach($contagem AS $area => $vezes) {
				if($vezes >= 1) {
					$responsavelAcaoArray[$area] = $vezes;
				}
			}
			arsort($responsavelAcaoArray);
			$this->coreView['responsavelAcaoArray'] = $responsavelAcaoArray;

			//CALCULO DO GRAFICO DE TNC POR AREA
			$responsavelPlanoArray = array();
			$contagem = array_count_values($responsavelPlano);
			foreach($contagem AS $area => $vezes) {
				if($vezes >= 1) {
					$responsavelPlanoArray[$area] = $vezes;
				}
			}
			arsort($responsavelPlanoArray);
			$this->coreView['responsavelPlanoArray'] = $responsavelPlanoArray;






			//GERAÇÃO DA TABELA DE AUDITORIA E EFETIVIDADE
			$controlesEfetividadeFiliais = array();
			$controlesEfetividadeAnos = array();
			$controlesEfetividadeConteudo = array();

			foreach($total as $efControle){
				//gera as linhas das colunas (unidades)



				foreach ($safras as $ano)
				{
					$ano = $ano['safra'];
					$contagemControleTabelaConformes = 0;
					$contagemNCTabelaConformes = array();

					foreach ($total as $controle)
					{
						if($controle['filial'] == $efControle['filial'] && $controle['ano auditoria'] == $efControle['ano auditoria']){
							$contagemControleTabelaConformes ++;
							if($controle['id da nc'] <> null){
								array_push($contagemNCTabelaConformes, array($efControle['ano auditoria']));
							}
						}
					}
					$percentual = round(($contagemControleTabelaConformes-count($contagemNCTabelaConformes))/$contagemControleTabelaConformes*100).'%';
					if(!in_array(array($efControle['filial'], $percentual, $efControle['ano auditoria']), $controlesEfetividadeFiliais)){
						array_push($controlesEfetividadeFiliais, array($efControle['filial'], $percentual, $efControle['ano auditoria']));
					}
				}




				//gera as colunas da tabela (ANO)
				if(!in_array($efControle['ano auditoria'], $controlesEfetividadeAnos)){
					array_push($controlesEfetividadeAnos, $efControle['ano auditoria']);
				}
			}
			asort($controlesEfetividadeAnos);
			asort($controlesEfetividadeFiliais);
			$this->coreView['controlesEfetividadeFiliais'] = $controlesEfetividadeFiliais;
			$this->coreView['controlesEfetividadeAnos'] = $controlesEfetividadeAnos;
			//GERAÇÃO DA TABELA DE AUDITORIA E EFETIVIDADE




			//ENVIO DAS INFORMAÇÕES DO CONTROLLER PARA A VIEW
			enviarVarParaView:
			$this->coreView['total'] = $total;
			$this->coreView['controlesTotal'] = $controlesTotal;
			$this->coreView['controlesEfetivos'] = $controlesEfetivos;
			$this->coreView['controlesEfetivosPercent'] = $controlesEfetivosPercent;
			$this->coreView['controlesNaoEfetivosPercent'] = $controlesNaoEfetivosPercent;
			$this->coreView['controlesNaoEfetivos'] = $controlesNaoEfetivos;
			$this->coreView['controlesEncerrados'] = $controlesEncerrados;
			$this->coreView['controlesEncerradosPercent'] = $controlesEncerradosPercent;
			$this->coreView['controlesAndamento'] = $controlesAndamento;
			$this->coreView['controlesAndamentoPercent'] = $controlesAndamentoPercent;
			$this->coreView['controlesAtraso'] = $controlesAtraso;
			$this->coreView['controlesAtrasoPercent'] = $controlesAtrasoPercent;
			$this->coreView['controlesPrazo'] = $controlesPrazo;
			$this->coreView['controlesPrazoPercent'] = $controlesPrazoPercent;
			$this->coreView['controlesSemPlano'] = $controlesSemPlano;
			$this->coreView['controlesSemPlanoPercent'] = $controlesSemPlanoPercent;
			$this->coreView['controlesNaoAplicaveis'] = $controlesNaoAplicaveis;
			$this->coreView['controlesNaoAplicaveisPercent'] = $controlesNaoAplicaveisPercent;

			$this->coreView['nc'] = $nc;
			$this->coreView['nc30'] = $nc30;
			$this->coreView['nc90'] = $nc90;
			$this->coreView['reincidente'] = $reincidente ;
			$this->coreView['encerrados'] = $encerrados ;
			$this->coreView['andamento'] = $andamento;
			$this->coreView['controlesAndamentoEmdia'] = $controlesAndamentoEmdia;
			$this->coreView['controlesAndamentoEmatraso'] = $controlesAndamentoEmatraso ;
			$this->coreView['controlesEncerradosEmdia'] = $controlesEncerradosEmdia;
			$this->coreView['controlesEncerradosEmatraso'] = $controlesEncerradosEmatraso;
			$this->coreView['semPlano'] = $semPlano;
			$this->coreView['responsavelPlano'] = $responsavelPlano;
			$this->coreView['responsavelPlanoIDNC'] = $responsavelPlanoIDNC;
			$this->coreView['responsavelAcao'] = $responsavelAcao;
			$this->coreView['naoReincidenciaPercent'] = $naoReincidenciaPercent;
			$this->coreView['naoReincidencia'] = $naoReincidencia;
			$this->coreView['reincidencia'] = $reincidencia;
			$this->coreView['controlesImplementadosPercent'] = $controlesImplementadosPercent;

			$this->coreView['controlesEncerradosEmatrasoContagem'] = $controlesEncerradosEmatrasoContagem;
			$this->coreView['controlesEncerradosEmatrasoContagemPercent'] = $controlesEncerradosEmatrasoContagemPercent;
			$this->coreView['controlesEncerradosEmdiaContagem'] = $controlesEncerradosEmdiaContagem;
			$this->coreView['controlesEncerradosEmdiaContagemPercent'] = $controlesEncerradosEmdiaContagemPercent;

		}catch(PDOException $e)
		{
			echo ($e->getMessage());
		}


		if(isset($_POST['espelho'])){
			header('Pragma: anytextexeptno-cache', true);
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false);
			header("Content-Type: text/plain");
			header("Content-Disposition: attachment; filename=\"Espelho KRI ".date('Y').'-'.date('m').".html\"");
		}
		$this->loadView('relatorioView');

	}

	function listagem(){

		//definição de usuario
		$this->loadModel("usuarioCSC");
		$user = $this->models->usuarioCSC->getUserAD($_SERVER['REMOTE_ADDR']);
		if(!$user){
			die('Primeiro faca login no no portal https://suporte.amaggi.com.br ou no SoftExpert <span style="color: #FFF">'.$_SERVER['REMOTE_ADDR'].'</span>');
		}
		$this->usuario = $user[0];
		$this->coreView['usuario'] = $this->usuario;
		//definição de usuario


		//Permissoes
		$this->loadLib('Permission');
		$this->libs->Permission->required = array('csc.processo', 'controle.interno');
		$this->libs->Permission->verify($this->usuario['memberof']);
		$this->coreView['usuario'] = $this->usuario;
		//Permissoes

		//processamento das requisições
		$sqlSafra = null;
		$sqlFilial = null;
		if(isset($_GET['safra']) && $_GET['safra'] > '') {
			$sqlSafra = $_GET['safra'];
		}

		if(isset($_GET['filial']) && $_GET['filial'] > '') {
			$sqlFilial = $_GET['filial'];
		}


		//coleta dos dados
		$data = $this->models->kriModel->getControles($sqlSafra, $sqlFilial);
		$filiais = $this->models->kriModel->getUnidades();
		$safras = $this->models->kriModel->getAnosSafras();
		$total = array();
		foreach($data as $row)
		{
				array_push($total, $row);
		}

		//passagem dos dados para a view
		$this->coreView['total'] = $total;
		$this->coreView['filiais'] = $filiais;
		$this->coreView['safras'] = $safras;
		$this->coreView['title'] = 'Listagem de Controles do KRI';

		//chamada da view
		$this->loadView('listagemView');
	}

	function editar(){

		//definição de usuario
		$this->loadModel("usuarioCSC");
		$user = $this->models->usuarioCSC->getUserAD($_SERVER['REMOTE_ADDR']);
		if(!$user){
			die('Primeiro faca login no no portal https://suporte.amaggi.com.br ou no SoftExpert <span style="color: #FFF">'.$_SERVER['REMOTE_ADDR'].'</span>');
		}
		$this->usuario = $user[0];
		$this->coreView['usuario'] = $this->usuario;
		//definição de usuario


		$this->loadLib('Permission');
		$this->libs->Permission->required = array('csc.processo', 'controle.interno');
		$this->libs->Permission->verify($this->usuario['memberof']);
		$this->coreView['usuario'] = $this->usuario;
		//Permissoes

		$this->coreView['title'] = 'Adição de controle do KRI';

		$filiais = $this->models->kriModel->getUnidades();
			$this->coreView['filiais'] = $filiais;

		//ABRE PARA EDIÇÃO
		if(isset($_GET['oid'])){

			//coleta dos dados
			$controle = $this->models->kriModel->getControle($_GET['oid']);
			$this->coreView['controle'] = $controle;
			$this->coreView['title'] = 'Edição de controle do KRI';
		}

		if(isset($_POST['ANO']) && isset($_POST['NRCONTROLE']) && isset($_POST['OID']) && $_POST['OID'] <> ''){ //UPDATE
			$controle = array();
			$controle['ANO'] = $_POST['ANO'];
			$controle['NRCONTROLE'] = $_POST['NRCONTROLE'];
			$controle['TXTCONTROLE'] = $_POST['TXTCONTROLE'];
			$condicao = $_POST['OID'];
			$result = $this->models->kriModel->saveControle($controle, $condicao);

			if($result){
				header('Location: '.CONTROLLERDIR.'/listagem');
			}
		}

		//chamada da view
		$this->loadView('listagemEditarView');
	}


	//ALTERA A KEY PARA LOWER CASE DE UM ARRAY RECURSIVAMENTE
	public function array_change_key_case_recursive($input, $case = null){
		if(!is_array($input)){
			trigger_error("Invalid input array '{$array}'",E_USER_NOTICE); var_dump($input); exit;
		}
		// CASE_UPPER|CASE_LOWER
		if(null === $case){
			$case = CASE_LOWER;
		}
		if(!in_array($case, array(CASE_UPPER, CASE_LOWER))){
			trigger_error("Case parameter '{$case}' is invalid.", E_USER_NOTICE); exit;
		}
		$input = array_change_key_case($input, $case);
		foreach($input as $key=>$array){
			if(is_array($array)){
				$input[$key] = $this->array_change_key_case_recursive($array, $case);
			}
		}
		return $input;
	}
}