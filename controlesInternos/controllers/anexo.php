<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class Anexo extends ModularCore\Controller{

	function __construct() {
		parent::__construct();
	}

	function visualizar(){
		$this->loadModel('anexoModel');

		if(isset($_GET['oid'])){
			$oid = $_GET['oid'];

			$file = MODULEFOLDER.'/views/assets/documentos/'.$oid.'.pdf';
			$mimeUsado = 'application/pdf';

			$this->coreView['mime'] = $mimeUsado;
			$this->coreView['arquivo'] = fopen($file, "r");

		}else{
			$this->coreView['arquivo'] = 'Erro na sua solicitação. O OID deve ser informado';
		}
		$this->loadView('anexoView');
	}
	
	function download(){

		if(isset($_GET['arquivo'])){
			$arquivo = $_GET['arquivo'];
			
			// Define o tempo máximo de execução em 0 para as conexões lentas
			set_time_limit(0);
			// Arqui você faz as validações e/ou pega os dados do banco de dados
			$aquivoNome = $arquivo; // nome do arquivo que será enviado p/ download
			$arquivoLocal = 'https://172.16.48.202/MVCore/modules/controlesinternos/views/arquivos/'.$aquivoNome; // caminho absoluto do arquivo
			echo $arquivoLocal;
			// Verifica se o arquivo não existe
			if (!file_exists($arquivoLocal)) {
			// Exiba uma mensagem de erro caso ele não exista
			//exit;
			}
			// Aqui você pode aumentar o contador de downloads
			// Definimos o novo nome do arquivo
			$novoNome = 'imagem_nova.jpg';
			// Configuramos os headers que serão enviados para o browser
			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename="'.$novoNome.'"');
			header('Content-Type: application/octet-stream');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($aquivoNome));
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Expires: 0');

		}else{
			$this->coreView['arquivo'] = 'Erro na sua solicitação. O OID deve ser informado';
		}
	}
}