<!DOCTYPE html >
<html lang="en">

<head>

    <meta charset="iso-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />
	
	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
	
	
    <title>Relatorio de KRI - Controles Internos - Amaggi</title>
    
	<!--Imports-->
		
		
		<!-- Bootstrap Core CSS --><link href="<?php echo ASSETS;?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<!-- Bootstrap Core JS --> <script src="<?php echo ASSETS;?>/bootstrap/js/bootstrap.min.js"></script>
		<!-- jQuery --><script src="<?php echo ASSETS;?>/jquery/js/jquery.min.js"></script>
		<!-- Google Charts  --><script src="<?php echo ASSETS;?>/GoogleCharts/loader.js"></script>

</head>

<body>


			<div class="container">
				<div class="col-md-12">
					<div class="panel text-center">
						<div class="panel-heading" style="background-color: #fff;">
							<img src="<?php echo ASSETS;?>LogoAmaggi.gif" width="400px" />
						</div>
						<div class="panel-body">
							<h2>Relatorio de KRI - Controles Internos</h2></br>
							
													<!-- link de backup do banco -->
													<?php if(!(count($unidades)>0 && (isset($_GET['ano']) || isset($_GET['anokri'])))){ ?>
																<div class="container">
																	<div class="col-md-12">
																		<form action="#" method="POST" enctype="multipart/form-data">
																		<div class="form-group col-md-4">
																			<a download="banco_KRI - <?php echo date('Y-m-d H.i.s');?>'.backup" href="<?php echo BASEDIR?>modules/kri/models/coreCache_anosSafras.cache"> Backup do banco</a>
																		</div>
																		<div class="form-group col-md-3">
																			<input type="file" name="arquivo[]" />
																		</div>
																		<div class="form-group col-md-3">
																			<input class="btn btn-default" type="submit" value="Enviar" />
																		</div>
																		
																		
																		</form>
																	</div>
																</div>
															<br /><br />
													<?php	}?>
													<!-- link de backup do banco -->
							
							

'
														    
							<div class="container">
								<form class="form-inline" method="GET">
									  <div class="col-md-4 text-left">
											<?php	
											foreach($filiais as $filial){
													if(count($unidades)==0){
														
														if(in_array($filial['filial'], $selecionadasUnidades)){
															if(isset($_GET['ano']) || isset($_GET['anokri'])){
																echo '<div class="checkbox">
																		<label>
																		<input type="checkbox" name="un-'.$filial['md5filial'].'" checked /> '.$filial['filial'].'
																		</label>
																	</div><br />';
															}else{
																echo '<div class="checkbox">
																		<label>
																		<input type="checkbox" name="un-'.$filial['md5filial'].'" /> '.$filial['filial'].'
																		</label>
																	</div><br />';
															}
															
														}else{
															echo '<div class="checkbox">
																		<label>
																		<input type="checkbox" name="un-'.$filial['md5filial'].'" /> '.$filial['filial'].'
																		</label>
																	</div><br />';
														}
													}else{
														if(in_array($filial['filial'], $selecionadasUnidades)){
															echo '<div class="checkbox">
																		<label>
																		<input type="checkbox" name="un-'.$filial['md5filial'].'" checked disabled/> '.$filial['filial'].'
																		</label>
																	</div><br />';
														}
													}
													
												}
											
											?>
											</div>
										
										
									  
									  <!--ANO AUDITORIA-->
									  <div class="form-group col-md-3">
										<label>Ano Auditoria</label>
											<select name="ano" id="ano" class="form-control" <?php if(count($unidades)>0 && (isset($_GET['ano']) || isset($_GET['anokri']))) echo 'disabled=""';?> >
											<option value=""></option>
												<?php
													foreach($safras as $safra){
														if($safra['safra'] == $ano){
															echo '<option value="'.$safra['safra'].'" selected>'.$safra['safra'].'</option>';
														}else{
															echo '<option value="'.$safra['safra'].'">'.$safra['safra'].'</option>';
														}
													}
												?>
											</select>
									  </div>
									  
									  <!--ANO KRI-->
									  <div class="form-group col-md-3">
										<label>Ano KRI</label>
											<select name="anokri" id="anokri" class="form-control" <?php if(count($unidades)>0 && (isset($_GET['ano']) || isset($_GET['anokri']))) echo 'disabled=""';?> >
											<option value=""></option>
												<?php
													foreach($KRIs as $KRI){
														if($KRI['anokri'] == $anokri){
															echo '<option value="'.$KRI['anokri'].'" selected>'.$KRI['anokri'].'</option>';
														}else{
															echo '<option value="'.$KRI['anokri'].'">'.$KRI['anokri'].'</option>';
														}
													}
												?>
											</select>
									  </div>
									  

										<?php if(!(count($unidades)>0 && (isset($_GET['ano']) || isset($_GET['anokri'])))){ ?>
											<button class="btn btn-default" type="submit" value="submit">Buscar</button>
										<?php } ?>
								</form>
							</div>
							<br>
							
							<?php if(count($total)>=1) { ?>
							<div class="col-md-12">
								<div class="panel panel-default" id="panelx">
								  <div class="panel-heading">
									<h3 class="panel-title">Resumo das Nao conformidades</h3>
								  </div>
								  <div class="panel-body">
									<table class="table">
										<tr>
											<th>Controles Auditados</th>
											<th>Nao Conformes</th>
											<th>Vencidos mais de 30 dias</th>
											<th>Vencidos mais de 90 dias</th>
											<th>Tratados no Prazo</th>
											<th>Nao Reicidencia</th>
											<th>Meta</th>
										</tr>
										<tr>
											<?php 
												if(count($total)>=1 && count($nc)>=1) {
													echo '<td>'.count($total).'</td>';
													echo '<td>'.count($nc).'</td>';
													echo '<td>'.count($nc30).'</td>';
													echo '<td>'.count($nc90).'</td>';
													echo '<td>'.$tratamentoPrazo.'%</td>';
													echo '<td>'.$naoReincidenciaPercent.'%</td>';
													echo '<td>'.$aderencia.'%</td>';
												
											?>
										</tr>
									</table>
								  </div>
								</div>
							</div>
							
							<!-- UNIDADES AUDITADAS POR ANO -->
							<?php if(!(count($unidades)>0 && (isset($_GET['ano']) || isset($_GET['anokri'])))){ ?>
								<div class="col-md-12">
									<div class="panel panel-default">
									  <div class="panel-heading">
										<h3 class="panel-title">Comparativo de Efetividade %</h3>
										<img src="<?php echo ASSETS?>check.png" />
										
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="panel panel-default">
									  <div class="panel-heading">
										<h3 class="panel-title">Comparativo de Unidades Auditadas</h3>
										
										
										</div>
									</div>
								</div>
							<?php } ?>
							<!-- UNIDADES AUDITADAS POR ANO -->
							
							
							<div class="col-md-12">
								<div class="panel panel-default">
								  <div class="panel-heading">
									<h3 class="panel-title">KRI - Key Risk Indicators</h3>
								  </div>
								  <div class="panel-body" style="height: 300px;">
									<!-- GRAFICO DO KRI -->
									
									<button class="btn btn-default box" id="box1" style="top: 120px; left: 800px; width: 130px;">Vencido + 30</button><button class="btn <?php if(count($nc30)>=1) echo 'btn-danger'; else echo 'btn-success'?> box" id="box1" style="top: 120px; left: 930px; width: 80px;"><?php echo $vencido30;?></button>
									<button class="btn btn-default box" id="box2" style="top: 220px; left: 800px; width: 130px;">Vencido + 90</button><button class="btn <?php if(count($nc90)>=1) echo 'btn-danger'; else echo 'btn-success'?> box" id="box1" style="top: 220px; left: 930px; width: 80px;"><?php echo $vencido90;?></button>
									
									<button class="btn btn-default box" id="box3" style="top: 60px; left: 430px; width: 130px;"><small>Nao Reincidencia</small></button><button class="btn btn-warning box" id="box1" style="top: 60px; left: 560px; width: 80px;"><?php echo $naoReincidenciaPercent;?>%</button>
									<button class="btn btn-default box" id="box3" style="top: 160px; left: 430px; width: 130px;"><small>Tratamento no Prazo</small></button><button class="btn btn-warning box" id="box1" style="top: 160px; left: 560px; width: 80px;"><?php echo $tratamentoPrazo;?>%</button>
									
									<!--linhas-->
									<div class="line" style="top: 150px; left: 730px; width: 70px;">30%<br /><br /><br /><br /><br />70%</div><div class="lineAlone" style="top: 200px; left: 640px; width: 90px;"></div>
									<div class="line" style="top: 95px; left: 380px; width: 50px;">50%<br /><br /><br /><br /><br />50%</div><div class="lineAlone" style="top: 140px; left: 310px; width: 70px;"></div>
									<!--linhas-->
									
									<button class="btn btn-default title" id="box1" style="top: 90px; left: 50px; width: 180px;"><small>Meta</small></button><button class="btn btn-default title" id="box1" style="top: 90px; left: 250px; width: 80px;">90%</button>
									<button class="btn btn-default box" id="box1" style="top: 120px; left: 50px; width: 180px;"><small>Aderencia as Regras TNC</small></button><button class="btn btn-warning box" id="box1" style="top: 120px; left: 250px; width: 80px;"><?php echo $aderencia;?>%</button>
									
									
									
									
									

									<style>
									.box{
										border: 1px solid black;
										height: 70px;
										position: absolute;
									}
									.line{
										width: 1px;
										height: 100px;
										border: 1px solid black;
										position: absolute;
										border-right: 0;
									}
									
									.lineAlone{
										width: 1px;
										height: 100px;
										border: 0;
										border-top: 1px solid black;
										position: absolute;
									}
									.title{
										border: 1px solid black;
										height: 30px;
										position: absolute;
									}

									</style>
									
									<!-- GRAFICO DO KRI -->
								  </div>
								</div>
							</div>
							
							
							
							<!-- graficos-->
							<div class="col-md-12">
								<div class="panel panel-default">
								  <div class="panel-heading">
									<h3 class="panel-title">Indicadores dos Controles e Nao Conformidades</h3>
								  </div>
								  <div class="panel-body">
								 	<div class="col-md-5">
										<table class="table col-md-6">
											<tr>
												<th>Descricao</th>
												<th>Quant.</th>
												<th>%</th>
												
											</tr>

												<tr>
													<th>Controles Auditados</th>
													<td><?php echo $controlesTotal; ?></td>
													<td>100%</td>
												</tr>
												<tr>
													<th>Total Efetivos (Conforme)</th>
													<td><?php echo $controlesEfetivos;?></td>
													<td><?php echo $controlesEfetivosPercent;?>%</td>
												</tr>
												<tr>
													<th>Total de Nao Efetivos (Nao Conforme)</th>
													<td><?php echo $controlesNaoEfetivos;?></td>
													<td><?php echo $controlesNaoEfetivosPercent;?>%</td>
												</tr>
												<tr>
													<th>TNC Concluido</th>
													<td><?php echo $controlesEncerrados; ?></td>
													<td><?php echo $controlesEncerradosPercent ?>%</td>
												</tr>
												<tr>
													<th>TNC Em Andamento</th>
													<td><?php echo $controlesAndamento; ?></td>
													<td><?php echo $controlesAndamentoPercent ?>%</td>
												</tr>
												<tr>
													<th>TNC Atrasada</th>
													<td><?php echo $controlesAtraso; ?></td>
													<td><?php echo $controlesAtrasoPercent ?>%</td>
												</tr>
												<tr>
													<th>TNC Dentro do Prazo</th>
													<td><?php echo $controlesPrazo; ?></td>
													<td><?php echo $controlesPrazoPercent ?>%</td>
												</tr>
												<tr>
													<th>TNC Sem Prazo</th>
													<td><?php echo $controlesSemPlano; ?></td>
													<td><?php  echo $controlesSemPlanoPercent; ?>%</td>
												</tr>
												<tr>
													<th>TNC Nao Aplicavel</th>
													<td>0</td>
													<td>0%</td>
												</tr>
												
											</tr>
											
										</table>
										<br />
										
									</div>
									<div class="col-md-7">
										<script type="text/javascript">
											      google.charts.load('current', {'packages':['corechart', 'bar']});
											      google.charts.setOnLoadCallback(drawVisualization);
											
											
											      function drawVisualization() {
											        // Some raw data (not necessarily accurate)
											        var data = google.visualization.arrayToDataTable([
											         ['Element', 	'No prazo', 	'Atrasado','Sem Prazo'],
											         ['Em aberto', 	<?php echo count($controlesAndamentoEmdia); ?>,		<?php echo count($controlesAndamentoEmatraso);?>, <?php echo $controlesSemPlano; ?>],
											         ['Encerrado',	<?php echo count($controlesEncerradosEmdia);?>,		<?php echo count($controlesEncerradosEmatraso);?>, 0],
											      ]);
											
											    var options = {
											      title : 'Status TNC',
											      vAxis: {title: '',colors: 'green'},
											      hAxis: {title: ''},
											      seriesType: 'bars',
											      series: {0: {type: 'bars', color: 'green'}}
											    };
											
											    var chart = new google.visualization.ComboChart(document.getElementById('prazos'));
											    chart.draw(data, options);
											  }
											    </script>
										<div id="prazos" style="height: 400px;"></div>
									</div>
									<div class="col-md-5">
										
									</div>
									<div class="col-md-7">
									
										    
										    <script type="text/javascript">
										      google.charts.load('current', {'packages':['corechart']});
										      google.charts.setOnLoadCallback(drawChart);
										      function drawChart() {
										
										        var data = google.visualization.arrayToDataTable([
										          ['Task', 'Hours per Day'],
										          ['Reincidente',<?php echo $reincidencia; ?>],
										          ['Nao Reincidente',<?php echo $naoReincidencia; ?>]
										        ]);
										
										        var options = {
										          title: 'Reincidencia',
										          slices: {
													            0: { color: '#DD3311' },
													            1: { color: 'blue' },
													          }
										        };
										
										        var chart = new google.visualization.PieChart(document.getElementById('reincidencia'));
										
										        chart.draw(data, options);
										      }
										    </script>
										    <div id="reincidencia" style="height: 400px;"></div>


									</div>
									<div class="container">
										<div class="col-md-3">
											<table class="table" style="font-size: 13px;">
												<tr>
													<th>Area / Plano</th>
													<th>Quant.</th>
													<th>%</th>
													
												</tr>
													<?php
														foreach($responsavelPlanoArray AS $area => $vezes) {
																echo '<tr><th>'.$area.'</th><td>'.$vezes.'</td><td>'.number_format($vezes/$controlesAndamento*100,1).'%</td></tr>';

														}
													?>
													
													
													
												</tr>
												
											</table>
											
										</div>
										<br />
										<div class="col-md-9">
											<script type="text/javascript">
											      google.charts.load('current', {'packages':['corechart']});
											      google.charts.setOnLoadCallback(drawChart);
											      function drawChart() {
											
											        var data = google.visualization.arrayToDataTable([
											          ['Task', 'Hours per Day'],
											          <?php
															foreach($responsavelPlanoArray AS $area => $vezes) {
																	echo "['$area',$vezes],";
															}
														?>
											        ]);
											
											        var options = {
											          title: 'TNC por Area',
											        };
											
											        var chart = new google.visualization.PieChart(document.getElementById('area'));
											
											        chart.draw(data, options);
											      }
											    </script>
											    <div id="area" style="height: 500px;"></div>
										</div>
									</div>
									
									<br />
									<!--Nay - Responsavel por ação -->
									<div class="container">
										<div class="col-md-3">
											<table class="table" style="font-size: 13px;">
												<tr>
													<th>Area / Acao</th>
													<th>Quant.</th>
													<th>%</th>
													
												</tr>
													<?php
														foreach($responsavelAcaoArray AS $area => $vezes) {
																echo '<tr><th>'.$area.'</th><td>'.$vezes.'</td><td>'.number_format($vezes/array_sum($responsavelAcaoArray)*100, 1).'%</td></tr>';//"$numero - $vezes<br />";
														}
													?>
													
													
													
												</tr>
												
											</table>
											
										</div>
										<div class="col-md-9">
											<script type="text/javascript">
											      google.charts.load('current', {'packages':['corechart']});
											      google.charts.setOnLoadCallback(drawChart);
											      function drawChart() {
											
											        var data = google.visualization.arrayToDataTable([
											          ['Task', 'Hours per Day'],
											          <?php
															$contagem = array_count_values($responsavelAcao);
															foreach($responsavelAcaoArray AS $area => $vezes) {
																	echo "['$area',$vezes],";
															}
														?>
											        ]);
											
											        var options = {
											          title: 'Acao por Area',
											        };
											
											        var chart = new google.visualization.PieChart(document.getElementById('acao'));
											
											        chart.draw(data, options);
											      }
											    </script>
											    <div id="acao" style="height: 500px;"></div>
										</div>
									</div>
									<!--Nay - Responsavel por ação -->
								  </div>
								</div>
							</div>
							<!-- graficos-->
							
							<div class="col-md-12">
								<div class="panel panel-default">
								  <div class="panel-heading">
									<h3 class="panel-title">Relatorio dos Controles e Nao Conformidades</h3>
								  </div>
								  <div class="panel-body">
									<table class="table" style="font-size: 14px;">
										<tr>
											<th>Filial</th>
											
											<th>Auditoria</th>
											<th>KRI</th>
											<th>Controle</th>
											<th>ID NC <small style="color: grey; font-size: 9px;">(SoftExpert)</small></th>
											<th>ID Plano <small style="color: grey; font-size: 9px;">(SoftExpert)</small></th>
											<th>Status TNC</th>
											<th>Status Atraso</th>
											<th>Reincidencia</th>
										</tr>
										
									
										<?php }else{ echo '<h4>Nao ha NAO CONFORMIDADES para esta(s) unidade(s) e ano de auditoria/ano de KRI</h4>';}
											foreach($total as $row)
											{
													echo '<tr>';
														echo '<td>'.utf8_encode($row['filial']).'</td>';
														echo '<td>'.$row['ano auditoria'].'</td>';
														echo '<td>'.$row['ano kri'].'</td>';
														echo '<td>'.$row['nrcontrole'].'</td>';
														echo '<td><a href="'.BASEDIR.'se/instancia/abrirLeitura?instancia=">'.$row['id da nc'].'</a></td>';
														echo '<td>'.$row['id plano'].'</td>';
														echo '<td>'.$row['status do tnc'].'</td>';
														echo '<td>'.$row['status dias atraso'].'</td>';
														echo '<td>'.$row['reincidente'].'</td>';
													echo '</tr>';
											}
											
										?>
										</tr>
										
									</table>
								  </div>
								</div>
							</div>
							<?php } else{echo '<h3>Nao ha dados a serem exibidos para esta filial / setor e auditoria / KRI</h3>';}?>
							
						</div>
					</div>
				</div>
			</div>				

					



 <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo ASSETS;?>/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo ASSETS;?>/metisMenu/js/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo ASSETS;?>/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo ASSETS;?>/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo ASSETS;?>/sb-admin-2/js/sb-admin-2.js"></script>
    
    <!-- Morris Charts JavaScript -->
    <script src="<?php echo ASSETS;?>/raphael/raphael-min.js"></script>
    <script src="<?php echo ASSETS;?>/morrisjs/morris.min.js"></script>
    <script src="<?php echo ASSETS;?>/morrisjs/morris-data.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>