<?php require (BASE.'/modules/default/views/layout/head.php'); ?>
<?php require (BASE.'/modules/default/views/layout/header.php'); ?>

			<div class="containser">
				<div class="col-md-12">
					<div class="panel text-center">
						<div class="panel-heading" style="background-color: #fff;">
							
						</div>
						<div class="panel-body">
							<div class="container">
								<form class="form-inline" method="GET">
									  <div class="form-group col-md-3">
										<label>Ano Auditoria</label>
											<select name="filial" id="safra" class="form-control" <?php if(isset($unidades) && count($unidades)>0) echo 'disabled=""';?> >
											<option value=""></option>
												<?php
													foreach($filiais as $safra){
														if(isset($unidades) && $safra['FILIAL'] == $unidade){
															echo '<option value="'.$safra['FILIAL'].'" selected>'.$safra['FILIAL'].'</option>';
														}else{
															echo '<option value="'.$safra['FILIAL'].'">'.$safra['FILIAL'].'</option>';
														}
													}
												?>
											</select>
									  </div>
										
										<br>
									  
									  <!--ANO AUDITORIA-->
									  <div class="form-group col-md-offset-1 col-md-3 ">
										<label>Ano Auditoria</label>
											<select name="safra" id="safra" class="form-control" <?php if(isset($unidades) && count($unidades)>0) echo 'disabled=""';?> >
											<option value=""></option>
												<?php
													foreach($safras as $safra){
														if(isset($ano) && $safra['SAFRA'] == $ano){
															echo '<option value="'.$safra['SAFRA'].'" selected>'.$safra['SAFRA'].'</option>';
														}else{
															echo '<option value="'.$safra['SAFRA'].'">'.$safra['SAFRA'].'</option>';
														}
													}
												?>
											</select>
									  </div>
									  <div class="form-group col-md-1 ">
										<button class="btn btn-default" type="submit" value="submit">Buscar</button>
										</div>
										
								</form>
								
							</div>
							<br />

							

							
							<div class="col-md-12">
								<div class="panel panel-default">
								  <div class="panel-heading">
								  
									<h3 class="panel-title">Relatorio dos Controles</h3>
								  </div>
								  
								  <div class="panel-body pull-left">
								  	
									<table class="table" style="font-size: 14px;">
										<tr>
											<th></th>
											<th>Unidade</th>
											<th>Ano</th>
											<th>NRCONTROLE</th>
											<th>TXT</th>
										</tr>
										
									
										<?php 
											foreach($total as $registro){
												echo '<tr>';
													//botoes
													echo '<td>';
														//echo '<a href="'.CONTROLLERDIR.'/remover" class="btn btn-danger"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> ';
														echo ' <a href="'.CONTROLLERDIR.'/editar?oid='.$registro['OID'].'" class="btn btn-default"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
													echo '</td>';
													//botoes
													
													echo '<td>';
														echo $registro['UNIDADE'];
													echo '</td>';

													echo '<td>';
														echo $registro['ANO'];
													echo '</td>';

													echo '<td>';
														echo $registro['NRCONTROLE'];
													echo '</td>';

													echo '<td>';
														echo $registro['TXTCONTROLE'];
													echo '</td>';
												echo '</tr>';
												
											}
											
											
										?>
										<tr>
											<th>Total de Controles</th>
											<td><?php echo count($total);?></td>
										</tr>
										
									</table>
								  </div>
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
			</div>				

					



<?php require (BASE.'/modules/default/views/layout/footer.php'); ?>