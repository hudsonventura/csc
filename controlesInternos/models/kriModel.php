<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class KriModel extends ModularCore\Model{

	function __construct() {
		parent::__construct();
		$this->loadDb('oraclePRD', 'ufmt');
	}

	function getUnidades(){
		$this->db->ufmt->sqlBuilder->select('distinct REMOVE_ACENTOS(UNIDADE) as FILIAL, SUBSTR(MD5(unidade), 0, 5) AS MD5FILIAL');
		$this->db->ufmt->sqlBuilder->from('dyncintcontroleaud');
		$this->db->ufmt->sqlBuilder->orderby("FILIAL");
		//echo($this->db->ufmt->sqlBuilder->getQuery()); die();
		return $this->db->ufmt->sqlBuilder->executeQuery();
	}

	function getAreas(){
		$this->db->ufmt->sqlBuilder->select('"Area Responsavel Acao"');
		$this->db->ufmt->sqlBuilder->from('"AMAGGICONTROLESINTERNOSKRI"');
		$this->db->ufmt->sqlBuilder->orderby("FILIAL");
		return $this->db->ufmt->sqlBuilder->executeQuery();
	}

	function getAnosKris(){
		$this->db->ufmt->sqlBuilder->select('distinct "Ano KRI" as "AnoKRI"');
		$this->db->ufmt->sqlBuilder->from('AMAGGICONTROLESINTERNOSKRI');
		$this->db->ufmt->sqlBuilder->orderby('"Ano KRI"');

		return $this->db->ufmt->sqlBuilder->executeQuery();
	}

	function getAnosSafras(){
		$this->db->ufmt->sqlBuilder->select('distinct ANO as SAFRA');
		$this->db->ufmt->sqlBuilder->from('dyncintcontroleaud');
		$this->db->ufmt->sqlBuilder->orderby('ano');
		return $this->db->ufmt->sqlBuilder->executeQuery();
	}

	function getControle($oid){
		$this->db->ufmt->sqlBuilder->select('REMOVE_ACENTOS(UNIDADE) AS UNIDADE, ANO, NRCONTROLE, TXTCONTROLE, OID');
		$this->db->ufmt->sqlBuilder->from('dyncintcontroleaud');
		$this->db->ufmt->sqlBuilder->where("oid = '$oid'");
		return $this->db->ufmt->sqlBuilder->executeQuery()[0];
	}

	function saveControle($controle, $condicao){
		$this->db->ufmt->sqlBuilder->update($controle, 'dyncintcontroleaud');
		$this->db->ufmt->sqlBuilder->where("OID = '$condicao'");
		return $this->db->ufmt->sqlBuilder->executeQuery();
	}



	function getKri(){
		$this->db->ufmt->sqlBuilder->select('distinct "Ano KRI" as "AnoKRI"');
		$this->db->ufmt->sqlBuilder->from('"AMAGGICONTROLESINTERNOSKRI"');
		$this->db->ufmt->sqlBuilder->orderby('"Ano KRI"');
		return $this->db->ufmt->sqlBuilder->executeQuery();
	}

	function getUnidadesMD5($unidades){
		$this->db->ufmt->sqlBuilder->select('distinct REMOVE_ACENTOS(UNIDADE) as FILIAL');
		$this->db->ufmt->sqlBuilder->from('dyncintcontroleaud');
		$this->db->ufmt->sqlBuilder->where('1=0');
		foreach($unidades as $unidade){
			$this->db->ufmt->sqlBuilder->whereor("SUBSTR(MD5(unidade), 0, 5) = '$unidade' ");
		}
		$this->db->ufmt->sqlBuilder->orderby("FILIAL");
		$result = $this->db->ufmt->sqlBuilder->executeQuery();
		$return = array();
		foreach($result as $row){
			array_push($return, $row['FILIAL']);
		}
		return $return;
	}

	function getKRICompleto($unidades, $ano, $anoKri){

		//CRIA O WHERE -----------------------------------------------
		if($anoKri)
			$anoKriSQL = "\"ano kri\" like '$anoKri'";
		else
			$anoKriSQL = "\"ano kri\" like '%'";

		if($ano)
			$anoSQL = " \"ANO AUDITORIA\" like '$ano'";
		else
			$anoSQL = " \"ANO AUDITORIA\" like '%'";

		$unidadesSQL = array();
		foreach($unidades as $unidades){
			array_push($unidadesSQL, "FILIAL = '$unidades' OR ");
			}
		if(count($unidadesSQL)>0)
		{
			$unidadesSQL[count($unidadesSQL)-1] = substr($unidadesSQL[count($unidadesSQL)-1], 0, $unidadesSQL[count($unidadesSQL)-1]-4);
		}else
		{
			array_push($unidadesSQL, "");
		}
		$unidadeQuery = null;
		if($unidadesSQL[0]<>''){
			$unidadeQuery.= '(';
			foreach($unidadesSQL as $unidadeSQL){
				$unidadeQuery.= "$unidadeSQL";
			}
			$unidadeQuery.= ")";
		}else{
			$unidadeQuery = "FILIAL LIKE '%'";
		}
		//CRIA O WHERE -----------------------------------------------



		//VERIFICA SE ESTÁ DENTRO DO TEMPO DE VALIDADE DO CACHE
		$timestampCache = $this->libs->cache->getTimestamp('anosSafras');
		$timestampAtual = time();
		if($timestampAtual - $timestampCache <= 1800)
		{
			//carrega do cache
			$sqlParams = "($anoSQL AND $anoKriSQL AND $unidadeQuery) group by FILIAL, \"ANO AUDITORIA\", NRCONTROLE ORDER BY \"ANO AUDITORIA\", FILIAL, CAST(Substr(NRCONTROLE, 2)AS NUMBER)";
			return $this->libs->cache->load('anosSafras', $sqlParams);
		}



		//CASO BANCO SEJA ORACLE
		if($this->db->ufmt->vendor == 'oci')
		{
			$query = "	select KRI.\"Status do TNC\", REMOVE_ACENTOS(\"Status da NC\") AS \"Status da NC\", \"Id da NC\", CASE WHEN UNIDADE IS NOT NULL THEN REMOVE_ACENTOS(UNIDADE) ELSE FILIAL END as FILIAL, \"Status dias Atraso\", KRI.\"Area Responsavel Plano\", KRI.\"Area Responsavel Acao\", Reincidente, ano as \"ANO AUDITORIA\", NRCONTROLE, KRI.\"Id Plano\"
								, CASE WHEN \"Id da NC\" is not null THEN 1 ELSE 0 END AS \"NUM NC\", 1 as \"NUM CONTROLE\", CASE WHEN \"Id da NC\" is null THEN 1 ELSE 0 END AS \"NUM NAOCONTROLE\"
								, CASE WHEN KRI.\"Ano KRI\" IS NOT NULL THEN KRI.\"Ano KRI\" ELSE TO_NUMBER((SELECT MAX(A.\"Ano KRI\") FROM AMAGGICONTROLESINTERNOSKRI A WHERE A.FILIAL = AUD.UNIDADE AND A.\"ANO AUDITORIA\" = AUD.ANO)) END AS \"Ano KRI\"
								, KRI.NAOAPLICAVEL
								from dyncintcontroleaud AUD

								LEFT JOIN (SELECT DISTINCT \"Id da NC\"
														,CASE
														  WHEN \"Status da NC\" = 'Cancelado' THEN 'Nao aplicavel'
														  WHEN \"Status do TNC\" = 'Em andamento Em dia' THEN 'Em andamento em dia'
														  WHEN \"Status do TNC\" = 'Em andamento Em atraso' THEN 'Em andamento em atraso'
														  WHEN \"Status do TNC\" = 'Encerrada Em dia' THEN 'Encerrada em dia'
														  WHEN \"Status do TNC\" = 'Encerrada Em atraso' THEN 'Encerrada em atraso'
														  ELSE \"Status do TNC\"
														 END AS \"Status do TNC\"

														,CASE
														  WHEN \"Status da NC\" = 'Cancelado' THEN 'Nao aplicavel'
														  WHEN \"Status do TNC\" like 'Em andamento%' THEN 'Em andamento'
														  WHEN \"Status do TNC\" like 'Encerrada%' THEN 'Encerrada'
														  WHEN \"Status do TNC\" like 'Sem plano%' THEN 'Sem plano'
														  ELSE \"Status da NC\"
														 END AS \"Status da NC\"
														, filial, KRI.\"ANO AUDITORIA\", KRI.\"NR Controle\", KRI.\"Ano Auditoria\", KRI.\"Ano KRI\", KRI.\"Id Plano\"
														, \"Status dias Atraso\", KRI.\"Area Responsavel Plano\", KRI.\"Area Responsavel Acao\", CASE WHEN Reincidente = 'Sim' then 'Reincidente' else  'Nao reincidente' END AS Reincidente, KRI.NAOAPLICAVEL

														FROM AMAGGICONTROLESINTERNOSKRI KRI) KRI ON AUD.UNIDADE = KRI.FILIAL AND AUD.ANO = KRI.\"ANO AUDITORIA\" AND KRI.\"NR Controle\" = AUD.NRCONTROLE ";
		}
		
		$this->db->ufmt->sqlBuilder->setQuery($query);
			//echo $this->db->ufmt->sqlBuilder->getQuery(); die();
		
		//retorna do banco
		$db = $this->db->ufmt->sqlBuilder->executeQuery();

		//salva no cache
		$this->libs->cache->save('anosSafras', $db);
		header('Location: '.$_SERVER['REQUEST_URI']);

		//remove itens duplicados no cache
		//$this->libs->cache->removeDuplicates('anosSafras', '');d



		//carrega do cache
		$data = $this->libs->cache->load('anosSafras', "($anoSQL AND $anoKriSQL AND $unidadeQuery) group by FILIAL, \"ANO AUDITORIA\", NRCONTROLE ORDER BY \"ANO AUDITORIA\", FILIAL, CAST(Substr(NRCONTROLE, 2)AS NUMBER)");
		
		return $data;
	}

	public function getControles($safra, $filial){
		$sqlSafra = null;
		if($safra){
			$sqlSafra = ' AND ANO = '.$safra;
			$ano = $safra;
		}

		$sqlFilial = null;
		if($filial){
			$sqlFilial = " AND REMOVE_ACENTOS(UNIDADE) =  REMOVE_ACENTOS('".$filial."')";
			$unidade = $filial;
		}

		$this->db->ufmt->sqlBuilder->select('REMOVE_ACENTOS(UNIDADE) AS UNIDADE, NRCONTROLE, REMOVE_ACENTOS(TXTCONTROLE) AS TXTCONTROLE, ANO, OID');
		$this->db->ufmt->sqlBuilder->from('dyncintcontroleaud AUD');
		$this->db->ufmt->sqlBuilder->where("1=1 $sqlSafra $sqlFilial");
		$this->db->ufmt->sqlBuilder->orderby('UNIDADE, ANO, substr(NRCONTROLE, 2)');
		return $this->db->ufmt->sqlBuilder->executeQuery();
	}

	//ALTERA A KEY PARA LOWER CASE DE UM ARRAY RECURSIVAMENTE
	public function array_change_key_case_recursive($input, $case = null){
		if(!is_array($input)){
			trigger_error("Invalid input array '{$array}'",E_USER_NOTICE); exit;
		}
		// CASE_UPPER|CASE_LOWER
		if(null === $case){
			$case = CASE_LOWER;
		}
		if(!in_array($case, array(CASE_UPPER, CASE_LOWER))){
			trigger_error("Case parameter '{$case}' is invalid.", E_USER_NOTICE); exit;
		}
		$input = array_change_key_case($input, $case);
		foreach($input as $key=>$array){
			if(is_array($array)){
				$input[$key] = $this->array_change_key_case_recursive($array, $case);
			}
		}
		return $input;
	}

}



