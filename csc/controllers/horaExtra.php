<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


	  class HoraExtra extends ModularCore\Controller{

		  function __construct() {
			  parent::__construct();

			  $this->loadLib('ActiveDirectory');

		  }

    public function index(){
		
    	$this->loadLib('nusoap');
		$this->server = new \nusoap_server();
		$this->server->configureWSDL('MVCore','urn:hudsonventura.com.br');
		$this->server->wsdl->schemaTargetNamespace = 'urn:ModularCore';

		$this->server->register('HoraExtra.armazenaIP',
									array('usuario'=>'xsd:string', 'ip'=>'xsd:string', 'token'=>'xsd:string'),
									array('sucesso'=>'xsd:string', 'mensagem'=>'xsd:string'));

		$this->server->register('HoraExtra.alterarUsuario',
								array('usuario'=>'xsd:string', 'bloqueio'=>'xsd:string', 'loken'=>'xsd:string'),
								array('sucesso'=>'xsd:string', 'mensagem'=>'xsd:string'));


		$data = file_get_contents('php://input');
		$data = isset($data) ? $data : '';
		$this->server->soap_defencoding = 'utf-8';
		$this->server->service($data);
	}
	
	function armazenaIP($usuario = null, $ip = null){

		if(!$usuario && !$ip){
			if(isset($_GET['ip']) && isset($_GET['usuario'])){
				$this->armazenaIP($_GET['usuario'], $_GET['ip']);
				die();
			}
		}
		$this->loadModel('modelHoraExtra', 'horaExtra');

		$registro = array();
		$registro['usuario'] = $usuario;
		$registro['ip'] = $ip;

		$lista = $this->models->horaExtra->inserir($registro);
		var_dump($lista);
		return true;
	}


	function alterarUsuario($login, $bloqueio, $token)// : array
	{
		if(!(isset($token) && $token == 'c2V1X2lwXGZvaV9zYWx2bzplbV9ub3NzYV9iYXNl'))
			return array('sucesso' => '0', 'mensagem'=> 'Você nao tem acesso a este serviço');

		$this->loadLib('ActiveDirectory');
		$this->loadModel('usuarioCSC');

		$usuarioAD = $this->libs->ActiveDirectory->getUser('samaccountname='.$login);
		if (!$usuarioAD)
		{
			return array('sucesso' => '0', 'mensagem'=> "Usuário $login não encontrado no dominio");
		}


		$usuarioHCM = $this->models->usuarioCSC->getUserHCM($usuarioAD)['HCM'][0];


		//////exceção de gestores
		{
			//exceção de cargo de gestao
			$cargos = array('supervisor','gerente','diretor','presidente','acionista','especialista', 'lider', 'coord', 'controller', 'conselho');
			foreach ($cargos as $cargo)
			{
				$pos = strripos($usuarioHCM['CARGO'], $cargo);
				if ($bloqueio == true && is_numeric($pos)) {
					return array('sucesso' => '0', 'mensagem'=> "O usuário $login é um gestor. O sistema de lock não será aplicado");
				}
			}

			//exceção de usuario ser superior a algum outro usuario
			$subordinados = $this->models->usuarioCSC->getSubordinadosHCM($usuarioAD);
			if ($bloqueio == true && count($subordinados)>=1) {
				return array('sucesso' => '0', 'mensagem'=> "O usuário $login é um gestor. O sistema de lock não será aplicado");
			}
		}




		 /* busca os grupos de acesso

		$haveGroup = $this->libs->ActiveDirectory->listUsersofaGroup('memberOf=CN=GU_Lock_User_Excecao,OU=Recursos,OU=Grupos,OU=Servicos,DC=maggi,DC=corp');
		$notHaveGroup = $this->libs->ActiveDirectory->listUsersofaGroup('memberOf=CN=GU_CRP_ACL_CSC,OU=Compartilhamento,OU=Grupos,OU=Servicos,DC=maggi,DC=corp) (!(memberOf=CN=GU_Lock_User_Excecao,OU=Recursos,OU=Grupos,OU=Servicos,DC=maggi,DC=corp)');

		foreach ($haveGroup as $item)
		{
			//var_dump($item['cn'][0]);
		}

		foreach ($notHaveGroup as $item)
		{
			//var_dump($item['cn'][0]);
		}
		//die();


		//var_dump($notHaveGroup);
		//die();




		/*foreach ($usuarioAD['memberof'] as $group)
		{
			if ($group == 'CN=GU_Lock_User_Excecao,OU=Recursos,OU=Grupos,OU=Servicos,DC=maggi,DC=corp' && $bloqueio == true)
			{
				return array('sucesso' => '0', 'mensagem'=> 'Usuario possui exceção de bloqueio. O sistema de lock não será aplicado.');
			}

		}*/

		$return = 'Erro na passagem do ';

		//REMOVE grupo e liberação
		if ($bloqueio == true)
		{
			$returnAddGroup = $this->libs->ActiveDirectory->removeGroup($usuarioAD, 'CN=GU_Lock_User_Excecao,OU=Recursos,OU=Grupos,OU=Servicos,DC=maggi,DC=corp', 'Maggi Corp');
			$returnLogonHours = $this->libs->ActiveDirectory->changeLogonHours($usuarioAD, '00000000f83f00f83f00f83f00f83f00f83f000000');
		}

		//ADICIONA grupo e liberação
		if ($bloqueio == false){
			$returnAddGroup = $this->libs->ActiveDirectory->addGroup($usuarioAD, 'CN=GU_Lock_User_Excecao,OU=Recursos,OU=Grupos,OU=Servicos,DC=maggi,DC=corp', 'Maggi Corp');
			$returnLogonHours = $this->libs->ActiveDirectory->changeLogonHours($usuarioAD, 'ffffffffffffffffffffffffffffffffffffffffff');
		}

		if ($returnAddGroup && $returnLogonHours)
		{
			if ($bloqueio == true)
			{
				return array('sucesso' => '1', 'mensagem'=> "Bloqueado a execução de horas extras para o usuario $login");
			}else{
				return array('sucesso' => '1', 'mensagem'=> "Liberado a execução de horas extras para o usuario $login");
			}


		}else{
			if ($returnAddGroup && !$returnLogonHours){
				return array('sucesso' => '0', 'mensagem'=> "Erro ao alterar o horario de acesso do usuario $login");
			}
			if (!$returnAddGroup && $returnLogonHours){
				return array('sucesso' => '0', 'mensagem'=> "Erro ao alterar grupo de exceção do usuario $login");
			}
			if (!$returnAddGroup && !$returnLogonHours){
				return array('sucesso' => '0', 'mensagem'=> "Erro adicionar grupo de exceção e ao alterar o horario de acesso do usuario $login");
			}
		}






	}
}


