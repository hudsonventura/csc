<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


	  class ReservaSalas extends ModularCore\Controller{

		  function __construct() {
			  parent::__construct();

			  //definição de usuario
			  $this->loadModel("usuarioCSC");
			  $user = $this->models->usuarioCSC->getUserAD($_SERVER['REMOTE_ADDR']);
			  //var_dump($user);
			  if(!$user){
				  die('Primeiro faca login no no portal https://suporte.amaggi.com.br ou no SoftExpert <span style="color: #FFF">'.$_SERVER['REMOTE_ADDR'].'</span>');
			  }
			  //var_dump($user);
			  $this->coreView['usuario'] = $user[0];
			  //definição de usuario

		  }

    public function index(){
    	$this->coreView['title']='Reserva de Salas de Reunião';
    	$this->loadModel('modelReservaSalas', 'model');
    	if(isset($_GET['reservar'])){

    		$temp = explode('_', $_GET['reservar']);
    		$reservar['sala'] = $temp[2];
    		$reservar['usuario'] = $temp[3];

			$reservar['usuarioupdater'] = $this->coreView['usuario']['samaccountname'][0];
    		$reservarData = explode('-', $temp[0]);
    		$reservarHora = explode(':', $temp[1]);
    		$reservar['horario'] = date('Y-m-d H:i:s', mktime($reservarHora[0],$reservarHora[1],0, $reservarData[1], $reservarData[0], $reservarData[2]));
			if(isset($_GET['admin'])){
				$exec = $this->models->model->reservar($reservar, true);
			}else{
				$exec = $this->models->model->reservar($reservar, false);
			}

			echo json_encode($exec); die();
    		//header('Location: '.CONTROLLERDIR.'?dia='.$temp[0]);
    	}


    	if(isset($_GET['reservarDia'])){
    		$temp = explode('_', $_GET['reservarDia']);
			//echo 'ok';
    		$horarios = $this->models->model->listarHorarios($categoria);

    		foreach($horarios as $horario){
				$reservar['horario'] = date('Y-m-d', strtotime(date('d-m-Y', strtotime($temp[0])))).' '.$horario['horario'].':00';
				$reservar['usuario'] = $temp[2];
				$reservar['sala'] = $temp[1];
				$exec = $this->models->model->reservarDia($reservar);
    		}
    		header('Location: '.CONTROLLERDIR.'?dia='.$temp[0]);
    	}

    	if(isset($_GET['liberar'])){
    		$temp = explode('_', $_GET['liberar']);
    		$reservar['sala'] = $temp[2];
    		$reservar['usuario'] = $temp[3];
    		$reservarData = explode('-', $temp[0]);
    		$reservarHora = explode(':', $temp[1]);
    		$reservar['horario'] = date('Y-m-d H:i:s', mktime($reservarHora[0],$reservarHora[1],0, $reservarData[1], $reservarData[0], $reservarData[2]));
    		$exec = $this->models->model->liberar($reservar);
			echo json_encode(1); die();
    		//header('Location: '.CONTROLLERDIR.'?dia='.$temp[0]);
    	}

    	if(isset($_GET['dia'])){
    		$data = $_GET['dia'];
    	}else{
    		$data = date('d-m-Y');
    	}

    	$dataExplode = explode('-', $data);
    	$dataExplode = date('Y-m-d', mktime(0,0,0, $dataExplode[1], $dataExplode[0], $dataExplode[2]));

		if(!isset($_GET['categoria'])){
			die('E necessario informar a categoria a GET');
		}else{
			$categoria = $_GET['categoria'];
		}

		//permite que pela categoria barcarena, somente pessoas com o grupo do AD de fileserver especificos possam acessar a pagina
		if ($categoria == 'barcarena') {
			foreach($this->coreView['usuario']['memberof'] as $item)
			{
				if(strpos($item, 'GL_FileServer_BAR') <> false)
				{
					goto escapaBarcarena;
				}
			}
			die('Acesso somente a colaboradores de Barcarena (grupo do AD GL_FileServer_BAR*).');
		}	
		
		escapaBarcarena:
		

		$reservas = $this->models->model->listarReservas($dataExplode);
		$salas = $this->models->model->listarSalas($categoria);
		$horarios = $this->models->model->listarHorarios();

		//deixar sempre por ultima solicitação no banco
		$usuarios = $this->models->model->listarUsuarios();

		//var_dump($usuarios);

    	$i = 0;
    	$dias = array();
    	for ($i = 0; $i<=22; $i++){
			$dataArray = $this->somar_dias_uteis(date('d/m/Y'), $i);
			//$dataGerada = date('d/m/Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " +$i days"));
			if($dataArray){
				$dataArray = explode('/', $dataArray);
    			$dia		= date('d', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
				$mes		= date('m', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
				$ano		= date('Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
				$semana	= date('w', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
				$ano = date('Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));

    			//switch ($mes){ case 1: $mes = "Jan"; break; case 2: $mes = "Fev"; break; case 3: $mes = "Mar"; break; case 4: $mes = "Abr"; break; case 5: $mes = "Mai"; break; case 6: $mes = "Jun"; break; case 7: $mes = "Jul"; break; case 8: $mes = "Ago"; break; case 9: $mes = "Set"; break; case 10: $mes = "Out"; break; case 11: $mes = "Nov"; break; case 12: $mes = "Dez"; break;}
				switch ($semana){ case 0: $semana = "Dom"; break; case 1: $semana = "Seg"; break; case 2: $semana = "Ter"; break; case 3: $semana = "Qua"; break; case 4: $semana = "Qui"; break; case 5: $semana = "Sex"; break; case 6: $semana = "Sab"; break;}


				if($i == 0){
					$semana = '<b>(Hoje)</b> ';//.$semana;
				}

    			if(date('d-m-Y', strtotime("+$i days")) == $data){
    				array_push($dias, array('view' => $semana.' - '.$dia.'/'.$mes.'/'.$ano, 'link' => date('d-m-Y', strtotime("+$i days")).'&categoria='.$_GET['categoria'], 'class' => 'active'));
    			}else{
    				array_push($dias, array('view' => $semana.' - '.$dia.'/'.$mes.'/'.$ano, 'link' => date('d-m-Y', strtotime("+$i days")).'&categoria='.$_GET['categoria'], 'class' => ''));
    			}
			}
    	}

		//calcula calendario para aparesentar no campo de Outras Datas
		$dataMaxima = strtotime("+21 days");
		$dataCorrente =  explode('-', $data);
		$dataCorrente = mktime(0, 0, 0, $dataCorrente[1], $dataCorrente[0], $dataCorrente[2]);

		if($dataMaxima <= $dataCorrente){
			$dataCorrente = date('d/m/Y', $dataCorrente);
			$this->coreView['dataCorrente'] = $dataCorrente;
		}




    	$tabela = array();
    	foreach($horarios as $horario){
    		$horario = $horario['horario'];
    		$tabela[$horario] = array();
    			foreach($salas as $sala){
    				if($reservas){
    					foreach($reservas as $reserva){
	    					if(substr($reserva['horario'],11, strlen($reserva['horario'])-14) == $horario && $reserva['sala'] == $sala['id']){


	    						foreach($this->coreView['usuario']['memberof'] as $item) {
									if($item == 'CN=csc.processo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp' || $this->coreView['usuario']['samaccountname'][0] == 'keizy.queiroz' || $this->coreView['usuario']['samaccountname'][0] == 'camila.pires')
									{
										if($reserva['usuario'] == $this->coreView['usuario']['samaccountname'][0]){
											$cor = '#11AA11';
										}else{
											$cor = '#888';
											//ar_dump($this->coreView['usuario']['samaccountname']); die();
											if ($this->coreView['usuario']['samaccountname'][0] == 'keizy.queiroz' || $this->coreView['usuario']['samaccountname'][0] == 'camila.pires')
											{
												$cor = '#337ab7';
											}
										}
										$tabela[$horario][$sala['nome']] = '<i style="color: '.$cor.'"><b>'.$reserva['usuario'].'</b></i>';
										goto autorizado;
									}
    								else
 									{
										 if($reserva['usuario'] == $this->coreView['usuario']['samaccountname'][0]){
											 $cor = '#11AA11';
										 }else{
											 $cor = '#888';
										 }

 										if($reserva['usuario'] == $this->coreView['usuario']['samaccountname'][0]){
											 $tabela[$horario][$sala['nome']] = '<i style="color: '.$cor.'">'.$reserva['usuario'].'</i>';
			    						}else{
			    							//foreach($this->coreView['usuario']['memberof'] as $item) {
												//if($item == 'CN=apoio.administrativo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp' || $item == 'CN=csc.processo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp')
			    								//	$tabela[$horario][$sala['nome']] = '<i style="color: #CCCCCC">Reservado1aaa</i>';
			    								//else
			    									$tabela[$horario][$sala['nome']] = '<i style="color: #CCCCCC">'.$reserva['usuario'].'</i>';
			    							//}

			    							///VER SE FUNCIONA
			    							if(isset($usuario['memberof'])){
				    							foreach($this->coreView['usuario']['memberof'] as $item) {
													if(strpos($item, $this->libs->Permission)){
														$tabela[$horario][$sala['nome']] = '<i style="color: #AA1111">'.$reserva['usuario'].'</i>';
													}
												}
											}
											///VER SE FUNCIONA



			    						}
 									}

								}
								autorizado:




	    						BREAK;
	    					}else{
	    						$tabela[$horario][$sala['nome']] = 'Sala Livre';
	    					}
	    				}
    				}else{
    					$tabela[$horario][$sala['nome']] = 'Sala Livre';
    				}

    		}
    	}

    	$this->coreView['salas'] = $salas;
    	$this->coreView['horarios'] = $horarios;
    	$this->coreView['dias'] = $dias;
    	$this->coreView['data'] = $data;
    	$this->coreView['tabela'] = $tabela;
    	$this->coreView['reservas'] = $reservas;
		$this->coreView['usuarios'] = $usuarios;

		//var_dump($salas); die();

		$this->loadView('viewReserva');
		//echo "<meta HTTP-EQUIV='refresh' CONTENT='30;URL=''>";
    }

	private function somar_dias_uteis($data,$dias,$resursivo=0) {
		$dataArray = explode('/', $data);
		$dataGerada = date('d/m/Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " +$dias days"));
		$fdsDataGerada = date('w', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " +$dias days"));
		//$fdsData = date('w', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));


		if($fdsDataGerada == 0 || $fdsDataGerada == 6){
			return null;
			//$resursivo++;
			//$dataGerada = $this->somar_dias_uteis($data, $dias+($resursivo*2), $resursivo);
		}



		return $dataGerada;
	}
}
