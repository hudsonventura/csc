<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


class ModelHoraExtra extends ModularCore\Model{

	function __construct() {
		parent::__construct();

		$this->loadDb('horaextra', 'horaextra');
	}

	public function listarIPs(){
        //var_dump($this->db->horaextra);
		$this->db->horaextra->sqlBuilder->select('*');
		$this->db->horaextra->sqlBuilder->from('usuariosip');
		//echo $this->db->horaextra->sqlBuilder->getQuery(); die();
		return $this->db->horaextra->sqlBuilder->executeQuery();
    }
    
    public function inserir($registro){
		$usuario = $registro['usuario'];
		$ip = $registro['ip'];

		$registro['data'] = 'now';
        $registro['hora'] = 'now';

		$this->db->horaextra->sqlBuilder->select('*');
		$this->db->horaextra->sqlBuilder->from('usuariosip');
		$this->db->horaextra->sqlBuilder->where("usuario = '$usuario' and ip = '$ip'");
		$this->db->horaextra->sqlBuilder->orderby("data desc, hora desc");
		
		$retorno = $this->db->horaextra->sqlBuilder->executeQuery();
		if(count($retorno) >=1){ //necessario fazer update
			$id = $retorno[0]['id'];
			$this->db->horaextra->sqlBuilder->update($registro, 'usuariosip');
			$this->db->horaextra->sqlBuilder->where("id = '$id'");
			//echo $this->db->horaextra->sqlBuilder->getQuery(); die();
		}else{ //insert
			$this->db->horaextra->sqlBuilder->insert($registro, 'usuariosip');
		}
		return $this->db->horaextra->sqlBuilder->executeQuery();
    }

}