<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


class ModelReservaSalas extends ModularCore\Model{

	function __construct() {
		parent::__construct();

		$this->loadDb('salasPRD', 'salas');
	}

	public function listarReservas($data){
		$this->db->salas->sqlBuilder->select('*');
		$this->db->salas->sqlBuilder->from('reservas');
		$this->db->salas->sqlBuilder->leftjoin('salas');
		$this->db->salas->sqlBuilder->on('reservas.sala = salas.id');
		$this->db->salas->sqlBuilder->where("horario > '$data 00:00:00' and horario < '$data 23:23:59'");
		//echo $this->db->salas->sqlBuilder->getQuery(); die();
		return $this->db->salas->sqlBuilder->executeQuery();
	}

	public function listarSalas($categoria){
		//$this->loadDb('salas', 'salas');
		$this->db->salas->sqlBuilder->select('*');
		$this->db->salas->sqlBuilder->from('salas');
		$this->db->salas->sqlBuilder->where("categoria = '$categoria'");
		$this->db->salas->sqlBuilder->orderby('id');

		//echo $this->db->salas->sqlBuilder->getQuery(); die();

		return $this->db->salas->sqlBuilder->execute();
	}

	public function listarHorarios(){
		$this->db->salas->sqlBuilder->select('*');
		$this->db->salas->sqlBuilder->from('horarios');
		$this->db->salas->sqlBuilder->orderby('horario ASC');
		return $this->db->salas->sqlBuilder->executeQuery();
	}

	public function reservar($reserva, $admin){
		$this->db->salas->sqlBuilder->select('id');
		$this->db->salas->sqlBuilder->from('salas');
		$this->db->salas->sqlBuilder->where("nome = '".$reserva['sala']."'");
		$reserva['sala'] = $this->db->salas->sqlBuilder->executeQuery()[0]['id'];

		//se tiver acesso admin, passa sem saber se a sala ja foi reservada ou nao
		if(!$admin){
			$this->db->salas->sqlBuilder->select('usuario');
			$this->db->salas->sqlBuilder->from('reservas');
			$this->db->salas->sqlBuilder->where("horario = '".$reserva['horario']."' AND sala='".$reserva['sala']."'");
			$salaJaReservada = $this->db->salas->sqlBuilder->executeQuery();
		}else{
			$salaJaReservada = null;
			//remove todas as reservas feitas para aquela sala no horario informado
			$this->db->salas->sqlBuilder->delete($reserva);
			$this->db->salas->sqlBuilder->from('reservas');
			$this->db->salas->sqlBuilder->where("horario = '".$reserva['horario']."' AND sala = ".$reserva['sala']);
			$this->db->salas->sqlBuilder->executeQuery();
		}

		if($salaJaReservada){
			return $salaJaReservada[0];
		}else{
			$this->db->salas->sqlBuilder->insert($reserva, 'reservas');
			$return = $this->db->salas->sqlBuilder->executeQuery();
			if ($return) {
				return 1;
			}else{
				return 0;
			}
		}

		echo '123';



	}

	public function reservarDia($reserva){
		$this->db->salas->sqlBuilder->select('id');
		$this->db->salas->sqlBuilder->from('salas');
		$this->db->salas->sqlBuilder->where("nome = '".$reserva['sala']."'");
		$return = $this->db->salas->sqlBuilder->executeQuery();
		$reserva['sala'] = $return[0]['id'];

		$this->db->salas->sqlBuilder->insert($reserva, 'reservas');
		return $this->db->salas->sqlBuilder->executeQuery();
	}

	public function liberar($reserva){
		$this->db->salas->sqlBuilder->select('id');
		$this->db->salas->sqlBuilder->from('salas');
		$this->db->salas->sqlBuilder->where("nome = '".$reserva['sala']."'");
		$return = $this->db->salas->sqlBuilder->executeQuery();
		$reserva['sala'] = $return[0]['id'];

		$this->db->salas->sqlBuilder->delete($reserva);
		$this->db->salas->sqlBuilder->from('reservas');
		$this->db->salas->sqlBuilder->where("horario = '".$reserva['horario']."' AND sala = ".$reserva['sala']);
		return $this->db->salas->sqlBuilder->executeQuery();
	}

	public function listarUsuarios(){
		$this->loadDb('oraclePRD', 'oracle');

		$this->db->oracle->sqlBuilder->select('lower(idlogin) as idlogin');
		$this->db->oracle->sqlBuilder->FROM('aduser');
		$this->db->oracle->sqlBuilder->where("FGUSERENABLED = '1' and idlogin like '%.%'");
		$this->db->oracle->sqlBuilder->orderby('idlogin');

		$return = $this->db->oracle->sqlBuilder->executeQuery();

		return $return;
	}
}