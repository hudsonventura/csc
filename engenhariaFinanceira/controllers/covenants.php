<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
header('Content-Type:text/html;charset=utf-8');


class covenants extends ModularCore\Controller{

	private $operacoesGarantias = array(	'17466' => array('Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Al Ghurair (United Arab Emirates)','Alimentos Agrosuper (Chile)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','Cereal Docks International','Chinatex (China)','CHS Inc. (USA)','CJ International (Singapore)','Concordia Trading (Netherlands)','Gardner Smith (Australia)','Glencore (UK or Holland)','Graneles de Chile (Chile)','Intergrain Company (USA)','Kuok (Singapore)','Lansing Trade(USA)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','R&H Hall PLC (Ireland)','Scanmills AS (USA)','Sumitomo (Japan)','Toepfer (Germany)','W&R Barnett (Ireland)','Wilmar (China)','Zen Noh Grain (Japan)'),
										'17834' => array('Agrex Inc. (USA)','Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Al Ghurair (United Arab Emirates)','Alimentos Agrosuper (Chile)','Amaggi Europe BV (Netherlands)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','CGG Trading (Netherlands)','Chinatex (China)','CHS Inc. (USA)','CJ International (Singapore)','Concordia Trading (Netherlands)','Denofa (Norway)','Gardner Smith (Australia)','Gavillon Grain (USA)','Glencore (UK or Holland)','Graneles de Chile (Chile)','Intergrain Company (USA)','Kuok (Singapore)','Lansing Trade(USA)','Lesieur Cristal (Morocco)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','R&H Hall PLC (Ireland)','Rice Company (USA)','Scanmills AS (USA)','Sodrugestvo (Russia)','Soya Mills SA (Greece)','Sumitomo (Japan)','Toepfer (Germany)','W&R Barnett (Ireland)','Wilmar (China)','Zen Noh Grain (Japan)'),
										'21314' => array('Agrex Inc. (USA)','Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Al Ghurair (United Arab Emirates)','Alimentos Agrosuper (Chile)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','CGG Trading (Netherlands)','Chinatex (China)','CHS Inc. (USA)','CJ International (Singapore)','Concordia Trading (Netherlands)','Denofa (Norway)','Gardner Smith (Australia)','Gavillon Grain (USA)','Glencore (UK or Holland)','Graneles de Chile (Chile)','Intergrain Company (USA)','Kuok (Singapore)','Lansing Trade(USA)','Lesieur Cristal (Morocco)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','R&H Hall PLC (Ireland)','Rice Company (USA)','Scanmills AS (USA)','Sodrugestvo (Russia)','Soya Mills SA (Greece)','Sumitomo (Japan)','Toepfer (Germany)','W&R Barnett (Ireland)','Wilmar (China)','Zen Noh Grain (Japan)'),
										'19077' => array('Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Al Ghurair (United Arab Emirates)','Alfred C Toepfer International (Germany)','Alimentos Agrosuper (Chile)','Amaggi Europe BV (Netherlands)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','Cereal Docks International','CGG Trading (Netherlands)','Chinatex (China)','CHS Inc. (USA)','CJ International (Singapore)','Concordia Trading (Netherlands)','Denofa (Norway)','DLG Amba','Gardner Smith (Australia)','Gavillon Grain (USA)','Glencore (UK or Holland)','Graneles de Chile (Chile)','Intergrain Company (USA)','Kuok (Singapore)','Lansing Trade(USA)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','R&H Hall PLC (Ireland)','Rice Company (USA)','Scanmills AS (USA)','Sumitomo (Japan)','Toepfer (Germany)','Vitol SA','W&R Barnett (Ireland)','Wilmar (China)','Zen Noh Grain (Japan)'),
										'17016' => array('Agrex Inc. (USA)','Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Al Ghurair (United Arab Emirates)','Alimentos Agrosuper (Chile)','Amaggi Europe BV (Netherlands)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','Chinatex (China)','CHS Inc. (USA)','CJ International (Singapore)','Concordia Trading (Netherlands)','Denofa (Norway)','Gardner Smith (Australia)','Gavillon Grain (USA)','Glencore (UK or Holland)','Graneles de Chile (Chile)','Intergrain Company (USA)','Kuok (Singapore)','Lansing Trade(USA)','Lesieur Cristal (Morocco)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','R&H Hall PLC (Ireland)','Rice Company (USA)','Scanmills AS (USA)','Sodrugestvo (Russia)','Soya Mills SA (Greece)','Sumitomo (Japan)','Toepfer (Germany)','W&R Barnett (Ireland)','Wilmar (China)','Zen Noh Grain (Japan)'),
										'19453' => array('Agrex Inc. (USA)','Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','Chinatex (China)','CHS Inc. (USA)','Glencore (UK or Holland)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','R&H Hall PLC (Ireland)','Sumitomo (Japan)','Toepfer (Germany)','W&R Barnett (Ireland)','Wilmar (China)','Zen Noh Grain (Japan)'),
										'17986' => array('Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','Chinatex (China)','CHS Inc. (USA)','Concordia Trading (Netherlands)','Gardner Smith (Australia)','Glencore (UK or Holland)','Kuok (Singapore)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','Rice Company (USA)','Sumitomo (Japan)','Toepfer (Germany)','Wilmar (China)','Zen Noh Grain (Japan)'),
										'19935' => array('Agrex Inc. (USA)','Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Al Ghurair (United Arab Emirates)','Alimentos Agrosuper (Chile)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','Cereal Docks International','Chinatex (China)','CHS Inc. (USA)','CJ International (Singapore)','Denofa (Norway)','Glencore (UK or Holland)','Graneles de Chile (Chile)','Intergrain Company (USA)','Kuok (Singapore)','Lansing Trade(USA)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','R&H Hall PLC (Ireland)','Scanmills AS (USA)','Sumitomo (Japan)','Toepfer (Germany)','W&R Barnett (Ireland)','Wilmar (China)','Zen Noh Grain (Japan)'),
										'19797' => array('Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Alimentos Agrosuper (Chile)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','CGG Trading (Netherlands)','CHS Inc. (USA)','Concordia Trading (Netherlands)','DLG Amba','Gavillon Grain (USA)','Glencore (UK or Holland)','Intergrain Company (USA)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','Sumitomo (Japan)','Toepfer (Germany)','Wilmar (China)'),
										'19797' => array('Agro Supply/DLG (Denmark)','Agrograin Ltd. (Cayman Islands)','Alimentos Agrosuper (Chile)','Ameropa (Switzerland)','Archer Daniel Midlands (USA)','Bunge (USA)','Cargill (USA)','Cefetra (Netherlands)','CGG Trading (Netherlands)','CHS Inc. (USA)','Concordia Trading (Netherlands)','DLG Amba','Gavillon Grain (USA)','Glencore (UK or Holland)','Intergrain Company (USA)','Louis Dreyfus (France)','Marubeni (Japan)','Multigrain AG (Switzerland)','Nidera (Netherlands)','Noble Grain (Singapore)','Sumitomo (Japan)','Toepfer (Germany)','Wilmar (China)'),
								);

								
								
	function __construct() {
		parent::__construct();


		//definição de usuario
		$this->loadModel("usuarioCSC");
		$user = $this->models->usuarioCSC->getUserAD($_SERVER['REMOTE_ADDR']);
		if(!$user){
			die('Primeiro faca login no no portal https://suporte.amaggi.com.br ou no SoftExpert <span style="color: #FFF">'.$_SERVER['REMOTE_ADDR'].'</span>');
		}
		$this->coreView['usuario'] = $user;
		$this->usuario = $user;
		//definição de usuario

		$this->loadModel('covenantsModel');
	}




	function obrigacoes(){

		$this->coreView['title'] = 'Relatório de Covenants';



		//buscas
		$buscaOperacoes = null;
		if(isset($_GET['buscaOperacoes'])){
			$buscaOperacoes = $_GET['buscaOperacoes'];
		}

		$realizadas = null;
		if(isset($_GET['realizadas'])){
			$realizadas = true;
		}
		$this->coreView['realizadas'] = $realizadas;

		$futuras = null;
		if(isset($_GET['futuras'])){
			$futuras = true;
		}
		$this->coreView['futuras'] = $futuras;

		$dataInicio = null;
		if(isset($_GET['dataInicio'])){
			$dataInicio = $_GET['dataInicio'];
		}
		$this->coreView['dataInicio'] = $dataInicio;

		$dataFim = null;
		if(isset($_GET['dataFim'])){
			$dataFim = $_GET['dataFim'];
		}
		$this->coreView['dataFim'] = $dataFim;

		//buscas



		$resultadoBanco_Obrigacoes = $this->models->covenantsModel->getObrigacoes($buscaOperacoes, $realizadas, $futuras, $dataInicio, $dataFim);
		//var_dump($resultadoBanco_Obrigacoes); die();


		$this->coreView['operacoesGarantias'] = $this->operacoesGarantias;
		$this->coreView['buscaOperacoes'] = $buscaOperacoes;
		$this->coreView['realizadas'] = $realizadas;

		$this->coreView['resultadoBanco_Obrigacoes'] = $resultadoBanco_Obrigacoes;


		//var_dump($this->coreView); die();
		$this->loadView('obrigacoesView');
	}

	function executaObrigacao(){

		if(isset($_GET['op']) && isset($_GET['venc']) && isset($_GET['auditoria'])){

			//execução de obrigação comum
			if ($_GET['auditoria'] == 0 || $_GET['auditoria'] == 1)
			{
				$resultadoBanco_ExecutaObrigacao = $this->models->covenantsModel->executaObrigacao(array('OPERACAO'=>$_GET['op'],'VENC'=>$_GET['venc'], 'EXECUSER' => $this->usuario['IDLOGIN'], 'AUDITORIA' => $_GET['auditoria'], 'TIPO' => 0));
			}

			//execução de outras obrigações
			if ($_GET['auditoria'] == 2){
				$resultadoBanco_ExecutaObrigacao = $this->models->covenantsModel->executaOutrasObrigacoes(array('OPERACAO'=>$_GET['op'],'VENC'=>$_GET['venc']));
			}



			if($resultadoBanco_ExecutaObrigacao){
				header('Location: '.CONTROLLERDIR.'/obrigacoes');
			}else{
				echo '<h1>Algo de errado aconteceu</h1> ';
				echo '<h4>Operacao: '.$_GET['op'].'</h4>';
				echo '<h4>vencimento: '.$_GET['venc'].'</h4>';
				echo '<h4>Usuario: '.$this->usuario['IDLOGIN'].'</h4>';
				echo '<h4>Tipo: obrigacao</h4>';
			}
		}else{
			exit('<h2>ERROR 403 - FORBIDDEN</h2> Voce nao tem permissao para acessar esta pagina');
		}

	}





	function garantias(){
		$this->coreView['title'] = 'Relatório de Garantias';

		$this->coreView['operacoesGarantias'] = $this->operacoesGarantias;

		//buscas
		$buscaOperacoes = null;
		if(isset($_GET['buscaOperacoes'])){
			$buscaOperacoes = $_GET['buscaOperacoes'];
		}
		$this->coreView['buscaOperacoes'] = $buscaOperacoes;

		$realizadas = null;
		if(isset($_GET['realizadas'])){
			$realizadas = true;
		}
		$this->coreView['realizadas'] = $realizadas;

		$futuras = null;
		if(isset($_GET['futuras'])){
			$futuras = true;
		}
		$this->coreView['futuras'] = $futuras;

		$dataInicio = null;
		if(isset($_GET['dataInicio'])){
			$dataInicio = $_GET['dataInicio'];
		}
		$this->coreView['dataInicio'] = $dataInicio;

		$dataFim = null;
		if(isset($_GET['dataFim'])){
			$dataFim = $_GET['dataFim'];
		}
		$this->coreView['dataFim'] = $dataFim;
		//buscas



		$this->coreView['resultadoBanco_Obrigacoes'] = $this->models->covenantsModel->getGarantias($buscaOperacoes, $realizadas, $futuras, $dataInicio, $dataFim);

		//var_dump($resultadoBancoContratosGarantias); die();





		//var_dump($this->coreView); die();
		$this->loadView('garantiasView');
	}


	function saveEtapasGarantias(){

		$array = array();
		$array['CMP1'] = 0;
		$array['CMP2'] = 0;
		$array['CMP3'] = 0;
		$array['CMP4'] = 0;
		$array['CMP5'] = 0;
		$array['OBS1'] = '';
		$array['OBS2'] = '';
		$array['OBS3'] = '';
		$array['OBS4'] = '';
		$array['OBS5'] = '';
		$array['NOPERAOXRT'] = $_REQUEST['NOPERAOXRT'];
		$array['DATAVENC'] = $_REQUEST['DATAVENC'];

		if(isset($_REQUEST['cmp1'])){
			$array['CMP1'] = 1;
		}
		if(isset($_REQUEST['cmp2'])){
			$array['CMP2'] = 1;
		}
		if(isset($_REQUEST['cmp3'])){
			$array['CMP3'] = 1;
		}
		if(isset($_REQUEST['cmp4'])){
			$array['CMP4'] = 1;
		}
		if(isset($_REQUEST['cmp5'])){
			$array['CMP5'] = 1;
		}

		if(isset($_REQUEST['obs1'])){
			$array['OBS1'] = $_REQUEST['obs1'];
		}
		if(isset($_REQUEST['obs2'])){
			$array['OBS2'] = $_REQUEST['obs2'];
		}
		if(isset($_REQUEST['obs3'])){
			$array['OBS3'] = $_REQUEST['obs3'];
		}
		if(isset($_REQUEST['obs4'])){
			$array['OBS4'] = $_REQUEST['obs4'];
		}
		if(isset($_REQUEST['obs5'])){
			$array['OBS5'] = $_REQUEST['obs5'];
		}



		$resultadoBancoRetorno = $this->models->covenantsModel->getEtapasGarantias($array['NOPERAOXRT'], $array['DATAVENC']);
		//var_dump($resultadoBancoRetorno); die();
		if($resultadoBancoRetorno){
			$resultadoBancoSalvar = $this->models->covenantsModel->updateEtapasGarantias($array, $resultadoBancoRetorno[0]);


			if($array['CMP1'] == 1 && $array['CMP2'] == 1 && $array['CMP3'] == 1 && $array['CMP4'] == 1 && $array['CMP5'] == 1){
				$insert = array('OPERACAO'=>$array['NOPERAOXRT'], 'EXECUSER' => $this->usuario['IDLOGIN'], 'AUDITORIA' => '0', 'TIPO' => 1);
				$insert['VENC'] = substr($array['DATAVENC'], 0, 2).'/'.substr($array['DATAVENC'], 2, 2).'/'.substr($array['DATAVENC'], 4, 4);
				$resultadoBanco_ExecutaObrigacao = $this->models->covenantsModel->executaObrigacao($insert);
				if (!$resultadoBanco_ExecutaObrigacao)
				{
					var_dump($insert);
					die('Erro ao inserir informações no banco de dados');
				}
			}
		}else{
			$resultadoBancoSalvar = $this->models->covenantsModel->insertEtapasGarantias($array);
		}
		$url = CONTROLLERDIR.'/garantias';
		
		//var_dump($array);
		header("Location: $url");
	}

	function getEtapasGarantias(){
		$resultadoBanco = $this->models->covenantsModel->getEtapasGarantias($_REQUEST['NOPERAOXRT'], $_REQUEST['DATA_VENC']);
		if($resultadoBanco){
			echo json_encode($resultadoBanco);
		}else{
			echo json_encode(array(array('exitenobanco' => false, 'NOPERAOXRT' => $_REQUEST['NOPERAOXRT'], 'DATAVENC' => $_REQUEST['DATA_VENC'])));
		}

	}


	function getContratosGarantias(){
		if(isset($_GET['operacao']) && isset($_GET['data_venc'])){

			$data_venc = substr($_GET['data_venc'], 0, 2).'/'.substr($_GET['data_venc'], 2, 2).'/'.substr($_GET['data_venc'], 4);
			$resultadoBancoContratosGarantias = $this->models->covenantsModel->getContratosGarantias($_GET['operacao'], $data_venc);

			echo '<table class="table table-bordered table-condensed table-hover">
									<tr>
                                        <th></th>
                                        <th>Importador</th>
										<th>Nº Contrato</th>
										<th>Produto</th>
										<th>Quant.</th>
										<th>Período</th>
										<th>Cotação</th>
										<th>Valor</th>
									</tr>';

			$somaTotal = 0;
			foreach ($resultadoBancoContratosGarantias as $somar)
			{
				if ($somar['FGENABLED'])
				{
					switch ($somar['PRODUTO'])
					{


						case 'SOJA': $indice = 0.367454; break;
						case 'MILHO': $indice = 0.39368; break;
						case 'FARELO': $indice = 1.1023; break;

						default: $indice = 1;
					}




					$quant = str_replace(',', '.', $somar['QUANT']);
					$cotacao = str_replace(',', '.', $somar['COTACAO']);
					$somaTotal = $somaTotal+($quant*$cotacao*$indice);
				}
			}
			$somaTotal = str_replace(',', '.', $somaTotal);


			foreach ($resultadoBancoContratosGarantias as $contratoGarantia)
			{
				{
					switch ($contratoGarantia['PRODUTO'])
					{
						case 'SOJA': $indice = 0.367454; break;
						case 'MILHO': $indice = 0.39368; break;
						case 'FARELO': $indice = 1.1023; break;

						default: $indice = 1;
					}



					$quant = str_replace(',', '.', $contratoGarantia['QUANT']);
					$cotacao = str_replace(',', '.', $contratoGarantia['COTACAO']);
					$total = $quant*$cotacao*$indice;
					//$total = str_replace('.', ',', $quant*$cotacao*$indice);
					$parcelaID = str_replace('/', '', $contratoGarantia['PARCELA']);

					$enabled = false;
					if ($contratoGarantia['FGENABLED'] == 1)
					{
						$enabled = true;
					}

					$coluna = "style='color: #BBB;'";
					$buttonRemove = "<td></td>";
					if($enabled)
					{
						$coluna = '';
						$buttonRemove = '<td><a type="button" class="btn btn-danger" onclick="formEtapasRemocao(\''.CONTROLLERDIR.'/removeContratosGarantias?oid='.$contratoGarantia['OID'].'\')"><i class="fa fa-minus" aria-hidden="true"></i></a>';
						$buttonRemove .= ' <a type="button" class="btn btn-warning" onclick="editContratoGarantias(\''.$contratoGarantia['OID'].'\')" data-toggle="modal" data-target="#myModalAddGarantias"><i class="fa fa-edit" aria-hidden="true"></i></a></td>';
					}else{
						$buttonRemove = '<td><a type="button" class="btn btn-danger" onclick="formEtapasRemocao(\''.CONTROLLERDIR.'/removeContratosGarantias?oid='.$contratoGarantia['OID'].'\')"><i class="fa fa-minus" aria-hidden="true"></i></a>';
					}




					echo "<tr>";
					echo $buttonRemove;
					echo "<td $coluna>".$contratoGarantia['IMPORTADOR']."</td>";
					echo "<td $coluna>".$contratoGarantia['CONTRATO']."</td>";
					echo "<td $coluna>".$contratoGarantia['PRODUTO']."</td>";
					echo "<td $coluna>".number_format($contratoGarantia['QUANT'])."</td>";
					echo "<td $coluna>".$contratoGarantia['PERIODO']."</td>";
					echo "<td $coluna>".$contratoGarantia['COTACAO']."</td>";
					//echo $total;
					echo "<td $coluna>".'USD ' . number_format($total, 2)."</td>";
					echo "</tr>";
				}

			}

			echo "<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><b>".'USD ' .number_format($somaTotal, 2)."</b></td>
				</tr>
			</table>";

		}

	}

	function getContratoGarantias(){
		if(isset($_GET['contrato'])){
			$dbResult = $this->models->covenantsModel->getContratoGarantias($_GET['contrato'], null);
		}

		if(isset($_GET['oid'])){
			$dbResult = $this->models->covenantsModel->getContratoGarantias(null, $_GET['oid']);
		}

		echo json_encode($dbResult);
	}

	function removeContratosGarantias(){
		if(isset($_GET['oid'])){
			$resultadoBancoContratosGarantias = $this->models->covenantsModel->removeContratosGarantias($_GET['oid']);

			header("Location: ".CONTROLLERDIR.'/garantias');
		}
	}

	function addContratosGarantias(){
		if(isset($_REQUEST['importador']) && isset($_REQUEST['contrato']) && isset($_REQUEST['produto']) && isset($_REQUEST['parcelaAddGarantias']) && isset($_REQUEST['quant']) && isset($_REQUEST['periodo']) && isset($_REQUEST['cotacao']) && isset($_REQUEST['operacaoAddGarantias'])){

			$insert = array();
			$insert['OID'] = date('YmdHis').rand(1, 1000);
			$insert['IMPORTADOR'] = $_REQUEST['importador'];
			$insert['CONTRATO'] = $_REQUEST['contrato'];
			$insert['PRODUTO'] = $_REQUEST['produto'];
			$insert['PARCELA'] = substr($_REQUEST['parcelaAddGarantias'], 0, 2).'/'.substr($_REQUEST['parcelaAddGarantias'], 2, 2).'/'.substr($_REQUEST['parcelaAddGarantias'], 4, 4);
			$insert['QUANT'] = $_REQUEST['quant'];
			$insert['PERIODO'] = $_REQUEST['periodo'];
			$insert['COTACAO'] = $_REQUEST['cotacao'];
			$insert['OPERACAO'] = $_REQUEST['operacaoAddGarantias'];

			$resultadoBancoContratosGarantias = $this->models->covenantsModel->addContratosGarantias($insert);
			header('Location: '.CONTROLLERDIR.'/garantias');
		}else{
			echo 'Erro na requisição';
			die();
		}
	}


	function executaGarantias(){


		if(isset($_GET['op']) && isset($_GET['venc'])){
			$resultadoBanco_ExecutaObrigacao = $this->models->covenantsModel->executaObrigacao(array('OPERACAO'=>$_GET['op'],'VENC'=>$_GET['venc'], 'EXECUSER' => $this->usuario['IDLOGIN'], 'AUDITORIA' => '0', 'TIPO' => 1));

			if($resultadoBanco_ExecutaObrigacao){
				header('Location: '.CONTROLLERDIR.'/garantias');
			}else{
				echo '<h1>Algo de errado aconteceu</h1> ';
				echo '<h4>Operacao: '.$_GET['op'].'</h4>';
				echo '<h4>vencimento: '.$_GET['venc'].'</h4>';
				echo '<h4>Usuario: '.$this->usuario['IDLOGIN'].'</h4>';
				echo '<h4>Tipo: garantias</h4>';
			}
		}else{
			exit('<h2>ERROR 403 - FORBIDDEN</h2> Voce nao tem permissao para acessar esta pagina');
		}

	}





}

