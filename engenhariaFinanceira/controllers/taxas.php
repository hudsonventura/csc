<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
header('Content-Type:text/html;charset=ISO-8859-1');

class Taxas extends ModularCore\Controller{

	private $server;

	function __construct() {

		parent::__construct();
		error_reporting(0);
		$this->loadModel('taxasModel');
	}

	//fun��o de configura��o
	function ptax(){

		$this->loadLib('nusoap');
		$this->server = new \nusoap_server();
		$this->server->configureWSDL('ModularCore','urn:hudsonventura.com.br');
		$this->server->wsdl->schemaTargetNamespace = 'urn:ModularCore';

		$this->server->register('getTaxa',
								array('data'=>'xsd:string'),
								array('dolarCompra'=>'xsd:string', 'dolarVenda'=>'xsd:string', 'euroCompra'=>'xsd:string', 'euroVenda'=>'xsd:string', 'libraCompra'=>'xsd:string', 'libraVenda'=>'xsd:string'));

		$this->server->register('converteRealDolar',
								array('real'=>'xsd:string'),
								array('dolar'=>'xsd:string'));

		$this->server->register('converteDolarReal',
								array('dolar'=>'xsd:string'),
								array('real'=>'xsd:string'));
								
		$this->server->register('converteRealDolarTaxa',
								array('real'=>'xsd:string', 'indice'=>'xsd:string'),
								array('dolar'=>'xsd:decimal'));

		$this->server->register('converteDolarRealTaxa',
								array('dolar'=>'xsd:string', 'indice'=>'xsd:string'),
								array('real'=>'xsd:decimal'));

		$data = file_get_contents('php://input');
		$data = isset($data) ? $data : '';
		$this->server->soap_defencoding = 'utf-8';
		$this->server->service($data);
	}
	
	function ptaxDash1(){
		
		
		
		
		
		
		
		
		$retorno = $this->models->taxasModel->getTaxas("DATA >= CURRENT_DATE - interval '60 days' ");
		$this->coreView['banco'] = $retorno;
		if (isset($_GET['export']))
		{
			$this->loadView('ptaxDash1ExportView');	
		}else{
			$this->loadView('ptaxDash1View');
		}
		
		
		
		
		
		
	}
	
	
	function ptaxEnviarEmailDiario(){
		
			$retorno = $this->models->taxasModel->getTaxas("DATA >= CURRENT_DATE - interval '22 days' "); //pega somente os dias uteis
			
			//iniciando as variavei
			$minimo = 99999999;
			$medio = 0;
			$maximo = 0;
			$atual = 0;
			$variacao = 0;
			
			foreach ($retorno as $dia) {
				if ($minimo > $dia['dolarVenda']) {
					$minimo = number_format($dia['dolarVenda'],4,",",".");
				}
				if ($maximo < $dia['dolarVenda']) {
					$maximo = number_format($dia['dolarVenda'],4,",",".");
				}
				$medio +=  $dia['dolarVenda'];
				$dataPrimeira = new DateTime($dia['data']);
			}
			
			$medio = number_format($medio/count($retorno),4,",",".");
			
			$dolarVenda = number_format($retorno[0]['dolarVenda'],4,",",".");
			$dolarVendaAnterior = number_format($retorno[1]['dolarVenda'],4,",",".");
			$dolarCompra = number_format($retorno[0]['dolarCompra'],4,",",".");
			$data = date('d/m/Y');
			$dataPrimeira = $dataPrimeira->format('d/m/Y');
			$variacao = number_format((($retorno[0]['dolarVenda']-$retorno[1]['dolarVenda'])*100)/$retorno[1]['dolarVenda'],4,",",".");
			
			
			
		
			//die();
			//envio de email de confirmação de reset de senha
			$this->loadLib('Mail');
			$this->libs->Mail->AddAddress('hudson.ventura@amaggi.com.br');
			$this->libs->Mail->AddAddress('csc.financeiro@amaggi.com.br');

			$this->libs->Mail->From = 'suporte.infra@amaggi.com.br';
			$this->libs->Mail->FromName = 'Sistema SE';
			$this->libs->Mail->Subject  = "PTAX Cadastrada em $data";

			
			
			$this->libs->Mail->Body =
											'<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
												<html>
												  <head>
												    <meta http-equiv="Content-Type" content="text/html;UTF-8" />
												  </head>
												  <body style="margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;" text="#444444" bgcolor="#F4F3F4" link="#21759B" alink="#21759B" vlink="#21759B" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
												    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">
												      <tbody>
												        <tr>
												          <td style="padding: 15px;"><center>
												            <table width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
												              <tbody>
												                <tr>
												                  <td align="left">
												                    <div style="border: solid 1px #d9d9d9;">
												                      <table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" border="0" width="93%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
												                        <tbody>
												                          <tr>
												                            <td style="color: #ffffff;" colspan="2" valign="bottom" height="30">.</td>
												                          </tr>
												                          <tr>
												                            <td style="line-height: 32px; padding-left: 30px;" valign="baseline"><span style="font-size: 32px;"><a style="text-decoration: none;" href="https://suporte.amaggi.com.br" target="_blank"><img class="alignnone size-full wp-image-21" src="https://suporte.amaggi.com.br/repositorios/imagens/csc/logo_csc.png" alt="Logo-CSC" /></a></span></td>
												                            <td style="padding-right: 30px;" align="right" valign="baseline"><span style="font-size: 14px; color: #777777;"></span></td>
												                          </tr>
												                        </tbody>
												                      </table>
												                      <table id="content" style="margin-top: 15px; padding-left: 30px; color: #444; line-height: 1.6; font-size: 12px; font-family: Arial, sans-serif;" border="0" width="93%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
												                        <tbody>
																			<tr>
																				<td colspan="2">
																					<div style="padding: 15px; border-top: solid 1px #d9d9d9;"><br /><br />
																						
																					
																					
																					
																					<div style="border: solid 1px #f9f9f9;">
																						<h4>PTAX Cadastrada em '.$data.'</h4>
												                      					<table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" border="0" width="93%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
																							
																							<tr>
																								<td><b>Data</b></td>
																								<td><b>Ptax Anterior (D-1)</b></td>
																								<td><b>Compra</b></td>
																								<td><b>Venda</b></td>
																								<td><b>Variação</b></td>
																							</tr>
																							
																							<tr>
																								<td>'.$data.'</td>
																								<td>'.$dolarVendaAnterior.'</td>
																								<td>'.$dolarCompra.'</td>
																								<td>'.$dolarVenda.'</td>
																								<td>'.$variacao.' %</td>
																							</tr>
																						</table>
																						<br><br>
																						<h4>Gráfico dos últimos dias ('.$dataPrimeira.' até '.$data.')</h4>
																						<table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" border="0" width="93%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
																							
																							<tr>
																								<td><b>Mínimo</b></td>
																								<td><b>Médio</b></td>
																								<td><b>Máximo</b></td>
																								<td><b>Atual</b></td>
																							</tr>
																							
																							<tr>
																								<td>'.$minimo.'</td>
																								<td>'.$medio.'</td>
																								<td>'.$maximo.'</td>
																								<td>'.$dolarVenda.'</td>
																							</tr>
																						</table>
																						
																						
																						
																					</div>
																					<br>
																					Para acessar o gráfico com dados detalhados, visite: <a href="https://csc.amaggi.com.br/engenhariafinanceira/taxas/ptaxDash1" rel="nofollow">https://csc.amaggi.com.br/engenhariafinanceira</a><br />




																						<br />
																					</div>
																				</td>
																			</tr>
												                        </tbody>
												                      </table>

												                    </div>
												                  </td>
												                </tr>
												              </tbody>
												            </table>
												            </center>
															</td>
												        </tr>
												      </tbody>
												    </table>
												  </body>
												</html>';
			
			
			$resultadoEmail = $this->libs->Mail->Send();
			echo $resultadoEmail;
	}







	//fun��es auxiliares

	function getTaxaBanco($data = null){
		$retorno = $this->models->taxasModel->getTaxasData($data);
		return array('dolarVenda'=> moedaBrasil($retorno['dolarVenda']), 'dolarCompra'=> moedaBrasil($retorno['dolarCompra']),
						'euroVenda'=> moedaBrasil($retorno['euroVenda']), 'euroCompra'=> moedaBrasil($retorno['euroCompra']),
						'libraVenda'=> moedaBrasil($retorno['libraVenda']), 'libraCompra'=> moedaBrasil($retorno['libraCompra']) );

	}

	function converteDolarReal($real){
		$real = str_replace(',','.', str_replace('.','', $real));
		$taxa = $this->models->taxasModel->getTaxaUltima()['dolarVenda'];;
		$valor = $real*$taxa;
		return moedaBrasil3Digitos($valor);
	}


	function converteRealDolar($dolar){
		$dolar = str_replace(',','.', str_replace('.','', $dolar));
		$taxa = $this->models->taxasModel->getTaxaUltima()['dolarVenda'];
		$valor = $dolar/$taxa;
		return moedaBrasil3Digitos($valor);
	}

	function converteDolarRealTaxa($real, $indice){
		return $indice;
		$real = str_replace(',','.', str_replace('.','', $real));
		$indice = str_replace(',','.', str_replace('.','', $indice));
		return $real*$indice;
	}

		
	function converteRealDolarTaxa($real, $indice){
		$real = str_replace(',','.', str_replace('.','', $real));
		$indice = str_replace(',','.', str_replace('.','', $indice));
	  return  $real/$indice;
	}


}

function moedaBrasil($valor){
	//$retorno = number_format($valor, 2);
	$retorno =  str_replace('.',',', str_replace(',','', number_format($valor, 2)));
	return $retorno;
}

function moedaBrasil3Digitos($valor){
	$retorno =  str_replace('.',',', str_replace(',','', number_format($valor, 3)));
	return $retorno;
}

function getTaxa($data = null){
	$object = new Taxas();
	$retorno = $object->getTaxaBanco($data);
	if($retorno){
		return $retorno;
	}else{
		return array(0,0,0,0,0,0);
	}

}

function converteDolarReal($real){
	$object = new Taxas();
	$retorno = $object->converteDolarReal($real);
	return $retorno;
}

function converteRealDolar($dolar){
	$object = new Taxas();
	$retorno = $object->converteRealDolar($dolar);
	return $retorno;
}

function converteDolarRealTaxa($real, $indice){
	$object = new Taxas();
	$retorno = $object->converteDolarRealTaxa($real, $indice);
	return $retorno;
}

function converteRealDolarTaxa($dolar, $indice){
	$object = new Taxas();
	$retorno = $object->converteRealDolarTaxa($dolar, $indice);
	return $retorno;
}