<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />

    <!--Imports-->
    <!-- Bootstrap Core CSS -->
    <link href="						<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- jQuery -->
	<script src="						<?php echo DEFAULTVIEWDIR;?>assets/jquery/js/jquery.min.js"></script>
    <!-- Bootstrap Core JS -->
    <script src="						<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom Fonts -->
    <link href="						<?php echo DEFAULTVIEWDIR;?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <title><?php echo $title ?></title>

    <!--Imports-->
    <script src="<?php echo ASSETS?>ckeditor/ckeditor.js"></script>
    <script src="<?php echo ASSETS?>ckeditor/config.js"></script>
    <script src="<?php echo ASSETS?>ckeditor/plugins/image/dialogs/image.js"></script>

</head>










<body>
	<div class="">
		<div class="">
			<div class="panel text-center">
				<div class="panel-heading" style="background-color: #fff;">
					<h2><?php echo $title?></h2>
				</div>
				<div class="panel-body">

					<!--Busca-->
					<div class="col-md-12 form-horizontal">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Busca</h3>
							</div>
							<div class="panel-body ">
								<form method="GET" action="" id="form" name="form">
									<div class="row">
										<div class="form-group col-sm-4">
											<label class="col-sm-3 control-label" for="atividade">Operações</label>
											<div class="col-sm-8">
												<input class="form-control" type="text" name="buscaOperacoes" value="<?php if(isset($buscaOperacoes)) echo $buscaOperacoes?>" placeholder="Busca por Operacoes" />
											</div>
										</div>
										<div class="form-group col-sm-4">
											<label class="col-sm-3 control-label" for="dataInicio">Data de Início</label>
											<div class="col-sm-8">
												<input class="form-control" type="date" name="dataInicio" value="<?php if(isset($dataInicio)) echo $dataInicio?>" />
											</div>
										</div>
										<div class="form-group col-sm-4">
											<label class="col-sm-3 control-label" for="dataFim">Data Fim</label>
											<div class="col-sm-8">
												<input class="form-control" type="date" name="dataFim" value="<?php if(isset($dataFim)) echo $dataFim?>" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-6">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="realizadas" <?php if(isset($realizadas)) echo 'checked'?> />Operações Realizadas
												</label>
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox" name="futuras" <?php if(isset($futuras)) echo 'checked'?> />Operações Futuras
												</label>
											</div>
										</div>
										<div class="form-group col-sm-6">
											<button class="btn btn-default pull-right" value="submit">Buscar</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>


					<br />

					<table class="table table-condensed table-bordered">
						<?php
							echo "<th>Instância SoftExpert</th>";
							echo "<th>Operação</th>";
							echo "<th>Empresa</th>";
							echo "<th>Banco</th>";
							echo "<th>Data Contratação</th>";

							echo "<th>Moeda</th>";
							echo "<th>% de Garantia</th>";
							echo "<th>Dias de Garantia</th>";
							echo "<th>Garantias</th>";
							echo "<th>Parcela Calculada</th>";
							


							echo "<th>Data Alerta</th>";
							echo "<th>Data Limite da Parcela</th>";
							echo "<th>Data Vencimento</th>";


							echo '</tr>';
							$registro_NaoRepete = null;
							foreach ($resultadoBanco_Obrigacoes as $registro1)
							{
								$op = $registro1['NOPERAOXRT'];

								$dataVencimento = $registro1['DATA_VENC'];
								$dataAlerta = $registro1['DATA_ALERTA'];
								$dataLimite = $registro1['PARCELA_DATA_LIMITE'];



								$dataAlertaArray = explode('/', $dataAlerta);
								if (!isset($dataAlertaArray[2])) {
									goto escapaData; //die('Erro');
								}
								$dataAlerta = date('Y/m/d', strtotime($dataAlertaArray[2].'-'.$dataAlertaArray[1].'-'.$dataAlertaArray[0]));
								
								
								if($registro_NaoRepete == null || $registro_NaoRepete['NOPERAOXRT'] <> $registro1['NOPERAOXRT']){
									echo '<tr>';
									echo '<td>'.$registro1['IDPROCESS'].'</td>';
									echo '<td>'.$registro1['NOPERAOXRT'].'</td>';
									echo '<td>'.$registro1['EMPRESA'].'</td>';
									echo '<td>'.$registro1['BANCO'].'</td>';
									echo '<td>'.$registro1['DATACONTRATA'].'</td>';

									echo '<td>'.$registro1['MOEDA'].'</td>';
									echo '<td>'.$registro1['GARANTIA_PERCENTUAL'].'</td>';
									echo '<td>'.$registro1['GARANTIA_DIAS'].'</td>';
									echo '<td>';
									foreach ($operacoesGarantias as $operacao => $garantia)
									{
										if ($operacao == $registro1['NOPERAOXRT'])
										{
											echo '<button type="button" id="button-'.$operacao.'" onclick="document.getElementById(\'modal-bodyEmpresasGarantiasVisao\').innerHTML = \'';
											foreach ($garantia as $empresa)
											{
												echo $empresa.'<br>';
											}

											echo'\'" class="btn btn-primary" data-toggle="modal" data-target="#myModalEmpresasGarantias">
													Visualizar
													</button>';
										}

									}
									echo '</td>';
								}



								

								if($realizadas)
									$condicao = date('Y/m/d') <= date('Y/m/d', strtotime("-1 days",strtotime($dataAlerta)));
								else
									$condicao = date('Y/m/d') >= date('Y/m/d', strtotime("-1 days",strtotime($dataAlerta)));


								$class = 'primary';
								if ($condicao) {
									$class = 'danger';
								}


								echo '<td>'.number_format($registro1['PRI_JUR'], 2).'</td>';

								$dataAlerta = date_format(date_create_from_format('Y/m/d', $dataAlerta), 'd/m/Y');
								echo '<td>';
								echo "<h4><span class='label label-$class'>$dataAlerta</span><span class='label label-default' style='background-color: #FFF'>";
								if (!$registro1['JA_EXECUTADA']) //cria o botao para executar caso nao tenha sido executada
								{
									$dataVencimentoLink = implode('', explode('/', $dataVencimento));
									echo "<a href='#' id='$op-$dataVencimentoLink' data-toggle='modal' data-target='#myModal'>Executar</a>"; // data-toggle='modal' data-target='#myModal'
									
									 ?>
										<script>
											$("#<?php echo "$op-$dataVencimentoLink";?>").click(function(e) {
												$.ajax({
													type: "GET",
													url: "<?php echo CONTROLLERDIR?>/getEtapasGarantias",
													data: {"NOPERAOXRT": "<?php echo $op;?>",
															"DATA_VENC": "<?php echo $dataVencimentoLink;?>"},
													success: function (data) {
																				//console.log(data);
														var obj = JSON.parse(data)[0];
																				//console.log(obj);
														$("#obs1").val(obj.OBS1);
														$("#obs2").val(obj.OBS2);
														$("#obs3").val(obj.OBS3);
														$("#obs4").val(obj.OBS4);
														$("#obs5").val(obj.OBS5);

														if(obj.CMP1 == '1'){
														$("#cmp1").prop('checked', true);
														}else{
														$("#cmp1").prop('checked', false);
														}

														if(obj.CMP2 == '1'){
														$("#cmp2").prop('checked', true);
														}else{
														$("#cmp2").prop('checked', false);
														}

														if(obj.CMP3 == '1'){
														$("#cmp3").prop('checked', true);
														}else{
														$("#cmp3").prop('checked', false);
														}

														if(obj.CMP4 == '1'){
														$("#cmp4").prop('checked', true);
														}else{
														$("#cmp4").prop('checked', false);
														}

														if(obj.CMP5 == '1'){
														$("#cmp5").prop('checked', true);
														}else{
														$("#cmp5").prop('checked', false);
														}


														$("#NOPERAOXRT").val(obj.NOPERAOXRT);
														$("#DATAVENC").val(obj.DATAVENC);
														$("#parcelaAddGarantias").val(obj.DATAVENC);
														$("#operacaoAddGarantias").val(obj.NOPERAOXRT);
													}
												});
												$.ajax({
													type: "GET",
													url: "<?php echo CONTROLLERDIR?>/getContratosGarantias",
													data: {"operacao": "<?php echo $op;?>",
															"data_venc": "<?php echo $dataVencimentoLink;?>"},
													success: function (data) {
														console.log(data);
														$("#modalBodyAddGarantias").html(data);

													}
												});
											});
										</script>

									<?php
									//echo "<a class='' href='".CONTROLLERDIR."/executaGarantias?op=$op&venc=$dataVencimento' style='font-size: 9px'>Executar</a>"; //link de execução
								}
								echo '</td>';
								echo "<td><h4><span class='label label-success'>$dataLimite</span><span class='label label-default' style='background-color: #FFF'></td>";
								echo "<td><h4><span class='label label-success'>$dataVencimento</span><span class='label label-default' style='background-color: #FFF'></td>";
								echo '</tr>';
								
								escapaData:

							}

                                    ?>

					</table>
</div>
			</div>
		</div>
	</div>




	



	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		<div class="modal-dialog" role="document" style="width: 70%; left: 15%; top: 10%">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="modal-label">Garantias</h4>
				</div>


				<script language="JavaScript">
					function formEtapasConfirmacao(){
						if ($('#botaoExecutarBotaoExecutar').html() == 'Executar') {
							if (confirm('Você deseja realmente EXECUTAR esta garantia?')) {
								$('#formEtapas').submit();
							}
						} else {
							$('#formEtapas').submit();
						}
					}
				</script> 

				<script language="JavaScript">
					function formEtapasRemocao(link){
						//if (confirm('Você deseja realmente REMOVER este CONTRATO?')) {
								window.location=link; 
						//	}
					}
				</script> 




				<form class="" method="GET" id="formEtapas" action="<?php echo CONTROLLERDIR?>/saveEtapasGarantias">
					<div class="modal-body" id="modal-body">





















						<div class="row">
							<div class="form-group col-md-5">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="cmp1" onchange="checarMarcacoes();"  name="cmp1" />Busca de contratos de exportação no BI - Amaggi
									</label>
								</div>
							</div>
							<div class="form-group col-md-7">
								<input type="text" class="form-control" id="obs1" name="obs1" placeholder="Observações" />
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-5">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="cmp2" onchange="checarMarcacoes();"  name="cmp2" />Envio dos contratos para análise do banco
									</label>
								</div>
							</div>
							<div class="form-group col-md-7">
								<input type="text" class="form-control" id="obs2" name="obs2" placeholder="Observações" />
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-5">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="cmp3" onchange="checarMarcacoes();"  name="cmp3" />Recebimento do aceite do banco
									</label>
								</div>
							</div>
							<div class="form-group col-md-7">
								<input type="text" class="form-control" id="obs3" name="obs3" placeholder="Observações" />
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-5">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="cmp4" onchange="checarMarcacoes();"  name="cmp4" />Formalização dos <i>Notices of Assignments</i>
									</label>
								</div>
							</div>
							<div class="form-group col-md-7">
								<input type="text" class="form-control" id="obs4" name="obs4" placeholder="Observações" />
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-5">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="cmp5" onchange="checarMarcacoes();" name="cmp5" />Envio completo da formalização
									</label>
								</div>
							</div>
							<div class="form-group col-md-7">
								<input type="text" class="form-control" id="obs5" name="obs5" placeholder="Observações" />
								<input type="hidden" id="NOPERAOXRT" name="NOPERAOXRT" />
								<input type="hidden" id="DATAVENC" name="DATAVENC" />
							</div>
						</div>


						<div class="row">
							<div class="col-md-12" id="modalBodyAddGarantias">
								... AddContratos
							</div>
							<div class="col-md-12">
                                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModalAddGarantias"><i class="fa fa-plus" aria-hidden="true"></i></button> 
							</div>
						</div>


						<script>
							function checarMarcacoes() {
								if ($('#cmp1').is(":checked") && $('#cmp2').is(":checked") && $('#cmp3').is(":checked") && $('#cmp4').is(":checked") && $('#cmp5').is(":checked")) {
									$('#botaoExecutarBotaoExecutar').html('Executar');
									$('#botaoExecutarBotaoExecutar').addClass('btn-danger');
									$('#botaoExecutarBotaoExecutar').removeClass('btn-primary');
								}else{
									$('#botaoExecutarBotaoExecutar').html('Salvar');
									$('#botaoExecutarBotaoExecutar').addClass('btn-primary');
									$('#botaoExecutarBotaoExecutar').removeClass('btn-danger');
								}
							}


							function editContratoGarantias(contrato) {
								$.ajax({
									type: "GET",
									url: "<?php echo CONTROLLERDIR?>/getContratoGarantias",
									data: {"oid": contrato},
									success: function (data) {
										var obj = JSON.parse(data)[0];
										console.log(obj);
										//$('#operacaoAddGarantias').value(obj.OPERACAO);
										//$('#parcelaAddGarantias').value(obj.VENC);
										$('#importadorAddGarantias').val(obj.IMPORTADOR);
										$('#contratoAddGarantias').val(obj.CONTRATO);
										$('#produtoAddGarantias').val(obj.PRODUTO.toLowerCase());
										$('#periodoAddGarantias').val(obj.PERIODO);
										$('#quantAddGarantias').val(obj.QUANT);
										$('#cotacaoAddGarantias').val(obj.COTACAO);
									}
								});
							}

						</script>












					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" onclick="formEtapasConfirmacao();" class="btn btn-primary" id="botaoExecutarBotaoExecutar">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>



    <div class="modal fade" id="myModalAddGarantias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-label">Garantias</h4>
                </div>
                <form action="<?php echo CONTROLLERDIR ?>/addContratosGarantias" method="POST">
                    <div class="modal-body" id="modal-bodymyModalAddGarantias">
                        <div class="modal-body" id="modal-bodyEmpresasGarantias">


                            <div class="col-sm-12">
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-3 control-label" for="atividade">Importador</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="importadorAddGarantias" name="importador" value="" placeholder="Importador" />
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-3 control-label" for="atividade">Nº Contrato"</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="contratoAddGarantias" name="contrato" value="" onblur="getContratoGarantia(this)" placeholder="Nº Contrato" />
										<script>
											function getContratoGarantia(componente) {
												$.ajax({
													type: "GET",
													url: "<?php echo CONTROLLERDIR?>/getContratoGarantias",
													data: {"contrato": componente.value},
													success: function (data) {
														var obj = JSON.parse(data)[0];
														var operacao = obj.OPERACAO;
														var importador = obj.IMPORTADOR;
														console.log(obj);
														if(data.length > 2){
															alert('Este contrato já foi utilizado na operação '+operacao+ ' e importador '+importador);
														}
													}
												});
											}
										</script>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-3 control-label" for="atividade">Produto</label>
                                    <div class="col-sm-8">
                                        
											<select class="form-control" id="produtoAddGarantias" name="produto">
                                                <option value="SOJA">SOJA</option>
                                                <option value="MILHO">MILHO</option>
                                                <option value="FARELO">FARELO</option>
											</select>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-3 control-label" for="atividade">Quant.</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="quantAddGarantias" name="quant" value="" placeholder="Quant." />
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-3 control-label" for="atividade">Período</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="periodoAddGarantias" name="periodo" value="" placeholder="Período" />
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-3 control-label" for="atividade">Cotação</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="cotacaoAddGarantias" name="cotacao" value="" placeholder="Cotação" />
                                    </div>
                                </div>
                            </div>



                            <input type="hidden" id="operacaoAddGarantias" name="operacaoAddGarantias" value="" />
                            <input type="hidden" id="parcelaAddGarantias" name="parcelaAddGarantias" value="" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button class="btn btn-danger" value="submit">Salvar</button>
                        </div>
					</div>
                </form>
                </div>
            </div>
    </div>







    <div class="modal fade" id="myModalEmpresasGarantias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-label">Garantias</h4>
                </div>
                <div class="modal-body" id="modal-bodyEmpresasGarantiasVisao">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>







</body>
</html>
