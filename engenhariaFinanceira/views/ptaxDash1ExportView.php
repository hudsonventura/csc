

<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Data', 'Dolar Venda'],
		  <?php
		  	foreach ($banco as $registro) {
				
				if ($registro['data'] == date('Y-m-d')) {
					$data = 'Hoje';
				}else{
					$date = date_create($registro['data']);
					$data = date_format($date, 'd/m/Y');
				}
				
				$dolarCompra = $registro['dolarCompra'];
				$dolarVenda = $registro['dolarVenda'];

				$euroCompra = $registro['euroCompra'];
				$euroVenda = $registro['euroVenda'];

				$libraCompra = $registro['libraCompra'];
				$libraVenda = $registro['libraVenda'];

				echo "['$data', $dolarVenda],
				";
		  } ?>
        ]);

        var options =
        {
          title: 'Historico Dolar (PTAX)',
          vAxis: {title: 'Valor em R$'},
          hAxis: {direction: -1, slantedTextAngle: 90}
          
        };

       // var chart = new google.visualization.SteppedAreaChart(document.getElementById('chart_div'));
        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));

        chart.draw(data, options);
        //Console.Write("aab");
        document.write(chart.getImageURI());
        
        
        
        
      }
    </script>
  </head>
  <body id="body">
      <div id="chart_div" style="width: 1300px; height: 600px;"></div>
    
  </body>
</html>

