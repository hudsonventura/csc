<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />

    <link rel="shortcut icon" href="/modules\amaggi\views\assets\/favicon.png" type="image/x-icon" />
    <link href="https://suporte.amaggi.com.br/modules/amaggi/views/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<script src="https://suporte.amaggi.com.br/modules/amaggi/views/assets\jquery/js/jquery.min.js"></script>
	<script src="https://suporte.amaggi.com.br/modules/amaggi/views/assets\bootstrap/js/bootstrap.min.js"></script>

    <title><?php echo $title ?></title>

    <!--Imports-->
    <script src="<?php echo ASSETS?>ckeditor/ckeditor.js"></script>
    <script src="<?php echo ASSETS?>ckeditor/config.js"></script>
    <script src="<?php echo ASSETS?>ckeditor/plugins/image/dialogs/image.js"></script>

</head>

<body>
	<div class="">
		<div class="">
			<div class="panel text-center">
				<div class="panel-heading" style="background-color: #fff;">
					<h2><?php echo $title?></h2>
				</div>
				<div class="panel-body">

					<!--Busca-->
					<div class="col-md-12 form-horizontal">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Busca</h3>
							</div>
							<div class="panel-body ">
								<form method="GET" action="" id="form" name="form">
									<div class="row">
										<div class="form-group col-sm-4">
											<label class="col-sm-3 control-label" for="atividade">Operações</label>
											<div class="col-sm-9">
												<input class="form-control" type="text" name="buscaOperacoes" value="<?php if(isset($buscaOperacoes)) echo $buscaOperacoes?>" placeholder="Busca por Operações" />
											</div>
										</div>
                                        <div class="form-group col-sm-4">
                                            <label class="col-sm-3 control-label" for="dataInicio">Data de Início</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" name="dataInicio" value="<?php if(isset($dataInicio)) echo $dataInicio?>" />
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label class="col-sm-3 control-label" for="dataFim">Data Fim</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" name="dataFim" value="<?php if(isset($dataFim)) echo $dataFim?>" />
                                            </div>
                                        </div>
									</div>
									<div class="row">
										<div class="form-group col-sm-6">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="realizadas" <?php if(isset($realizadas)) echo 'checked'?> />Operações Realizadas
												</label>
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox" name="futuras" <?php if(isset($futuras)) echo 'checked'?> />Operações Futuras
												</label>
											</div>
										</div>
										<div class="form-group col-sm-6">
											<button class="btn btn-default pull-right" value="submit">Buscar</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>


					<br />

					<table class="table table-condensed table-bordered table-striped">
						<?php
							echo "<th>Operação</th>";
							echo "<th>Empresa</th>";
							echo "<th>Banco</th>";
							echo "<th>Data Contratação</th>";
							echo "<th>Vencimento Contrato</th>";
							echo "<th>Valor USD</th>";
							echo "<th>Anexo</th>";
							echo "<th>Compliance</th>";
							echo "<th>Tipo</th>";
							echo "<th>Data Alerta</th>";
							echo "<th>Vencimento Obrigação</th>";


							echo '</tr>';
							foreach ($resultadoBanco_Obrigacoes as $registro1)
							{
									if($registro1['AUDITORIA'] == 1)
										$auditoria = 'Auditada';
									else
										$auditoria = 'Não Auditada';

									$auditoriaLink = $registro1['AUDITORIA'];

									echo '<tr>';
									echo '<td>'.$registro1['NOPERAOXRT'].'</td>';
									echo '<td>'.$registro1['EMPRESA'].'</td>';
									echo '<td>'.$registro1['BANCO'].'</td>';
									echo '<td>'.$registro1['DATACONTRATA'].'</td>';
									echo '<td>'.$registro1['DATAVENCIMENTO'].'</td>';
									echo '<td>'.number_format($registro1['VALORCONTRATA'], 2).'</td>';


									echo '<td>'.$registro1['NANEXO'].'</td>';

									if ($registro1['COMPLIANCE'])
									{
										echo '<td>'.$registro1['COMPLIANCE'].'</td>';
									}else{
										echo '<td>'.$registro1['DEMONST'].'</td>';
									}


									if($registro1['AUDITORIA'] == 1)
										echo '<td>Auditada</td>';
									if($registro1['AUDITORIA'] == 0)
										echo '<td>Não Auditada</td>';
									if($registro1['AUDITORIA'] == 2)
										echo '<td>Outras Obrigações</td>';


									$op = $registro1['NOPERAOXRT'];
									$dataVencimento = $registro1['DATA_VENC'];

									$dataAlerta = $registro1['DATA_ALERTA'];
									$dataAlertaArray = explode('/', $dataAlerta);
									$dataAlerta = date('Y/m/d', strtotime($dataAlertaArray[2].'-'.$dataAlertaArray[1].'-'.$dataAlertaArray[0]));

									if($realizadas)
										$condicao = date('Y/m/d') <= date('Y/m/d', strtotime("-1 days",strtotime($dataAlerta)));
									else
										$condicao = date('Y/m/d') >= date('Y/m/d', strtotime("-1 days",strtotime($dataAlerta)));

									$class = 'primary';

									if($condicao && !$registro1['JA_EXECUTADA']){
										$class = 'danger';
									}

									$dataAlerta = date_format(date_create_from_format('Y/m/d', $dataAlerta), 'd/m/Y');
									echo '<td>';
									echo "<h4><span class='label label-$class'>$dataAlerta</span><span class='label label-default' style='background-color: #FFF'>";
									if (!$registro1['JA_EXECUTADA']) //cria o botao para executar caso nao tenha sido executada
									{
										echo "<a class='' href='".CONTROLLERDIR."/executaObrigacao?op=$op&auditoria=$auditoriaLink&venc=$dataVencimento' style='font-size: 9px'>Executar</a>"; //link de execução
									}
									echo '</td>';
									echo "<td><h4><span class='label label-success'>$dataVencimento</span><span class='label label-default' style='background-color: #FFF'></td>";
									echo '</tr>';
							}
						?>

					</table>
</div>
			</div>
		</div>
	</div>



	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="modal-label">Garantias</h4>
				</div>
				<div class="modal-body" id="modal-body">
					...
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>