<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class covenantsModel extends ModularCore\Model{

	function __construct() {
		parent::__construct();
		$this->loadDb('oraclePRD', 'oracle');

	}

	function getObrigacoes($buscaOperacoes = null, $realizadas = null, $futuras = null, $dataInicio = null, $dataFim = null){

		$buscaOperacoesSQL = '';
		if($buscaOperacoes){
			$buscaOperacoesSQL = " AND COV.NOPERAOXRT LIKE '%$buscaOperacoes%'";
		}

		$realizadasSQLJOIN = "LEFT OUTER JOIN";
		$realizadasSQLWHERE = "and ALERTAS.OPERACAO is null";
		$realizadasSQLWHEREOutrasObrigacoes = "obrigaexecutado = 0";
		if($realizadas){
			$realizadasSQLJOIN = "JOIN";
			$realizadasSQLWHERE = "and ALERTAS.OPERACAO is not null";
			$realizadasSQLWHEREOutrasObrigacoes = "obrigaexecutado = 1";
		}

		$futurasQLJOIN = "";
		$futurasSQLWHERE = "and TO_DATE(DATAS.DATA||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG-30 <= sysdate + 30";
		$futurasSQLWHEREOutrasObrigacoes = 'AND OBRIGA.OBRIGADATA-20 < SYSDATE';
		if($futuras){
			$futurasSQLJOIN = "";
			$futurasSQLWHERE = "and TO_DATE(DATAS.DATA||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG-30 >= sysdate";
			$futurasSQLWHEREOutrasObrigacoes = 'AND OBRIGA.OBRIGADATA-20 >= SYSDATE';
		}

		$dataInicioSQLWHERE = "";
		$dataInicioSQLWHEREOutrasObrigacoes = "";
		if($dataInicio){
			$dataInicioSQLWHERE = "and TO_DATE(DATAS.DATA||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG >= to_date('$dataInicio', 'DD/MM/YYYY')";
			$dataInicioSQLWHEREOutrasObrigacoes = "AND OBRIGA.OBRIGADATA >= to_date('$dataInicio', 'DD/MM/YYYY')";
		}

		$dataFimSQLWHERE = "";
		$dataFimSQLWHEREOutrasObrigacoes = "";
		if($dataFim){
			$dataFimSQLWHERE = "and TO_DATE(DATAS.DATA||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG <= to_date('$dataFim', 'DD/MM/YYYY')";
			$dataFimSQLWHEREOutrasObrigacoes = "AND OBRIGA.OBRIGADATA <= to_date('$dataFim', 'DD/MM/YYYY')";
		}



		$sql = "select * from(
								select * from (
												SELECT P.IDPROCESS, ANOGERADO, COV.NOPERAOXRT, COV.EMPRESA, COV.BANCO, COV.DATACONTRATA, COV.MOEDA, COV.VALORCONTRATA, COV.DATAVENCIMENTO, REMOVE_ACENTOS(DEMO.DEMONST) AS DEMONST,
												decode(DEMO.COMPLIANCE, 1, 'Sim', 0, 'Nao') as COMPLIANCE, DEMO.NANEXO,
												CASE WHEN DEMO.DEMONST = 'Auditadas' OR DEMO.DEMONST = 'Auditadas  ' THEN 1 ELSE 0 END AS AUDITORIA,
												TO_DATE('31/12'||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG AS OBRIGA_AUDITADA,
												TO_DATE(DATAS.DATA||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG-30 AS data_alerta,
												TO_DATE(DATAS.DATA||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG AS data_venc,
												CASE WHEN ALERTAS.OPERACAO IS NULL THEN NULL ELSE 1 END AS JA_EXECUTADA, DATAS.DATA as DATA_GERADA,
												DT03, DT06, DT09, DT12, '-' as OIDOBRIGAANEXO
												FROM DYNCOV COV
												LEFT JOIN DYNDEMFIN DEMO ON COV.OID = DEMO.OIDRELCOV01
												JOIN (SELECT TO_CHAR(TO_DATE('01/01/1999', 'DD/MM/YYYY')+(TESTE*365), 'YYYY') AS ANOGERADO FROM DUAL JOIN (SELECT ROWNUM AS TESTE FROM ALL_OBJECTS WHERE ROWNUM <= 200) ON 1=1) GERADOR ON GERADOR.ANOGERADO >= to_number(TO_CHAR(COV.DATACONTRATA, 'YYYY'), '0000') AND GERADOR.ANOGERADO < to_number(TO_CHAR(COV.DATAVENCIMENTO, 'YYYY'), '0000')
												JOIN GNASSOCFORMREG GNF on COV.oid = GNF.OIDENTITYREG
												JOIN wfprocess p ON P.cdassocreg = GNF.cdassoc AND P.FGSTATUS = 1
												LEFT JOIN (select column_value as data from table(sys.dbms_debug_vc2coll('31/03','30/06','30/09', '31/12'))) DATAS ON 1=1
												$realizadasSQLJOIN dyncovalertas ALERTAS   ON ALERTAS.OPERACAO = NOPERAOXRT and ALERTAS.TIPO = 0
																						AND CASE WHEN DEMO.DEMONST = 'Auditadas' OR DEMO.DEMONST = 'Auditadas  ' THEN 1 ELSE 0 END = ALERTAS.AUDITORIA
																						AND ALERTAS.venc = TO_DATE(DATAS.DATA||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG
												WHERE DEMO.DEMONST IS NOT NULL $realizadasSQLWHERE $buscaOperacoesSQL $futurasSQLWHERE $dataInicioSQLWHERE $dataFimSQLWHERE
												--ORDER BY TO_DATE(DATAS.DATA||ANOGERADO,'DD/MM/YYYY')+DEMO.PRAZODEENTREG, NOPERAOXRT, DEMONST
											)

								where ((DATA_GERADA = '31/03' AND DT03 = 1) OR (DATA_GERADA ='30/06' AND DT06 = 1) OR (DATA_GERADA ='30/09' AND DT09 = 1) OR (DATA_GERADA ='31/12' AND DT12 = 1))


								UNION


								select
								P.IDPROCESS, NULL AS ANOGERADO, COV.NOPERAOXRT, COV.EMPRESA, COV.BANCO, COV.DATACONTRATA, COV.MOEDA, COV.VALORCONTRATA, COV.DATAVENCIMENTO, REMOVE_ACENTOS(OBRIGA.OBRIGADESCRICAO) AS  DEMONST,
								null AS COMPLIANCE, NULL AS NANEXO,
								2 AS AUDITORIA,
								NULL AS  OBRIGA_AUDITADA,
								OBRIGA.OBRIGADATA-20 AS  data_alerta,
								OBRIGA.OBRIGADATA AS  data_venc,
								OBRIGA.obrigaexecutado AS JA_EXECUTADA,
								NULL AS DATA_GERADA,NULL AS DT03, NULL AS DT06, NULL AS DT09, NULL AS DT12,
								OBRIGA.OIDOBRIGAANEXO

								from wfprocess p
								JOIN GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc
								join DYNCOV COV on COV.oid = GNF.OIDENTITYREG
								join dyncovobrigacoes  OBRIGA on OBRIGA.OIDABCF1FTJC6ZCYMO = COV.oid

								where $realizadasSQLWHEREOutrasObrigacoes $futurasSQLWHEREOutrasObrigacoes $dataInicioSQLWHEREOutrasObrigacoes $dataFimSQLWHEREOutrasObrigacoes
							)

				ORDER BY data_alerta, NOPERAOXRT, DEMONST
				";

		$this->db->oracle->sqlBuilder->beginTransaction();
		$this->db->oracle->sqlBuilder->setQuery("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'");
		$this->db->oracle->sqlBuilder->executeQuery();




		$this->db->oracle->sqlBuilder->setQuery($sql);
		//echo  $this->db->oracle->sqlBuilder->getQuery(); die();
		$result = $this->db->oracle->sqlBuilder->executeQuery();

		$this->db->oracle->sqlBuilder->commit();

		return $result;
	}

	function executaObrigacao($insert){

		$this->db->oracle->sqlBuilder->beginTransaction();

		$this->db->oracle->sqlBuilder->setQuery("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'");
		$this->db->oracle->sqlBuilder->executeQuery();

		$insert['EXECDATA'] = date('d/m/Y');
		//$insert['EXECHORA'] = date('H:m:s'); --adicionar a hora posteriormente
		$this->db->oracle->sqlBuilder->insert($insert, 'dyncovalertas');
		//echo $this->db->oracle->sqlBuilder->getQuery(); die();
		$result = $this->db->oracle->sqlBuilder->executeQuery();

		$this->db->oracle->sqlBuilder->commit();


		return $result;
	}

	function executaOutrasObrigacoes($obrigacao){
		$NOPERAOXRT = $obrigacao['OPERACAO'];
		$OBRIGADATA = $obrigacao['VENC'];
		$this->db->oracle->sqlBuilder->select("OBRIGA.OID");
		$this->db->oracle->sqlBuilder->from("DYNCOV COV");
		$this->db->oracle->sqlBuilder->join("dyncovobrigacoes OBRIGA on OBRIGA.OIDABCF1FTJC6ZCYMO = COV.oid");
		$this->db->oracle->sqlBuilder->where("COV.NOPERAOXRT = '$NOPERAOXRT' and OBRIGA.OBRIGADATA = to_date('$OBRIGADATA', 'DD/MM/YYYY')");
		$oid = $this->db->oracle->sqlBuilder->executeQuery()[0]['OID'];

		$array = array();
		$array['OBRIGAEXECUTADO'] = 1;
		$this->db->oracle->sqlBuilder->update($array, "dyncovobrigacoes");
		$this->db->oracle->sqlBuilder->where("OID = '$oid'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();
		return $return;
	}



	function getGarantias($buscaOperacoes = null, $realizadas = null, $futuras = null, $dataInicio = null, $dataFim = null){


		$futurasSQLWHERE = "and (PARC.PAR_DATA-COV.PRAZOVENCEMDIAS)-30 <= sysdate + 30";
		if($futuras){
			$futurasSQLWHERE = "and PARC.PAR_DATA >= sysdate + 30";
		}


		$realizadasSQLJOIN = "LEFT OUTER JOIN";
		$realizadasSQLWHERE = "and ALERTAS.OPERACAO is null";
		if($realizadas){
			$realizadasSQLJOIN = "JOIN";
			$realizadasSQLWHERE = "and ALERTAS.OPERACAO is not null";
			$futurasSQLWHERE = '';
		}

		$operacoesSQLWHERE = '';
		if($buscaOperacoes){
			$operacoesSQLWHERE = "AND COV.NOPERAOXRT = $buscaOperacoes";
		}



		$dataInicioSQLWHERE = "";
		if($dataInicio){
			$dataInicioSQLWHERE = "and PARC.PAR_DATA >= to_date('$dataInicio', 'YYYY-MM-DD')";
		}

		$dataFimSQLWHERE = "";
		if($dataFim){
			$dataFimSQLWHERE = "and PARC.PAR_DATA <= to_date('$dataFim', 'YYYY-MM-DD')";
		}


		$sql = "SELECT PROC.IDPROCESS, COV.NOPERAOXRT, COV.EMPRESA, COV.BANCO, COV.DATACONTRATA, COV.MOEDA,
						PARC.VALOR_VALIDO AS VALOR_PARCELA_ORIGINAL,
						COV.PRAZOVENCEMDIAS AS GARANTIA_DIAS,
						PARC.PAR_DATA-COV.PRAZOVENCEMDIAS AS PARCELA_DATA_LIMITE,
						(PARC.VALOR_VALIDO*(COV.GARANTIAPORCE/100))+(PARC.VALOR_VALIDO) AS PARCELA_VALOR_CALCULADO,
						COV.GARANTIAPORCE AS GARANTIA_PERCENTUAL,
						PARC.PAR_TIPO AS PARCELA_TIPO,
						PARC.PAR_ENT_SAI AS E_S,
						PARC.PAR_DATA AS data_venc,
						(PARC.PAR_DATA-COV.PRAZOVENCEMDIAS)-30 AS data_alerta,
						(SELECT SUM(VALOR_VALIDO) FROM XRT.PARCELA@orli10g PARCIN WHERE PARCIN.OPR_NUMERO = PARC.OPR_NUMERO AND PARCIN.PAR_DATA = PARC.PAR_DATA)*(COV.GARANTIAPORCE/100) AS PRI_JUR,
						CASE WHEN ALERTAS.OPERACAO IS NULL THEN NULL ELSE 1 END AS JA_EXECUTADA

				FROM DYNCOV                         COV
				JOIN GNASSOCFORMREG                 GNF                       ON COV.OID        = GNF.OIDENTITYREG
				JOIN WFPROCESS                      PROC                      ON PROC.cdassocreg= GNF.cdassoc
				JOIN XRT.PARCELA@orli10g           PARC            ON COV.NOPERAOXRT = PARC.OPR_NUMERO
				$realizadasSQLJOIN dyncovalertas ALERTAS ON ALERTAS.OPERACAO = NOPERAOXRT AND ALERTAS.VENC = PARC.PAR_DATA and ALERTAS.TIPO = 1

				WHERE PROC.FGSTATUS=1 AND PARC.PAR_TIPO IN ('JUR','PRI')
				and PARC.PAR_DATA >= (SELECT MIN(PARCIN.PAR_DATA)
										FROM XRT.PARCELA@orli10g            PARCIN
										JOIN DYNCOV                         COVIN                       ON COVIN.NOPERAOXRT = PARCIN.OPR_NUMERO
										JOIN GNASSOCFORMREG                 GNFIN                       ON COVIN.OID        = GNFIN.OIDENTITYREG
										JOIN WFPROCESS                      PROCIN                      ON PROCIN.cdassocreg= GNFIN.cdassoc
										WHERE PROCIN.FGSTATUS=1 AND PARCIN.PAR_TIPO = 'PRI' and PARCIN.PAR_ENT_SAI = 'S' AND COVIN.NOPERAOXRT = COV.NOPERAOXRT and PARC.PAR_TIPO = 'PRI'
										$realizadasSQLWHERE $futurasSQLWHERE $dataInicioSQLWHERE $dataFimSQLWHERE $operacoesSQLWHERE
										group by COVIN.NOPERAOXRT
										)
				ORDER BY (PARC.PAR_DATA-COV.PRAZOVENCEMDIAS)-30, COV.NOPERAOXRT, PARC.PAR_TIPO DESC";


		$this->db->oracle->sqlBuilder->beginTransaction();
		$this->db->oracle->sqlBuilder->setQuery("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'");
		$this->db->oracle->sqlBuilder->executeQuery();



		$this->db->oracle->sqlBuilder->setQuery($sql);
		$result = $this->db->oracle->sqlBuilder->executeQuery();
		//echo  $this->db->oracle->sqlBuilder->getQuery(); die();
		$this->db->oracle->sqlBuilder->commit();
		return $result;
	}



	function getContratosGarantias($op, $data){
		$this->db->oracle->sqlBuilder->select("*");
		$this->db->oracle->sqlBuilder->from("dyncovgarantias");
		$this->db->oracle->sqlBuilder->where("OPERACAO = '$op' and PARCELA = '$data'");
		$result = $this->db->oracle->sqlBuilder->executeQuery();
		return $result;
	}

	function getContratoGarantias($contrato = '', $oid = ''){
		$this->db->oracle->sqlBuilder->select("*");
		$this->db->oracle->sqlBuilder->from("dyncovgarantias");
		$this->db->oracle->sqlBuilder->where("(contrato = '$contrato' or oid = '$oid') and FGENABLED = 1");
		$result = $this->db->oracle->sqlBuilder->executeQuery();
		return $result;
	}

	function removeContratosGarantias($oid){

		$this->db->oracle->sqlBuilder->select("*");
		$this->db->oracle->sqlBuilder->from("dyncovgarantias");
		$this->db->oracle->sqlBuilder->where("OID = '$oid'");
		$result = $this->db->oracle->sqlBuilder->executeQuery();

		$fgenable = $result[0]['FGENABLED'];

		if ($fgenable === '1') { //desabilitar
			$array = array('FGENABLED' => 0);
			$this->db->oracle->sqlBuilder->update($array, "dyncovgarantias");
			$this->db->oracle->sqlBuilder->where("OID = '$oid'");
		}else{
			$this->db->oracle->sqlBuilder->delete();
			$this->db->oracle->sqlBuilder->from("dyncovgarantias");
			$this->db->oracle->sqlBuilder->where("OID = '$oid'");
		}

		$result = $this->db->oracle->sqlBuilder->executeQuery();

		

		
		return $result;
	}

	function addContratosGarantias($insert){
		$contrato = $insert['CONTRATO'];
		$parcela = $insert['PARCELA'];



		$this->db->oracle->sqlBuilder->select("*");
		$this->db->oracle->sqlBuilder->from("dyncovgarantias");
		$this->db->oracle->sqlBuilder->where("FGENABLED = 1 and contrato = '$contrato' and parcela = '$parcela'");

		$retornoBanco = $this->db->oracle->sqlBuilder->executeQuery();

		//var_dump($retornoBanco); die();
		if (count($retornoBanco) <= 0)
		{
			$insert['FGENABLED'] = 1;
			$this->db->oracle->sqlBuilder->insert($insert, "dyncovgarantias");
			$result = $this->db->oracle->sqlBuilder->executeQuery();
			return $result;
		}else{
			unset($insert['OID']);
			$insert['FGENABLED'] = 1;
			$this->db->oracle->sqlBuilder->update($insert, "dyncovgarantias");
			$this->db->oracle->sqlBuilder->where("CONTRATO = '$contrato' and PARCELA = '$parcela'");
			//die($this->db->oracle->sqlBuilder->getQuery());
			$result = $this->db->oracle->sqlBuilder->executeQuery();
			return $result;
		}



	}



	function getEtapasGarantias($op, $data){
		$this->db->oracle->sqlBuilder->select("*");
		$this->db->oracle->sqlBuilder->from("dyncovetapas");
		$this->db->oracle->sqlBuilder->where("NOPERAOXRT = '$op' and DATAVENC = '$data'");
		//echo $this->db->oracle->sqlBuilder->getQuery(); die();
		$result = $this->db->oracle->sqlBuilder->executeQuery();
		
		return $result;
	}

	function insertEtapasGarantias($insert){

		$this->db->oracle->sqlBuilder->beginTransaction();

		$this->db->oracle->sqlBuilder->setQuery("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'");
		$this->db->oracle->sqlBuilder->executeQuery();

		$insert['EXECDATA'] = date('d/m/Y');
		//$insert['EXECHORA'] = date('H:m:s'); --adicionar a hora posteriormente
		//var_dump($insert);
		$this->db->oracle->sqlBuilder->insert($insert, 'dyncovetapas');
		//var_dump($this->db->oracle->sqlBuilder->getQuery()); die();
		$result = $this->db->oracle->sqlBuilder->executeQuery();

		$this->db->oracle->sqlBuilder->commit();


		return $result;
	}

	function updateEtapasGarantias($insert, $registroAtual){
		
		//preparando dados para where do update
		$dataVenc = $registroAtual['DATAVENC'];
		$operacao = $registroAtual['NOPERAOXRT'];
		
		
		
		//preparando dados a serem inseridos
		$insert['EXECDATA'] = date('d/m/Y');
		//$insert['EXECHORA'] = date('H:m:s'); --adicionar a hora posteriormente

		$this->db->oracle->sqlBuilder->beginTransaction();
		$this->db->oracle->sqlBuilder->setQuery("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'");
		$this->db->oracle->sqlBuilder->executeQuery();

		
		$this->db->oracle->sqlBuilder->update($insert, 'dyncovetapas');
		$this->db->oracle->sqlBuilder->where("NOPERAOXRT = '$operacao' and DATAVENC = '$dataVenc'");
		//var_dump($this->db->oracle->sqlBuilder->getQuery()); die();
		$result = $this->db->oracle->sqlBuilder->executeQuery();

		$this->db->oracle->sqlBuilder->commit();


		return $result;
	}

}