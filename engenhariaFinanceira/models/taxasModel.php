<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class TaxasModel extends ModularCore\Model{

	function __construct() {
		parent::__construct();
		ModularCore\core::$coreConfig['databases']['cscPRD']['dbname'] = 'engenhariafinanceira';
		$this->loadDb('cscPRD', 'ptax');
		
		//$this->urlSoap = Modularcore\core::$coreConfig['webservice_soap'];
	}

	function getTaxasData($data = null){
		if(!$data){
			$data = date('Y-m-d');
		}

		$this->db->ptax->sqlBuilder->select('*');
		$this->db->ptax->sqlBuilder->from('ptax A');
		$this->db->ptax->sqlBuilder->where("DATA = to_date('$data', 'DD/MM/YYYY')");
		$retorno = $this->db->ptax->sqlBuilder->executeQuery();

		if(count($retorno) == 1){
			return $retorno[0];
		}else{
			return false;
		}
	}

	function getTaxas($where = null){
		$this->db->ptax->sqlBuilder->select('*');
		$this->db->ptax->sqlBuilder->from('ptax A');
		if ($where) {
			$this->db->ptax->sqlBuilder->where($where);
			
		}
		$this->db->ptax->sqlBuilder->orderby('DATA DESC');
		
		return $this->db->ptax->sqlBuilder->executeQuery();
	}

	function getTaxaUltima(){
		$this->db->ptax->sqlBuilder->select('*');
		$this->db->ptax->sqlBuilder->from('ptax A');
		$this->db->ptax->sqlBuilder->orderby('DATA DESC');
		$this->db->ptax->sqlBuilder->limit('1');
		return $this->db->ptax->sqlBuilder->executeQuery()[0];
	}
}