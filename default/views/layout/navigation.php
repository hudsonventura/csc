	<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; margin-top: -40px;">

    <div class="navbar-header">

	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	    <span class="sr-only">Toggle navigation</span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	</button>
	<a class="navbar-brand"><img src="<?php echo ASSETS?>logo_csc.png" style="margin-left: 30px; margin-top: -10px;;" height="220%" /></a>
    </div>

    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
	
	
	<li class="dropdown">
	    
	</li>
	
	
	
	<li class="dropdown">
		<a id="itemmenu" class="dropdown-toggle itemmenu" data-toggle="dropdown" href="#"><?php echo $usuario['displayname'][0] ?><i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i></a>
		<ul class="dropdown-menu dropdown-user">
			<li ><a href="<?php echo MODULEDIR ?>/usuarios/desbloquearUsuario?samaccountname=<?php echo $usuario['samaccountname'][0] ?>&page=<?php echo $GLOBALS['CoreVars'][1]?>"><i class="fa fa-user fa-fw"></i> Desbloquear Usuário</a></li>
			<li class="divider"></li>
			<li><a href="<?php echo MODULEDIR ?>/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
		</ul>
	    <!-- /.dropdown-user -->
	</li>

	
	<!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <?php require ('menu.php'); ?>
</nav>