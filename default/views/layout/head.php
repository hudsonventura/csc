<!DOCTYPE html >
<html lang="en">

<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />
	
	<link rel="shortcut icon" href="<?php echo ASSETS?>/favicon.png" type="image/x-icon" />
	
	
    <title><?php echo $title?></title>
    
	<!--Imports-->
		<!-- Bootstrap Core CSS --><link href="<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<!-- Bootstrap Core JS --><script src="<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/js/bootstrap.min.js"></script>
		<!-- jQuery --><script src="<?php echo DEFAULTVIEWDIR;?>assets/jquery/js/jquery.min.js"></script>
		<!-- Bootstrap Select --><link href="<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/css/bootstrap-select.min.css" rel="stylesheet" />
		<!-- Bootstrap Select --><script src="<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/js/bootstrap-select.min.js"></script>
		
		
		
		<!-- MetisMenu CSS --><link href="<?php echo DEFAULTVIEWDIR;?>assets/metisMenu/css/metisMenu.min.css" rel="stylesheet" />
		<!-- MetisMenu JS  --><script src="<?php echo DEFAULTVIEWDIR;?>assets/metisMenu/js/metisMenu.min.js"></script>
		
		<!-- SB Admin 2 CSS --><link href="<?php echo DEFAULTVIEWDIR;?>assets/sb-admin-2/css/sb-admin-2.css" rel="stylesheet" />
		<!-- SB Admin 2 JS  --><script src="<?php echo DEFAULTVIEWDIR;?>assets/sb-admin-2/js/sb-admin-2.js"></script>

		<!--Custom CSS Login--><link rel="stylesheet" href="<?php echo DEFAULTVIEWDIR;?>assets/sb-admin-2/css/style.css" />

		<!-- Custom Fonts --><link href="<?php echo DEFAULTVIEWDIR;?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

		<!-- pignoseCalender.js  --><link rel="shortcut icon" type="image/x-icon" href="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/demo/img/pignose-ico.ico" />
		<!-- pignoseCalender.js  --><link rel="stylesheet" type="text/css" href="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/demo/css/semantic.ui.min.css">
		<!-- pignoseCalender.js  --><link rel="stylesheet" type="text/css" href="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/demo/css/prism.css" />
		<!-- pignoseCalender.js  --><link rel="stylesheet" type="text/css" href="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/demo/css/calendar-style.css" />
		<!-- pignoseCalender.js  --><link rel="stylesheet" type="text/css" href="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/demo/css/style.css" />
		<!-- pignoseCalender.js  --><link rel="stylesheet" type="text/css" href="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/dist/css/pignose.calendar.css" />


		<!-- pignoseCalender.js  --><script type="text/javascript" src="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/demo/js/semantic.ui.min.js"></script>
		<!-- pignoseCalender.js  --><script type="text/javascript" src="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/demo/js/prism.min.js"></script>
		<!-- pignoseCalender.js  --><script type="text/javascript" src="<?php echo DEFAULTVIEWDIR;?>assets/pg-calendar-master/dist/js/pignose.calendar.js"></script>
    
		<!--Fancy Checkbox-->
		<link rel="stylesheet" href="<?php echo DEFAULTVIEWDIR;?>assets/fancy-checkbox/css/fancy-bootstrap-checkbox.css" />
    
		<!--Bootstrap Switch-->
		<link rel="stylesheet" href="<?php echo DEFAULTVIEWDIR;?>assets/switch/css/bootstrap-switch.min.css" />
		<script src="<?php echo DEFAULTVIEWDIR;?>assets/switch/js/bootstrap-switch.min.js"></script>

		<!--Tooltip Editable-->
		<link href="<?php echo DEFAULTVIEWDIR;?>assets/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">
		<script src="<?php echo DEFAULTVIEWDIR;?>assets/bootstrap-editable/js/bootstrap-editable.js"></script>

		<!-- notify  -->
		<script src="<?php echo DEFAULTVIEWDIR;?>assets/<?php echo BASEDIR.'/modules/default/views/assets/';?>notify/bootstrap-notify.min.js"></script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!--Imports-->
    
    
    <script type="text/javascript" src="<?php echo DEFAULTVIEWDIR;?>assets/noty/packaged/jquery.noty.packaged.min.js"></script>

</head>

