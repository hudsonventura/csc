<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
	<ul class="nav" id="side-menu">


		<!--ACESSO GERAL-->
		<li>
			<a href="<?php echo MODULEDIR?>/home"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</a>
		</li>
		
		<li>
			<!--a href="<?php echo MODULEDIR?>/reservasalas"><i class="glyphicon glyphicon-calendar"></i> Reserva de Salas de Reunião</-->
		</li>
		<!--ACESSO GERAL-->
		
		
		<!--ACESSO TI-->
		<?php if(isset($usuario['memberof'])){?>
			<?php foreach($usuario['memberof'] as $item) {
					  if(strpos($item, 'GU_TI_SCSM_Perm_SuporteInfra') || strpos($item, 'GG_TI_Operadores_de_Conta') || strpos($item, 'csc.processo') || strpos($item, 'GU_CRP_ACL_GestaoTI') || strpos($item, 'GG_TI_AccountOperators')){ ?>
						<li><a class="text-center"><b style="color: #BBB;"> - Tecnologia da Informação - </b></a></li>
							<li>
								<a href="https://suporte.amaggi.com.br/amaggi/usuarios"><i class="fa fa-users fa-fw"></i> Manutenção de Usuários</a>
							</li>
							
							<!--LINKS-->
							<li>
								<a href="#"><i class="fa fa-exchange fa-fw"></i> Links<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level">
									<li>
										<a href="https://suporte.amaggi.com.br/amaggi/links/monitoramento"> Monitoramento</a>
									</li>
									<?php foreach($usuario['memberof'] as $item) {
											  if(strpos($item, 'GU_CRP_ACL_Infra') || strpos($item, 'csc.processo')){ //GU_CRP_ACL_Infra ?>
										<li>
											<a href="https://suporte.amaggi.com.br/amaggi/links/gerenciar"> Gerenciar</a>
										</li>
										<?php } ?>
									<?php } ?>
									<li>
										<a href="https://suporte.amaggi.com.br/amaggi/links/linksLogs"> Logs de Links</a>
									</li>
								</ul>
							</li>
							
							<!--SERVICOS-->
							<li>
								<a href="#"><i class="fa fa-cogs fa-fw"></i> Serviços<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level">
									<li>
										<a href="<?php echo MODULEDIR?>/servicos/index"> Monitoramento</a>
									</li>
									<?php foreach($usuario['memberof'] as $item) {
											  if(strpos($item, 'GU_CRP_ACL_Infra') || strpos($item, 'csc.processo')){ //GU_CRP_ACL_Infra ?>
										<li>
											<a href="https://suporte.amaggi.com.br/amaggi/servicos/gerenciar"> Gerenciar</a>
										</li>
										<?php } ?>
									<?php } ?>
									<li>
										<a href="https://suporte.amaggi.com.br/amaggi/servicos/servicosLogs"> Logs de Serviços</a>
									</li>
								</ul>
							</li>
				<?php goto out; ?>
				<?php } ?>
			<?php } out:?>
		<?php } ?>
		<!--ACESSO TI-->
		
		<!--ACESSO CSC-->
			<?php if(isset($usuario['memberof'])){?>
				<?php foreach($usuario['memberof'] as $item) {
					if(strpos($item, 'csc.processo')){ ?>
						<li><a class="text-center"><b style="color: #BBB;"> - Gestão de Processos - </b></a></li>
						<li>
							<a href="#"><i class="fa fa-exchange fa-fw"></i> Biblioteca de Conhecimento<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="<?php echo BASEDIR?>biblioteca/inicio"> Biblioteca</a>
								</li>
								<li>
									<a href="<?php echo BASEDIR?>biblioteca/gerenciar"> Gerenciar</a>
								</li>
							</ul>
						</li>
					<?php } ?>
				<?php } ?>
			<?php } ?>
			<?php if(isset($usuario['memberof'])){?>
				<?php foreach($usuario['memberof'] as $item) {
						  if(strpos($item, 'contabilidade')|| strpos($item, 'csc.processo')){ ?>
						<li><a class="text-center"><b style="color: #BBB;"> - Contabilidade - </b></a></li>
						<li>
							<a href="#"><i class="fa fa-bar-chart"></i> Fechamento Contabil<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="<?php echo BASEDIR?>fecontabil/cronograma"> Cronograma</a>
								</li>
								<li>
									<a href="<?php echo BASEDIR?>fecontabil/cronograma/iniciar"> Iniciar Fechamento</a>
								</li>
							</ul>
						</li>
					<?php } ?>
				<?php } ?>
			<?php } ?>
					
		<!--ACESSO CSC-->
		
		<!--ACESSO CONTROLES INTERNOS-->
			<?php if(isset($usuario['memberof'])){?>
				<?php foreach($usuario['memberof'] as $item) {
					if(strpos($item, 'csc.processo') || strpos($item, 'controle.interno')){ ?>
						<li><a class="text-center"><b style="color: #BBB;"> - Controles Internos - </b></a></li>
						<li>
							<a href="#"><i class="fa fa-compress"></i> Controles Internos<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="<?php echo BASEDIR?>controlesinternos/kri"> Relatório</a>
								</li>
								<li>
									<a href="<?php echo BASEDIR?>controlesinternos/kri/listagem"> Listagem dos Controles</a>
								</li>
							</ul>
						</li>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		<!--ACESSO CONTROLES INTERNOS-->



		<!--ACESSO GESTORES-->
		<?php if(isset($usuario['HCM']) && $usuario['HCM'][0]){?>
	
		<?php if(	strpos(strtolower($usuario['HCM'][0]['CARGO']), 'supervisor') > -1 ||
					strpos(strtolower($usuario['HCM'][0]['CARGO']), 'coordenador') > -1 ||
					strpos(strtolower($usuario['HCM'][0]['CARGO']), 'gerente') > -1 ||
					strpos(strtolower($usuario['HCM'][0]['CARGO']), 'lider') > -1 ||
					strpos(strtolower($usuario['HCM'][0]['CARGO']), 'diretor') > -1 ||
					strpos(strtolower($usuario['HCM'][0]['CARGO']), 'presidente') > -1 
					){ ?>  
					<li>
						<a class="text-center">
							<b style="color: #BBB;">- GESTORES - </b>
						</a>
					</li>

					<li>
						<a href="<?php echo MODULEDIR?>/usuarios/acessoGestor"><i class="fa fa-unlock"></i> Trocar Senha de Colaborador</a>
					</li>

					</li>
			<?php } ?>
		<?php } ?>
		<!--ACESSO GESTORES-->



		<!--ACESSO Planejamento Financeiro-->
		<?php if(isset($usuario['memberof'])){?>
		<?php foreach($usuario['memberof'] as $item) {
				  if(strpos($item, 'csc.processo') || strpos($item, 'planejamento.financeiro')){ ?>
		<li>
			<a class="text-center">
				<b style="color: #BBB;">- Engenharia Financeira - </b>
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-compress"></i>Convenants
				<span class="fa arrow"></span>
			</a>
			<ul class="nav nav-second-level">
				<li>
					<a href="<?php echo BASEDIR?>covenants/relatorio">Relatório</a>
				</li>
			</ul>
		</li>
		<?php } ?>
		<?php } ?>
		<?php } ?>
        <!--ACESSO Planejamento Financeiro-->

        <!-- SEGURANCA CORPORATIVA-->
		<?php if(isset($usuario['memberof'])){?>
		<?php foreach($usuario['memberof'] as $item) {
				  if(strpos($item, 'csc.processo') || strpos($item, 'corporate.security') || strpos($item, 'GU_TI_SCSM_Perm_SuporteInfra')){// || strpos($item, 'GU_CRP_ACL_Infra')){
						  echo '<li>
									<a href="'.MODULEDIR.'/logs"><i class="fa fa-file-text-o"></i> Logs</a>
								</li>';

					 } ?>
			<?php } ?>
		<?php } ?>
		<!-- SEGURANCA CORPORATIVA-->


	</ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->