<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Forbidden - Error 403</title>
        <meta name="robots" content="noindex">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans" />
    </head>
    <body style="background-color: #f2f2f2; font-family: 'Open Sans', Calibri;">
        <div class="container">
            <div class="row">
			<h1 style="color:#DDD"><c style="color:#0066cc">Modular</c>Core</h1>
                <div class="col-md-10 col-md-offset-2">
                    <h1 style="margin-top: 1em;font-size: 7em;font-weight: 500;">Error 403</h1>
                    <h2>Read access forbidden</h2>
                    <p class="lead">Sorry, an error has occured. The resource you requested cannot be accessed!</p>
					<a href="#" class="btn btn-default btn-md"  onclick="history.go(-1);"><span class="glyphicon glyphicon-hand-left"></span> Go Back </a>
                    <a href="<?PHP //ECHO BASEDIR;?>" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-home"></span> Home </a>
					
                </div>
            </div>
        </div>
    </body>
</html>