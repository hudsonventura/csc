<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class usuarioCSC extends ModularCore\Model{

	function __construct() {
		parent::__construct();
		$this->loadDb('oraclePRD', 'oracle');

		$this->soapUrl = "https://se.amaggi.com.br:6244/softexpert/webserviceproxy/se/ws/adm_ws.php";
		$this->soapUser = "SE Suite\\sistema.automatico";
		$this->soapPass = "softexpert123";
		$this->soapType = "basic";
	}

	public function getUserSE($ip = null, $samaccountmane = null){
		
		//busca usuario pela sessão quando nao for passado o username
		if (!$samaccountmane) {
			if(!$ip) $ip = $_SERVER['REMOTE_ADDR'];
			$this->db->oracle->sqlBuilder->select('U.IDLOGIN, U.IDUSER, U.CDUSER');
			$this->db->oracle->sqlBuilder->FROM('SEUSERSESSION S');
			$this->db->oracle->sqlBuilder->JOIN('ADUSER U ON U.IDLOGIN = S.IDLOGIN');
			$this->db->oracle->sqlBuilder->WHERE("FGSESSIONTYPE = 2 AND
													to_char(DTDATE, 'DD/MM/YY') = to_char(sysdate, 'DD/MM/YY') AND
													nmloginaddress = '$ip'");
			$this->db->oracle->sqlBuilder->orderby('DTDATE desc');
			//echo $this->db->oracle->sqlBuilder->getQuery(); die();
			$return = $this->db->oracle->sqlBuilder->executeQuery();
			//echo $this->db->oracle->sqlBuilder->getQuery(); die();
			if(isset($return[0]))
				return $return[0];
			else
				return false;
		}else{
			$this->db->oracle->sqlBuilder->select('U.IDLOGIN, U.IDUSER, U.CDUSER');
			$this->db->oracle->sqlBuilder->FROM('ADUSER U');
			$this->db->oracle->sqlBuilder->WHERE("IDUSER = '$samaccountmane'");
			//echo $this->db->oracle->sqlBuilder->getQuery(); die();
			$return = $this->db->oracle->sqlBuilder->executeQuery();
			//echo $this->db->oracle->sqlBuilder->getQuery(); die();
			if(isset($return[0]))
				return $return[0];
			else
				return false;
		}
		
		
	}


	public function getUserAD($ip){
		
		if (strpos($ip, '12.12.') == 4 || strpos($ip, '16.52.89') == 4) {
			$samaccountmane = 'suporte.unlockuser';
			$userSE = $this->getUserSE(NULL, $samaccountmane);
		}else{
			$userSE = $this->getUserSE($ip);
			$samaccountmane = $userSE['IDLOGIN'];
		}
		
		//var_dump($userSE);
			
		
		if(!$samaccountmane){
			return false;
		}
		$this->loadLib('activedirectory');
		$userAD = $this->libs->activedirectory->getUser('samaccountname='.$samaccountmane);

		if(!$userAD){
			return false;
		}

		array_push($userSE, $userAD);
		return $userSE;
	}

	public function getUserHCM($user = null, $cpf = null){
		$this->loadDb('oracle', 'oracle');

		if (is_array($user)) {
			$cpf = $user['description'][0];
		}
		
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('sap_xi.VRM_SOFTEXPERT@SEPRD');
		$this->db->oracle->sqlBuilder->where("cpf = '$cpf'");

		$user['HCM'] = $this->db->oracle->sqlBuilder->executeQuery();

		return $user;
	}

	public function getAreaSE($cdDepartment = null, $nmDepartment = null){
		$this->loadDb('oracle', 'oracle');
		
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('ADDEPARTMENT ');
		$this->db->oracle->sqlBuilder->where("cddepartment = '$cdDepartment' or nmdepartment = '$nmDepartment'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();

		if ($return) {
			return $return[0]['NMDEPARTMENT'];
		}
		return false;
	}

	public function criarAreaSE($area, $funcaoPadrao) {
		$client = @new nusoap_client($this->soapUrl);//*****ALTERAR DOMINIO****

		$paramWebService = array();
		$paramWebService["ID"] = $area;
		$paramWebService["DESC"] = $area;
		$paramWebService["IDPOS"] = $funcaoPadrao;

		$client->setCredentials($this->soapUser, $this->soapPass, $this->soapType);

		$result = $client->call("newDepartment", $paramWebService);
		$error = $client->getError();
		if($result <> -1){
			return $result;
		}else{
			return false;
		}

		return false;
	}


	public function getFuncaoSE($cdPosition = null, $nmPosition  = null){
		$this->loadDb('oracle', 'oracle');
		
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('adposition ');
		$this->db->oracle->sqlBuilder->where("cdposition = '$cdPosition' or nmposition = '$nmPosition'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();

		if ($return) {
			return $return[0]['IDPOSITION'];
		}
		return false;
	}

	public function criarFuncaoSE($funcao) {
		$this->loadLib('nusoap');

		$client = @new nusoap_client($this->soapUrl);//*****ALTERAR DOMINIO****

		$paramWebService = array();
		$paramWebService["ID"] = $funcao;
		$paramWebService["DESC"] = $funcao;

		$client->setCredentials($this->soapUser, $this->soapPass, $this->soapType);

		$result = $client->call("newPosition", $paramWebService);
		$error = $client->getError();
		if($result <> -1){
			return $result;
		}else{
			return false;
		}
		return false;
	}

	public function getAssocFuncAreaUsuarioSE($iduser){
		$this->loadDb('oracle', 'oracle');
		
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('aduser  u');
		$this->db->oracle->sqlBuilder->join('ADUSERDEPTPOS d ON d.cduser = u.cduser');
		$this->db->oracle->sqlBuilder->join('adposition p ON p.cdposition = d.cdposition');
		$this->db->oracle->sqlBuilder->join('addepartment dp ON dp.cddepartment = d.cddepartment');
		$this->db->oracle->sqlBuilder->where("d.fgdefaultdeptpos = 1 and iduser = '$iduser'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();


		if ($return) {
			return $return[0]['IDPOSITION'];
		}
		return false;
	}

	public function getAssocAreaFuncao($area, $funcao){
		$this->loadDb('oracle', 'oracle');
		
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('ADDEPTPOSITION assoc');
		$this->db->oracle->sqlBuilder->join('ADDEPARTMENT area on area.CDDEPARTMENT = assoc.CDDEPARTMENT');
		$this->db->oracle->sqlBuilder->join('ADPOSITION funcao on funcao.CDPOSITION = assoc.CDPOSITION');
		$this->db->oracle->sqlBuilder->where("NMDEPARTMENT = '$area' and NMPOSITION = '$funcao'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();


		if ($return) {
			return true;
		}
		return false;
	}

	public function criarAssocFuncAreaUsuarioseSE($iduser, $area, $funcao) {
		$this->loadLib('nusoap');

		$client = @new nusoap_client($this->soapUrl);//*****ALTERAR DOMINIO****

		$paramWebService = array();
		$paramWebService["ID"] = $iduser;
		$paramWebService["IDAREA"] = $area;
		$paramWebService["IDFUNC"] = $funcao;

		$client->setCredentials($this->soapUser, $this->soapPass, $this->soapType);

		$result = $client->call("addUserDepartment", $paramWebService);
		$error = $client->getError();
		if($result <> -1){
			return $result;
		}
		return false;
	}

	public function getAssocGrupoAcessoUsuarioseSE($iduser){
		$this->loadDb('oracle', 'oracle');
		
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('aduser  u');
		$this->db->oracle->sqlBuilder->join('ADUSERACCGROUP assoc on assoc.CDUSER = u.CDUSER');
		$this->db->oracle->sqlBuilder->join('ADACCESSGROUP grupo on grupo.CDGROUP = assoc.CDGROUP');
		$this->db->oracle->sqlBuilder->where("iduser = '$iduser'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();

		if ($return) {
			return $return[0]['IDGROUP'];
		}
		return false;
	}

	public function criarAssocGrupoAcessoUsuarioseSE($iduser, $grupoAcesso = '1') {
		$this->loadDb('oracle', 'oracle');
		
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('aduser  u');
		$this->db->oracle->sqlBuilder->where("iduser = '$iduser'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();

		if (!$return) {
			return false;
		}
		$grupoAcessoAdd = array('CDUSER'=> $return[0]['CDUSER'], 'CDGROUP'=> 3);

		$this->db->oracle->sqlBuilder->insert($grupoAcessoAdd,'ADUSERACCGROUP');

		$return = $this->db->oracle->sqlBuilder->executeQuery();
		return $return;
	}

	public function criarAssocAreaFuncaoSE($area, $funcao) {
		$this->loadDb('oracle', 'oracle');
		

		//obtem cd da area
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('ADDEPARTMENT area');
		$this->db->oracle->sqlBuilder->where("NMDEPARTMENT = '$area'");
		$returnArea = $this->db->oracle->sqlBuilder->executeQuery();

		if (!$returnArea) {
			return false;
		}
		$cdArea = $returnArea[0]['CDDEPARTMENT'];

		//obtem cd da funcao
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('ADPOSITION area');
		$this->db->oracle->sqlBuilder->where("NMPOSITION = '$funcao'");
		$returnFuncao = $this->db->oracle->sqlBuilder->executeQuery();

		if (!$returnFuncao) {
			return false;
		}
		$cdFuncao = $returnFuncao[0]['CDPOSITION'];

		$assoc = array('CDDEPARTMENT'=>$cdArea,'CDPOSITION'=>$cdFuncao,'DTINSERT'=>date('d/m/Y'),'NMUSERUPD'=>'API de Importação de Usuarios');

		$this->db->oracle->sqlBuilder->insert($assoc,'ADDEPTPOSITION');

		$return = $this->db->oracle->sqlBuilder->executeQuery();
		return $return;
	}

	

	public function getSubordinadosHCM($user){
		$this->loadDb('oraclePRD', 'oracle');

		$cpf = $user['description'][0];
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('sap_xi.VRM_SOFTEXPERT@SEPRD');
		$this->db->oracle->sqlBuilder->where("cpf_superior = '$cpf'");

		return $this->db->oracle->sqlBuilder->executeQuery();
	}



}