<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class comentarioModel extends ModularCore\Model{

	function __construct() {
		parent::__construct();
		$this->loadDb('oraclePRD', 'oracle');
	}

	function getEntidadeOid($tabela, $instancia){
		$this->db->oracle->sqlBuilder->select("oid");
		$this->db->oracle->sqlBuilder->from("wfprocess p");
		$this->db->oracle->sqlBuilder->join("GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc");
		$this->db->oracle->sqlBuilder->leftjoin("DYN$tabela FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG");
		$this->db->oracle->sqlBuilder->where("P.idprocess = '$instancia'");
		return $this->db->oracle->sqlBuilder->executeQuery()[0]['OID'];
	}

	function getComentario($tabela, $instancia, $campo){
		$oid = $this->getEntidadeOid($tabela, $instancia);
		$stmt = $this->db->oracle->sqlBuilder->conn->prepare("select $campo from DYN$tabela where oid = '$oid'");
		$stmt->bindColumn(1, $lob, PDO::PARAM_LOB);
		
		try
		{
			$stmt->execute();
		}
		catch (Exception $exception)
		{
			die('Erro ao obter dados. Solicite ajuda ao Escritorio de Processos do CSC');
		}

		$stmt->fetch(PDO::FETCH_BOUND);

		return $lob;
	}

	function setComentario($tabela, $instancia, $campo, $valor){
		$oid = $this->getEntidadeOid($tabela, $instancia);

		$stream = fopen('data://text/plain;base64,' . base64_encode($valor),'rb');

		$stmt = $this->db->oracle->sqlBuilder->conn->prepare("update DYN$tabela set $campo = EMPTY_BLOB() where oid = '$oid' RETURNING $campo INTO :stream");
		$stmt->bindParam(':stream', $stream, PDO::PARAM_LOB);

		$this->db->oracle->sqlBuilder->conn->beginTransaction();

		try
		{
			$stmt->execute();
		}
		catch (Exception $exception)
		{
			die('Erro ao salvar dados. Solicite ajuda ao Escritorio de Processos do CSC');
		}

		return $this->db->oracle->sqlBuilder->conn->commit();
	}

	function getListaRequest($tabela, $instancia){
		$oid = $this->getEntidadeOid($tabela, $instancia);

		$this->db->oracle->sqlBuilder->select("request.numrequest||', ' as numrequest");
		$this->db->oracle->sqlBuilder->from("DYNLRSAP formulario");
		$this->db->oracle->sqlBuilder->join("DYNIRSAP request on request.OIDINCLUSAOREQUEST = formulario.oid");
		$this->db->oracle->sqlBuilder->where("formulario.oid = '$oid'");
		$this->db->oracle->sqlBuilder->orderby("request.sequencia");
		$lista = $this->db->oracle->sqlBuilder->executeQuery();
		return $lista;
	}

	function strToHex($string){
		$hex='';
		for ($i=0; $i < strlen($string); $i++){
			$hex .= dechex(ord($string[$i]));
		}
		return $hex;
	}

	function getDefinicaoAtividade($instancia){
		$this->db->oracle->sqlBuilder->select("p.idprocess, b.nmuser as analista, aduser.dsuseremail as email, formulario.modulosigam, formulario.versaosigam ");
		$this->db->oracle->sqlBuilder->from("wfprocess p");
		$this->db->oracle->sqlBuilder->join("GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc");
		$this->db->oracle->sqlBuilder->join("DYNLRSAP FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG");
		$this->db->oracle->sqlBuilder->leftjoin("wfstruct a on a.idprocess = p.idobject");
		$this->db->oracle->sqlBuilder->leftjoin("wfactivity b on b.idobject = a.idobject");
		$this->db->oracle->sqlBuilder->leftjoin("aduser on aduser.cduser = b.cduser");
		$this->db->oracle->sqlBuilder->where("a.idstruct = 'definicaoAtividadeAnalista' and P.idprocess LIKE '$instancia'");
		//echo $this->db->oracle->sqlBuilder->getQuery(); DIE();
		return  $this->db->oracle->sqlBuilder->executeQuery()[0];
	}

	function getHomologacao($instancia){
		$this->db->oracle->sqlBuilder->select("p.idprocess, b.nmuser as keyuser, aduser.dsuseremail as email, hcm.area, formulario.sistema, formulario.modulo, a.dtexecution");
		$this->db->oracle->sqlBuilder->from("wfprocess p");
		$this->db->oracle->sqlBuilder->join("GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc");
		$this->db->oracle->sqlBuilder->join("DYNLRSAP FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG");
		$this->db->oracle->sqlBuilder->leftjoin("wfstruct a on a.idprocess = p.idobject");
		$this->db->oracle->sqlBuilder->leftjoin("wfactivity b on b.idobject = a.idobject");
		$this->db->oracle->sqlBuilder->leftjoin("aduser on aduser.cduser = b.cduser");
		$this->db->oracle->sqlBuilder->join("SAP_XI.VRM_SOFTEXPERT@SEPRD HCM ON HCM.CPF = aduser.iduser");
		$this->db->oracle->sqlBuilder->where("a.idstruct = 'testeHomologacao' and P.idprocess LIKE '$instancia'");
		//echo $this->db->oracle->sqlBuilder->getQuery();
		return  $this->db->oracle->sqlBuilder->executeQuery()[0];
	}

	function getVersaoProducao($instancia){
		$this->db->oracle->sqlBuilder->select("p.idprocess, a.dtexecution");
		$this->db->oracle->sqlBuilder->from("wfprocess p");
		$this->db->oracle->sqlBuilder->join("GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc");
		$this->db->oracle->sqlBuilder->join("DYNLRSAP FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG");
		$this->db->oracle->sqlBuilder->leftjoin("wfstruct a on a.idprocess = p.idobject");
		$this->db->oracle->sqlBuilder->where("a.idstruct = 'versaoProducao' and P.idprocess LIKE '$instancia'");
		return  $this->db->oracle->sqlBuilder->executeQuery()[0];
	}


}



