<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
header('Content-Type:text/html;charset=UTF-8');


class Instancia extends ModularCore\Controller{

	function __construct() {
		parent::__construct();
	}

  /*
  * NECESSIDADES:
  *
  * PASSAR VIA GET OS SEGUINTES PARAMENTROS:
  *
  *  - OID - O OID DO RESGISTRO DA TABELA A SER LIDA/ALTERADA
  *  - TABELA - O NOME COMPLETO DA TABELA A SER LIDA/ALTERADA. EX. DYNSOLACESSO
  *  - CAMPO - O NOME DO CAMPO DA TABELA A SER LIDA/ALTERADA
  *
  * PASSA VIA POST OS SEGUINTES PARAMETROS (PARA ALTERA��O DO CONTEUDO):
  *
  *  - CONTEUDO - O CONTEUDO EM TEXTO PLANO
  *  - EDICAO - PERMITE QUE O CONTEUDO SEJA EDITAVEL
  *
  */

	function abrirLeitura(){


		if(isset($_GET['instancia'])){
			$this->loadModel('instanciaModel');
			$intancia = $this->models->instanciaModel->getInstancia($_GET['instancia'])[0];
			//var_dump($intancia); die();
			$idobject = $intancia['IDOBJECT'];
			echo "<html>
					<body>

					<form id='form' action='https://se.amaggi.com.br:6244/se/v34647/workflow/wf_gen_instance/wf_gen_instance_data.php ' method='POST'>

					<input name='typeScreen' value='1'>
					<input name='oid' value='$idobject'>
					<input name='read' value='1'>
					<button type='SUBMIT'>Enviar</button>


					<script>
						document.getElementById('form').submit();
					</script>


					</form>";
		}else{
			die('Voce nao tem permissao para acessar esta pagina');
		}
	}

	function abrirExecucao(){

		//die('Nao implementado');

		if(isset($_GET['instancia'])){
			$this->loadModel('instanciaModel');
			$intancia = $this->models->instanciaModel->getInstanciaExecucao($_GET['instancia'], $_SERVER['REMOTE_ADDR'])[0];
			$idprocess = $intancia['IDPROCESS'];
			$oid = $intancia['OIDENTITYREG'];

			//PRINT_R($intancia); die();
			echo "<script>window.location.href = 'https://se.amaggi.com.br:6244/se/v34647/workflow/wf_gen_instance/wf_gen_instance_data.php?view=0&typeButton=5&docs=1&oid=225932w1w1&idprocess=225932&REF=1&typeScreen=2';</script>";
		}else{
			die('Voce nao tem permissao para acessar esta pagina');
		}
	}
	
	function listarAtividadesWF(){
		$this->loadModel('instanciaModel');
		
		$this->coreView['atividades'] = $atividades = $this->models->instanciaModel->getAtividadesPendentesAoMeuUsuario($_SERVER['REMOTE_ADDR']);
		
		
		
		$this->loadView('atividadesView');
		
		
	}

}