<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
header('Content-Type:text/html;charset=ISO-8859-1');

class Api extends ModularCore\Controller{

	private $server;

	function __construct() {
		
		parent::__construct();
		$this->loadModel('instanciaModel');
	}

	//funcao de configuracao
	function webServiceSoap(){
		$this->loadLib('nusoap');
		$this->server = new \nusoap_server();
		$this->server->configureWSDL('ModularCore','urn:hudsonventura.com.br');
		$this->server->wsdl->schemaTargetNamespace = 'urn:ModularCore';

		$this->server->register('aprovarAjusteRomaneioSap',
								array('instancia'=>'xsd:string', 'aprovacao'=>'xsd:string', 'resposta'=>'xsd:string', 'motivo'=>'xsd:string'),
								array('sucesso'=>'xsd:string', 'msg'=>'xsd:string'));

		$this->server->register('consultaStatusPedidoCETI',
								array('nrPedido'=>'xsd:string'),
								array('status'=>'xsd:string'));

		$this->server->register('sincronizarUsuarioSEAD',
								array('cpf'=>'xsd:string'),
								array('sucesso'=>'xsd:string', 'msg'=>'xsd:string'));


		$data = file_get_contents('php://input');
		$data = isset($data) ? $data : '';
		$this->server->soap_defencoding = 'utf-8';
		$this->server->service($data);
	}
	
	function aprovarAjusteRomaneioSap($instancia, $aprovacao, $resposta, $motivo){
		
		require(DEFAULTFOLDER.'libs/vendor/autoload.php');
		
		
		$retorno = array('sucesso'=> 0, 'msg'=>'Nada executado');
		$retornoBanco = $this->models->instanciaModel->romaneioGetApi();
		
		$uri = $retornoBanco['URI'];
		$token = $retornoBanco['TOKEN'];
		

		$url  = $uri;
		
		$data =  [	'I_WORKFLOW_ID' => $instancia, 
					'I_TP_APROVACAO' => $aprovacao, 
					'I_TP_RESPOSTA' => $resposta, 
					'I_MOTIVO_RESPOSTA' => $motivo, 
					'format' => 'json'];
		$headers = array('verify' => false, 'headers'=> ['Authorization' => "Basic $token"],'json'=>$data);//	
					
		try{
			$client = new \GuzzleHttp\Client();
			$res = $client->post($uri, $headers);
			$retornoREST = json_decode($res->getHeader('content-type'));
			$retornoREST = json_decode($res->getBody());

			
			$retorno['sucesso'] = $retornoREST->E_SUCESSO;
			$retorno['msg'] = $retornoREST->E_MENSAGEM;

			if ($retornoREST->E_MENSAGEM == 'FAILURE') {
				$retorno['msg'] = $retornoREST->E_MENSAGEM_ERRO;
			}

		}catch(Exception $exception){
			$retornoREST = json_decode($exception->getResponse()->getBody(true));
			if ($retornoREST->ERROR_CODE) {
				$retorno = array('sucesso'=> $retornoREST->ERROR_CODE, 'msg'=>$retornoREST->ERROR_MESSAGE);
			}else{
				$retorno = array('sucesso'=> $exception->code, 'msg'=>$exception->message);
			}
			
			
		}
		return $retorno;
	}	

	function consultaStatusPedidoCETI($nrPedido){
		$retornoBanco = $this->models->instanciaModel->getStatusPedido($nrPedido);
		return $retornoBanco['DS_STATUS'];
	}

	function sincronizarUsuarioSEAD($cpf){
		error_reporting('ALL');
		$retorno = array('sucesso'=> 1, 'msg'=>'Usuario sincronizado com sucesso');

		$this->loadModel('usuarioCSC');

		//obtem o usuario no cadastro do RH
		$usuarioRH = $this->models->usuarioCSC->getUserHCM(null, $cpf);
		$usuarioRH = $usuarioRH['HCM'][0];
		if (!$usuarioRH) {
			return array('sucesso'=> 1, 'msg'=>'Usuario não encontrado');
		}


		//obtem area e função
		$area = $this->models->usuarioCSC->getAreaSE(null, $usuarioRH['AREA']);
		$funcao = $this->models->usuarioCSC->getFuncaoSE(null, $usuarioRH['CARGO']);
		$assocAreaFuncao = $this->models->usuarioCSC->getAssocAreaFuncao($area, $funcao);


		//cria função se nao existir
		if (!$funcao) {
			# CRIAR FUNCAO NO SE
			$criacaoFuncao = $this->models->usuarioCSC->criarFuncaoSE($usuarioRH['CARGO']);
			if (!$criacaoFuncao || $criacaoFuncao === -1) {
				return array('sucesso'=> 0, 'msg'=>'Falha na criação da funcao');
			}
		}


		//cria a area se nao existir
		if (!$area) {
			# CRIAR AREA NO SE
			$criacaoArea = $this->models->usuarioCSC->criarAreaSE($usuarioRH['AREA'], $usuarioRH['CARGO']);
			if (!$criacaoArea || $criacaoArea === -1) {
				return array('sucesso'=> 0, 'msg'=>'Falha na criação da área');
			}
		}

		//cria associação entre area e função se nao existir
		if (!$assocAreaFuncao) {
			$criacaoAssocAreaFuncao = $this->models->usuarioCSC->criarAssocAreaFuncaoSE($area, $funcao);
		}




		//adiciona função / cargo e area no usuario
		$assocFuncAreaUsuario = $this->models->usuarioCSC->getAssocFuncAreaUsuarioSE($usuarioRH['CPF']);
		if (!$assocFuncAreaUsuario) {
			# CRIAR AREA NO SE
			$criacaoAssocFuncAreaUsuario = $this->models->usuarioCSC->criarAssocFuncAreaUsuarioseSE($usuarioRH['CPF'], $usuarioRH['AREA'], $usuarioRH['CARGO']);
			if (!$criacaoAssocFuncAreaUsuario || $criacaoAssocFuncAreaUsuario === -1) {
				return array('sucesso'=> 0, 'msg'=>'Falha ao associar area e funcao ao usuario');
			}
		}

		//adiciona grupo de acesso
		$assocGrupoAcesso = $this->models->usuarioCSC->getAssocGrupoAcessoUsuarioseSE($usuarioRH['CPF']);
		if (!$assocGrupoAcesso) {
			# CRIAR AREA NO SE
			$criacaoAssocGrupoAcesso = $this->models->usuarioCSC->criarAssocGrupoAcessoUsuarioseSE($usuarioRH['CPF']);
			if (!$criacaoAssocGrupoAcesso || $criacaoAssocGrupoAcesso === -1) {
				return array('sucesso'=> 0, 'msg'=>'Falha ao associar do grupo de acesso');
			}
		}
		

		return $retorno;
	}

}


function aprovarAjusteRomaneioSap($instancia, $aprovacao, $resposta, $motivo){
	$object = new Api();
	return $object->aprovarAjusteRomaneioSap($instancia, $aprovacao, $resposta, $motivo);
}

function consultaStatusPedidoCETI($nrPedido){
	$object = new Api();
	return $object->consultaStatusPedidoCETI($nrPedido);
}

function sincronizarUsuarioSEAD($cpf){
	$object = new Api();
	return $object->sincronizarUsuarioSEAD($cpf);
}
