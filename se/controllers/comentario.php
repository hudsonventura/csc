<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
header('Content-Type:text/html;charset=UTF-8');


class Comentario extends ModularCore\Controller{

	function __construct() {
		parent::__construct();
	}

  /*
  * NECESSIDADES:
  *
  * PASSAR VIA GET OS SEGUINTES PARAMENTROS:
  *
  *  - OID - O OID DO RESGISTRO DA TABELA A SER LIDA/ALTERADA
  *  - TABELA - O NOME COMPLETO DA TABELA A SER LIDA/ALTERADA. EX. DYNSOLACESSO
  *  - CAMPO - O NOME DO CAMPO DA TABELA A SER LIDA/ALTERADA
  *
  * PASSA VIA POST OS SEGUINTES PARAMETROS (PARA ALTERA��O DO CONTEUDO):
  *
  *  - CONTEUDO - O CONTEUDO EM TEXTO PLANO
  *  - EDICAO - PERMITE QUE O CONTEUDO SEJA EDITAVEL
  *
  */

	function index(){
		$this->loadModel('comentarioModel');
		$this->loadLib('encryption');

		if(isset($_GET['tabela']) and isset($_GET['campo']) and isset($_GET['instancia'])){

			if(isset($_GET['edicao']))
				$edicao = true;
			else
				$edicao = '';


			//criptografa dos dados passados por GET
			$crypt = $this->libs->encryption->encrypt($_GET['instancia'].' || '.$_GET['tabela'].' || '.$_GET['campo'].' || '.time().' || '.$edicao, '123');
			$decrypt = $this->libs->encryption->decrypt($crypt, '123');

			//chama novamente a mesma URL, mas com GET criptografado
			$url = explode('?', $GLOBALS['_SERVER']['REQUEST_URI'])[0];
			$url = "Location: $url?crypt=$crypt";
			$url = str_replace('+', '%2B', $url);
			header($url);
		}else{

			if(isset($_GET['crypt'])){

				//descriptografa o GET
				$decrypt = $this->libs->encryption->decrypt($_GET['crypt'], '123');
				$decrypt = explode(' || ', $decrypt);
				$instancia = $decrypt[0];
				$tabela = $decrypt[1];
				$campo = $decrypt[2];
				$time = substr($decrypt[3], 0, 10);
				if(isset($decrypt[4])) $edicao = $decrypt[4]; else $edicao = false;

				$this->coreView['instancia'] = $instancia;
				$this->coreView['tabela'] = $tabela;
				$this->coreView['campo'] = $campo;

				$this->coreView['tipoDocumento'] = '';
				$this->coreView['tipoDocumento2'] = '';
				switch ($campo) {
					case 'ESPEC':
							$this->coreView['tipoDocumento'] = 'DOCUMENTO DE ESPECIFICA&Ccedil;&Atilde;O';
							$this->coreView['tipoDocumento2'] = 'Formul&aacute;rio de Libera&ccedil;&atilde;o para PRODU&Ccedil;&Atilde;O';
							$this->coreView['titulo'] = "$instancia - Especificação Técnica";
							$this->coreView['conteudo'] = 'EVIDÊNCIA';
							break;

					case 'HOMOLOG':
							$this->coreView['tipoDocumento'] = 'DOCUMENTO DE HOMOLOGA&Ccedil;&Atilde;O';
							$this->coreView['tipoDocumento2'] = 'Formul&aacute;rio de Libera&ccedil;&atilde;o para PRODU&Ccedil;&Atilde;O com evid&ecirc;ncia de teste';
							$this->coreView['titulo'] = "$instancia - Homologação";
							$this->coreView['conteudo'] = 'EVIDÊNCIA';
							break;

					case 'DESCRICAOKEY':
							$this->coreView['tipoDocumento'] = 'SOLICITAÇÃO DE CUSTOMIZAÇÃO';
							$this->coreView['tipoDocumento2'] = 'Formul&aacute;rio de Customização';
							$this->coreView['titulo'] = "$instancia - Descrição Analítica do KeyUser";
							$this->coreView['conteudo'] = 'EVIDÊNCIA';
							break;

					default:
				}

				if(isset($_GET['imprimir'])){

					//campos necessarios para gerar o documento para impress�o
					//dados da defini��o de atividade (customiza��o)
					$dadosDefinicaoAtividade = $this->models->comentarioModel->getDefinicaoAtividade($instancia);
					$this->coreView['analistaNome'] = $dadosDefinicaoAtividade['ANALISTA'];
					$this->coreView['analistaEmail'] = $dadosDefinicaoAtividade['EMAIL'];

					//dados do key user (homologa��o)
					$dadosHomologacao = $this->models->comentarioModel->getHomologacao($instancia);
					$this->coreView['keyuserNome'] = $dadosHomologacao['KEYUSER'];
					$this->coreView['keyuserArea'] = $dadosHomologacao['AREA'];
					$this->coreView['sistema'] = $sistema = $dadosHomologacao['SISTEMA'];
					$this->coreView['modulo'] = $dadosHomologacao['MODULO'];
					$this->coreView['dataHomologacao'] = $dadosHomologacao['DTEXECUTION'];

					//nao permite a impressao de um documento de homolgoca��o quando esta ainda nao foi realizada
					if($campo == 'HOMOLOG' && !$this->coreView['dataHomologacao']){
						die('A homolocacao ainda nao foi realizada, por isso este documento nao pode ser gerado.');
					}

					//dados da versao em produ��o (gestao)
					$dadosVersaoProducao = $this->models->comentarioModel->getVersaoProducao($instancia);
					$this->coreView['versaoProducaoData'] = $dadosVersaoProducao['DTEXECUTION'];
					$this->coreView['versaoModulo'] = $versaoModulo = $dadosDefinicaoAtividade['MODULOSIGAM'];
					$this->coreView['versaoLiberada'] = $versaoLiberada = $dadosDefinicaoAtividade['VERSAOSIGAM'];
					//campos necessarios para gerar o documento para impress�o

					
					
					if (strpos($sistema, 'SAP')===0) {
						$versaoOUResquest = '';
						$versaoOUResquestArray = $this->models->comentarioModel->getListaRequest($tabela, $instancia);
						foreach ($versaoOUResquestArray as $value) {
							$versaoOUResquest .= $value['NUMREQUEST'];
						}
						$this->coreView['versaoOUResquest'] = $versaoOUResquest = substr($versaoOUResquest, 0, strlen($versaoOUResquest) -2);
					}else{
						$this->coreView['versaoOUResquest'] = $versaoOUResquest = @"$versaoModulo / $versaoLiberada";
					}
					

					$stream = $this->models->comentarioModel->getcomentario($tabela, $instancia, $campo);
					if($stream){
						$this->coreView['conteudoImprimir'] = stripslashes (stream_get_contents($stream));
					}
					$this->loadView('comentarioImprimirView');
					die();
				}







				if(isset($_POST['conteudo'])){
					$this->coreView['conteudo'] = $conteudo = $_POST['conteudo'];

					$retorno = $this->models->comentarioModel->setcomentario($tabela, $instancia, $campo, addslashes($conteudo));
					if($retorno){
						echo '<script>window.close();</script>';
					}else{
						die('Algum erro ocorreu ao salvar seus dados. Solicite ajuda ao Escritorio de Processos do CSC');
					}

				}

				//validando o horario para validar a criptgrafia e nao permitir que o usuario copie a URL e use posteriormente
				if($time+15 < time() || $edicao === false || $edicao == 0)
					$this->coreView['somenteleitura'] = '';




				$stream = $this->models->comentarioModel->getcomentario($tabela, $instancia, $campo);

				if($stream){
					$this->coreView['conteudo'] = stripslashes (stream_get_contents($stream));

				}else{
					$this->coreView['conteudo'] = '';
				}
				$this->loadView('comentarioView');
			}
			else{
				require BASE.'/core/errors/403.php';
			}

		}









	}

	private function imprimirDocumento(){
		header('Content-Type: text/html; charset=iso-8859-1');

		$this->loadLib('Pdf');
		$mpdf=new mPDF();
		$conteudo = '';

		$lines = file (MODULEFOLDER.'/views/homologViewPDF.php');


		// Percorre o array, mostrando o fonte HTML com numera��o de linhas.
		foreach ($lines as $line_num => $line) {
			$conteudo .= $line;
		}
		$mpdf->WriteHTML($conteudo);
		$mpdf->Output();


		//$this->loadView('homologViewPDF');
	}
}