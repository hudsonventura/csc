<!DOCTYPE html >
<html lang="en">

<head>

    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />
	
	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    
	<!--Imports-->
		<!-- Bootstrap Core CSS --><link href="						<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<!-- jQuery --><script src="								<?php echo DEFAULTVIEWDIR;?>assets/jquery/js/jquery.min.js"></script>
		<!-- Bootstrap Core JS --> <script src="					<?php echo DEFAULTVIEWDIR;?>assets/bootstrap/js/bootstrap.min.js"></script>
		<!-- Custom Fonts --><link href="							<?php echo DEFAULTVIEWDIR;?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <table class="table table-condensed table-hover">
        <tr>
            <th>Instância</th>
            <th>Atividade</th>
            <th>Prazo</th>
            <th>Solicitante</th>
        </tr>

        <?php
        foreach ($atividades as $atividade) {
            echo '<tr>';
                echo '<td>'.$atividade['IDPROCESS'].'</td>';
                echo '<td>'.$atividade['NMSTRUCT'].'</td>';
                echo '<td>'.$atividade['DTESTIMATEDFINISH'].'</td>';
                echo '<td>'.$atividade['NMUSERSTART'].'</td>';
                
            echo '</tr>';
        }
            
            //list-group-item-success
        ?>
    </table>
