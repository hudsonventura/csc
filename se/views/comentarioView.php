
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="ModularCore" />
    <meta name="author" content="hudsonventura@gmail.com" />

    <link rel="shortcut icon" href="/modules\amaggi\views\assets\/favicon.png" type="image/x-icon" />
    <link href="https://suporte.amaggi.com.br/modules/amaggi/views/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    
    <title><?php echo $titulo ?></title>

    <!--Imports-->
    <script src="<?php echo ASSETS?>ckeditor/ckeditor.js"></script>
    <script src="<?php echo ASSETS?>ckeditor/config.js"></script>
    <script src="<?php echo ASSETS?>ckeditor/plugins/image/dialogs/image.js"></script>
    
</head>

<body>
    <form action="" method="GET">
        <input type="hidden" name="instancia" value="<?php echo $instancia ?>" />
        <input type="hidden" name="tabela" value="<?php echo $tabela ?>" />
        <input type="hidden" name="campo" value="<?php echo $campo ?>" />
    </form>

    <form action="" method="POST">
            <?php $dominio= $_SERVER['HTTP_HOST'];
                $url = "https://" . $dominio. $_SERVER['REQUEST_URI'];
            ?>
            <a href="<?php echo $url;?>&imprimir" style="margin: 10px;" class="btn btn-primary">Imprimir Documento</a>
            <?php if(!isset($somenteleitura)) echo '<button class="btn btn-danger" style="margin: 10px;" value="Submit">Salvar Documento</button>'; ?>
        <br />
		 <textarea <?php if(isset($somenteleitura)) echo 'readonly'; ?> name="conteudo" id="editor1" style="height:500px;"><?php echo $conteudo?></textarea>
		 <script>
			 CKEDITOR.replace('editor1');
		 </script>
		
    </form>
    
