<?php define('ASSETS', '/amaggi/views');?>
<?php require ('/../../amaggi/views/layout/head.php'); ?>
<?php require ('/../../amaggi/views/layout/header.php'); ?>

<div class="container">
    <a href="adicionar" class="btn btn-primary"><span class="fa fa-plus"></span> Adicionar</a>
</div>

<br />

<div class="container col-xs-12" role="tabpanel" aria-labelledby="monitor" aria-expanded="true">
    <div class="panel panel-default">
	<div class="panel-body">
	    <?php echo $table?>
	</div>
    </div>
</div>





<?php require ('/../../amaggi/views/layout/footer.php');  ?>