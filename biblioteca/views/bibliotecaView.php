<?php if(isset($conhecimento[0])) 
{
	$html = str_replace('src="', 'src="'.ASSETS, $conhecimento[0]['conteudo']);
	$html = str_replace('<link href="', '<link href="'.ASSETS, $html);
	$html = str_replace('controls poster="', 'controls poster="'.ASSETS, $html);
	echo $html;
	die();
}?>


<!DOCTYPE HTML>
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Biblioteca de conheciomento CSC</title>
		<link rel="icon" type="image/png" href="<?php echo ASSETS;?>img/csc.png" />
		<link href='<?php echo ASSETS;?>style.css' rel='stylesheet' type='text/css' />
 		<link href="<?php echo ASSETS;?>css/se.css" rel="stylesheet" />
	</head>
	
	<body>
		<div id="wrapper-header">
			<div id="main-header" class="object">
				<img src="<?php echo ASSETS;?>img/csc.png" height="70" width="70" />
				<img src="<?php echo ASSETS;?>img/bconhecimento.png" style="margin: 20px;"/>
				<div id="main_tip_search">
					<form>
						<input type="text" name="busca" id="tip_search_input" list="busca" placeholder="<?php if(isset($busca)) echo $busca;?>" />
					</form>
				</div>
				<div id="stripes"></div>
			</div>
		</div>
		
		
		
		<!-- PORTFOLIO -->
		<div id="wrapper-container">
			<div class="container">
				<div id="main-container-image">
					<section class="work">
						<?php if(isset($listaConhecimentos)) foreach($listaConhecimentos as $conhecimento){ ?>
							<figure class="white">
								<a href="<?php echo MODULEDIR.'/conteudo?id='.$conhecimento['id']?>">
									<img src="<?php echo ASSETS.'img/'.$conhecimento['img_preview'];?>" alt="" />
									<dl>
										<dt><?php echo $conhecimento['titulo']?></dt>
										<dd><?php echo $conhecimento['descricao']?></dd>
									</dl>
								 </a>
								<div id="wrapper-part-info">
									<div id="part-info"><?php echo $conhecimento['titulo']?></div>
								</div>
							</figure>
						<?php }?>
						
					</section>
					
				</div>
			</div> 
		</div>
			
		
		<!-- SCRIPT -->
		<script src="<?php echo ASSETS;?>js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>js/jquery.scrollTo.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>js/jquery.localScroll.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>js/jquery-animate-css-rotate-scale.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>js/fastclick.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>js/jquery.animate-colors-min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>js/jquery.animate-shadow-min.js"></script>
		<script type="text/javascript" src="<?php echo ASSETS;?>js/main.js"></script>
		
		<div id="wrapper-copyright">
			<div class="copyright">
				<div class="copy-text object">Contato: csc.processo@amaggi.com.br </div>
			</div>
		</div>
	</body>



</html>
