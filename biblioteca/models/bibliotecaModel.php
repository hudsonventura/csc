<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class BibliotecaModel extends ModularCore\Model{

	function __construct() {
		parent::__construct();

		$db = 'cscPRD';
		$dbname = 'biblioteca';
		ModularCore\core::$coreConfig['databases'][$db]['dbname'] = $dbname;
		$this->loadDb($db, $dbname);
	}

	function getListaConhecimentos($busca){
		$this->db->biblioteca->sqlBuilder->select('id, titulo, descricao, img_preview');
		$this->db->biblioteca->sqlBuilder->from('conhecimento');
		$this->db->biblioteca->sqlBuilder->where("UPPER(titulo) like UPPER('%$busca%') OR UPPER(descricao) like UPPER('%$busca%') OR UPPER(conteudo) like UPPER('%$busca%')");
		$this->db->biblioteca->sqlBuilder->orderby("titulo");
		return $this->db->biblioteca->sqlBuilder->executeQuery();
	}

	function getConhecimento($id){
		$this->db->biblioteca->sqlBuilder->select('id, titulo, descricao, img_preview, conteudo');
		$this->db->biblioteca->sqlBuilder->from('conhecimento');
		$this->db->biblioteca->sqlBuilder->where("id = $id");
		return $this->db->biblioteca->sqlBuilder->executeQuery();
	}


}



