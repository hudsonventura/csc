<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class Gerenciar extends ModularCore\Controller{




	function __construct() {

		parent::__construct($this);
		$this->loadModel('bibliotecaModel');

		//Permissoes
		$this->loadLib('Permission');
		$this->loadLib('session');

		$this->usuario = $this->libs->session->getData('usuario');
		$this->coreView['usuario'] = $this->usuario;

		$this->libs->Permission->required = array('csc.processo');
		$this->libs->Permission->verify($this->coreView['usuario']['memberof']);
		//Permissoes
	}

	public function index(){


		$this->coreView['title']='Gerenciamento de Gonhecimentos';
		$this->loadLib('make');
		$listaConhecimentos = $this->models->bibliotecaModel->getListaConhecimentos('%');
		//echo ModularCore\core::$coreConfig;

		/*$listaNova = array();
		$lista = $this->models->modelLink->getLinks();
		foreach($lista as $link){
			unset($link['statusLink']);
			unset($link['tipo_servico']);
			unset($link['statusVPN']);
			unset($link['mensagem']);
			unset($link['aguardando']);
			unset($link['ip_alvo']);
			unset($link['cmd_ativaVPN']);
			unset($link['cmd_desativaVPN']);
			//VIRADA AUTOMATIRA?
			if($link['vpnAuto']) $link['vpnAuto'] = 'Sim'; else $link['vpnAuto'] = null;

			//VIRADA INVERSA?
			if($link['tipo_virada']) $link['tipo_virada'] = 'Inversa'; else $link['tipo_virada'] = null;


			$link['edicao'] = $this->libs->make->link('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', MODULEDIR.'/links/Alterar?id='.$link['id'], 'btn btn-success');
			$link['delete'] = $this->libs->make->link('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', MODULEDIR.'/links/Deletar?id='.$link['id'], 'btn btn-danger');
			unset($link['id']);
			array_push($listaNova, $link);
		}*/


		$this->coreView['listaConhecimentos'] = $listaConhecimentos;

		$this->coreView['table'] = $this->libs->make->table(array('Localidade', 'Ip do Roteador', 'Porta', 'Timeout', 'Comutação Automática da VPN', 'Virada', 'Editar', 'Deletar'), $listaConhecimentos, 'table table-bordered');

		$this->loadView('viewGerencia');
    }




    public function alterar(){
	$this->coreView['title']='Alteração do Cadastro do Link';
	$this->coreView['geral'] = $this->models->modelLink->getGeral();


	if(isset($_GET)){
	     $link = $this->models->modelLink->getLink($_GET['id']);
	     $cmd_ativaVPN = $link['cmd_ativaVPN'];
	     $cmd_ativaVPN = str_replace('|',"\n", $cmd_ativaVPN);
	     $link['cmd_ativaVPN'] = $cmd_ativaVPN;

	     $cmd_desativaVPN = $link['cmd_desativaVPN'];
	     $cmd_desativaVPN = str_replace('|',"\n", $cmd_desativaVPN);
	     $link['cmd_desativaVPN'] = $cmd_desativaVPN;

	     $this->coreView['link'] = $link;
	}

	if($_POST){
			$link = $_POST;
			$cmd_ativaVPN = $link['cmd_ativaVPN'];
			$cmd_ativaVPN = explode("\n ", $cmd_ativaVPN);
			$cmd_ativaVPN = implode("\n", $cmd_ativaVPN);
			$cmd_ativaVPN = explode("\n", $cmd_ativaVPN);
			//var_dump($_POST); die();
			$this->models->modelLink->update($_POST);
			header('Location: gerenciar');
	}

	$this->loadView('viewLinksAdicionar');
    }


    public function deletar(){
	$this->coreView['title']='Deletar Cadastro do Link';


	if(isset($_GET)){
	    $link = $this->models->modelLink->getLink($_GET['id']);
	    $this->models->modelLink->delete($link);
	}


	header('Location: '.MODULEDIR.'/links/gerenciar');
    }


    public function adicionar(){
	$this->coreView['title']='Adicionar Links';

	$this->coreView['listaLinks'] = $this->models->modelLink->getLinks();
	$this->coreView['geral'] = $this->models->modelLink->getGeral();
	if($_POST){
		$array = $_POST;
		$array['tipo_servico'] = 0;
			$this->models->modelLink->salvarLink($array);
	    header('Location: '.MODULEDIR.'/links/gerenciar');
	}

	$this->loadView('viewLinksAdicionar');
    }



}