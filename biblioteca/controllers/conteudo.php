<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class Conteudo extends ModularCore\Controller{

	function __construct() {
		parent::__construct();
		$this->loadModel('bibliotecaModel');
	}

	function index(){

		$id = '';
		if(isset($_GET['id'])){
			$id = $_GET['id'];
		}


		$this->coreView['conhecimento'] = $this->models->bibliotecaModel->getConhecimento($id);

		$this->loadView('bibliotecaView');
	}
	function teste(){
		$this->coreConsole = array('123', 'testeSSSS');
		$this->loadView('teste');
	}


}