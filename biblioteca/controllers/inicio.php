<?php
if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class Inicio extends ModularCore\Controller{

	function __construct() {
		parent::__construct();
	}

	function index(){
		//carrega os models e bibliotecas necessários
		$this->loadModel('bibliotecaModel');

		//trata a busca
		$busca = '';
		if(isset($_GET['busca'])){
			$busca = $_GET['busca'];
		}
		$this->coreView['busca'] = $busca;

		//busca a lista dos conhecimentos no banco
		$this->coreView['listaConhecimentos'] = $this->models->bibliotecaModel->getListaConhecimentos($busca);

		//carrega a view
		$this->loadView('bibliotecaView');
	}
}